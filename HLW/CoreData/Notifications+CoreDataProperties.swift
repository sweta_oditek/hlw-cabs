//
//  Notifications+CoreDataProperties.swift
//  
//
//  Created by ODITEK on 24/10/19.
//
//

import Foundation
import CoreData

extension Notifications {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Notifications> {
        return NSFetchRequest<Notifications>(entityName: "Notifications")
    }

    @NSManaged public var cus_id: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var image_url: String?
    @NSManaged public var notification_id: String?
    @NSManaged public var notification_type: String?
    @NSManaged public var subject: String?
    @NSManaged public var title: String?

}
