//
//  DatePickerViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 24/04/22.
//  Copyright © 2022 ODITEK. All rights reserved.
//

import UIKit

@objc protocol ChooseDateTimeDelegate: class {
    @objc optional func selectedDateTime(totalTripTime: Date)
}

class DatePickerViewController: UIViewController {
    
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    weak var delegate: ChooseDateTimeDelegate?
    var selectedDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        dateTimePicker.minimumDate = Date()
        dateTimePicker.date = selectedDate
        
//        dateTimePicker.maximumDate = Date().add(days: 2)//User can book a cab before 2 days only
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "DatePickerPage", screenClass: "DatePickerViewController")
    }
    
    //MARK: Button Actions
    @IBAction func datePickerCancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func datePickerDoneBtnAction(_ sender: Any) {
        self.delegate?.selectedDateTime?(totalTripTime: dateTimePicker.date)
        print("SelectDate === \(dateTimePicker.date)")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
