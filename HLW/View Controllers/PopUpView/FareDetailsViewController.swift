//
//  FareDetailsViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 27/12/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit

class FareDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTotalCost: UILabel!
    @IBOutlet weak var lblTaxCost: UILabel!
    @IBOutlet weak var lblFareInfo: UILabel!
    @IBOutlet weak var lblFareDetails: UILabel!
    @IBOutlet weak var tblFare: UITableView!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet var viewSelectedCar: UIView!
    @IBOutlet var innerViewSelectedCar: UIView!
    @IBOutlet weak var lblSelectedCar: UILabel!
    @IBOutlet weak var selectedCarImgView: UIImageView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTotalCostHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTaxCostHeightConstraint: NSLayoutConstraint!
    
    var arrFareDettails = [FareDetails]()
    var totalFare : String = "0"
    var info : String = ""
    var taxValue : String = "0"
    var vehicleTypeId : String = ""
    var package : String = ""
    var isForRental: Bool = false
    var vehicleTypeBo = VehicleTypeBO()
    var type: Int = 0
    
    var tableViewheight: CGFloat {
        tblFare.layoutIfNeeded()
        return tblFare.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDesign()
    }
    
    func initDesign(){
        tblFare.tableFooterView = UIView()
        okBtn.layer.cornerRadius = 5
        viewPopup.layer.cornerRadius = 10
        viewPopup.clipsToBounds = true
        
        popUpViewLeadingConstraint.constant = (AppConstant.screenSize.width * 11)/100
        popUpViewTrailingConstraint.constant = (AppConstant.screenSize.width * 11)/100
        
        let viewTotalHeight = (lblFareDetails.bounds.size.height + lblTotalCost.bounds.size.height + lblTaxCost.bounds.size.height + lblFareInfo.bounds.size.height + okBtn.bounds.size.height + viewSelectedCar.bounds.size.height)
        
        tableViewHeightConstraint.constant = self.tableViewheight
        popUpViewHeightConstraint.constant = (self.tableViewheight + viewTotalHeight + 100)
        
        lblTotalCost?.text = "Total Fare  ₹ " + totalFare
        lblTaxCost?.text = isForRental == true ? "package: \(package)" : "" //"( Includes ₹ " + taxValue + " Taxes )"
        lblTaxCostHeightConstraint.constant = isForRental == true ? 20 : 0
        lblFareInfo?.text = info
        
        let vehicle = self.setCabType(id: vehicleTypeId)
        self.lblSelectedCar.text = vehicle.name
        self.selectedCarImgView.image = UIImage(named: vehicle.selectedImage)
        
        viewSelectedCar.layer.cornerRadius = viewSelectedCar.frame.size.width / 2
        viewSelectedCar.clipsToBounds = true
        
        innerViewSelectedCar.layer.cornerRadius = innerViewSelectedCar.frame.size.width / 2
        innerViewSelectedCar.clipsToBounds = true
        
        //Manage for iPhone 5 and Below
        if AppConstant.screenSize.height <= 568{
            lblTotalCostHeightConstraint.constant = 40
            lblFareInfo.font = UIFont.systemFont(ofSize: 9, weight: .regular)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "FareDetailsPage", screenClass: "FareDetailsViewController")
    }
    
    func setCabType(id: String) -> VehicleTypeBO{
        let vehicleBo = VehicleTypeBO()
        switch id {
        case "16":
            vehicleBo.name = "Auto"
            vehicleBo.selectedImage = StringConstant.auto_blue
            break
        case "2":
            vehicleBo.name = "Micro"
            vehicleBo.selectedImage = StringConstant.car_micro_blue
            break
        case "3":
            vehicleBo.name = "Mini"
            vehicleBo.selectedImage = StringConstant.car_mini_blue
            break
        case "5":
            vehicleBo.name = "Sedan"
            vehicleBo.selectedImage = StringConstant.car_sedan_blue
            break
        case "15":
            vehicleBo.name = "SUV"
            vehicleBo.selectedImage = StringConstant.car_suv_blue
            break
        default:
            break
        }
        return vehicleBo
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Button Action
    @IBAction func btnOKAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.removeFromParent()
                self.view.removeFromSuperview()
            }
        })
    }
    
    
    //MARK: Tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFareDettails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FareDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FareDetailsTableViewCell") as! FareDetailsTableViewCell
        cell.selectionStyle = .none
        
        if (indexPath.row < arrFareDettails.count) {
            let fareDetailsBo = arrFareDettails[indexPath.row]
            cell.lblFareTitle?.text = fareDetailsBo.title
            cell.lblFare?.text = fareDetailsBo.type == 0 ? "\(fareDetailsBo.price)" : "₹ \(fareDetailsBo.price)"
        }
        
        return cell
    }
    

}
