//
//  MakeYourSelectionViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 28/05/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class MailInvoiceViewController: UIViewController {
  
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtFldMail: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        txtFldMail.text = AppConstant.retrievFromDefaults(key: StringConstant.email)
        
        self.btnCancel.layer.cornerRadius = 5
        self.btnCancel.clipsToBounds = true
        
        self.btnSend.layer.cornerRadius = 5
        self.btnSend.clipsToBounds = true
        
        self.viewPopup.layer.cornerRadius = 5
        self.viewPopup.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "MailInvoicePage", screenClass: "MailInvoiceViewController")
    }
    
    //MARK: Button Action
    @IBAction func btnCancelAction(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        self.serviceCallToBookInvoice()
    }

    //MARK: - Service Call Method
    func serviceCallToBookInvoice() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),"access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),"book_id": AppConstant.retrievFromDefaults(key: StringConstant.book_id),"user_email": AppConstant.retrievFromDefaults(key: StringConstant.email)]
            
            print("Url===\(AppConstant.bookInvoiceUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.bookInvoiceUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int{
                                if status == 1{
                                    if let msg = dict["msg"] as? String{
                                        self.dismiss(animated: true, completion: nil)
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
}
