//
//  SOSCallViewController.swift
//  HLW
//
//  Created by ODITEK on 31/08/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class SOSCallViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblViewEmergencyContactList: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpViewHeightConstraint: NSLayoutConstraint!
    
    var arrContacts = [EmergencyContactBO]()
    
    var tableViewheight: CGFloat {
        tblViewEmergencyContactList.layoutIfNeeded()
        return tblViewEmergencyContactList.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        tblViewEmergencyContactList.delegate = self
        tblViewEmergencyContactList.dataSource = self
        tblViewEmergencyContactList.tableFooterView = UIView()
        
//        tblViewHeightConstraint.constant = tableViewheight
//        popUpViewHeightConstraint.constant = (self.tableViewheight + 115)
        
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
        
        self.serviceCallToGetEmergencyContact()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "SOSCallPage", screenClass: "SOSCallViewController")
    }
    
    // MARK: - Button Action
    @IBAction func btnCancelAction(_ sender: Any) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.removeFromParent()
                self.view.removeFromSuperview()
            }
        })
    }
    
    @objc func btnCallAction(_ sender: UIButton){
        let phoneNumber: String = arrContacts[sender.tag].number!.replacingOccurrences(of: " ", with: "")
        if let phoneCallURL:URL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(phoneCallURL as URL)
                }
                
            }
        }
    }
    
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmergencyCall", for: indexPath as IndexPath)as! SOSCallTableViewCell
        cell.selectionStyle = .none
        
        let contactBo = arrContacts[indexPath.row]
        
        let name = contactBo.name
        let number = contactBo.number!
        cell.lblContactName.text = "\(name!) - \(number)"
        
        cell.btnCall.tag = indexPath.row
        cell.btnCall.addTarget(self, action: #selector(btnCallAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tblViewEmergencyContactList.reloadData()
    }
    
    //MARK: Service Call
    func serviceCallToGetEmergencyContact() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),"access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("url===\(AppConstant.getEmergencyUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getEmergencyUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            self.arrContacts.removeAll()
                            
                            let contactBo = EmergencyContactBO()
                            contactBo.name = "POLICE"
                            contactBo.number = "100"
                            self.arrContacts.append(contactBo)
                            
                            if let status = dict["status"] as? Int{
                                if status == 1{
                                    if let arrContactInfo = dict["data"] as? [[String: Any]]{
                                        for dictData in arrContactInfo{
                                            let contactBo = EmergencyContactBO()
                                            if let name = dictData["name"] as? String{
                                                contactBo.name = name
                                            }
                                            if let mob = dictData["mobile"] as? String{
                                                contactBo.number = mob
                                            }
                                            if let share = dictData["share"] as? String{
                                                contactBo.share_status = share
                                            }
                                            
                                            self.arrContacts.append(contactBo)
                                        }
                                    }
                                    self.tblViewEmergencyContactList.reloadData()
                                }else{
                                //   self.lblEmergencyDesc.text = StringConstant.empty_emergency_contact_msg
                                }
                            }
                            self.tblViewHeightConstraint.constant = self.tableViewheight
                            self.popUpViewHeightConstraint.constant = CGFloat(((self.arrContacts.count * 78) + 115))
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
