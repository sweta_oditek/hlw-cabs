//
//  PaymentModeViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 2/5/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit

@objc protocol ChoosePaymentModeDelegate: class {
    @objc func selectedPaymentMode(pMode: String)
}

class PaymentModeViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cashPaymentRadioImage: UIImageView!
    @IBOutlet weak var cardPaymentRadioImage: UIImageView!
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: ChoosePaymentModeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesigns()
    }
    
    func initDesigns() {
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            btnHeightConstraint.constant = 60
        }
        
        //Manage for iPhone 5 and Below
        if AppConstant.screenSize.height <= 568{
            lblTitle.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        }
        
        if (AppConstant.selectedPaymentModeStatus == "1") {
            cashPaymentRadioImage?.image = UIImage.init(named: "radio_checked")
            cardPaymentRadioImage?.image = UIImage.init(named: "radio_unchecked")
        }
        else {
            cashPaymentRadioImage?.image = UIImage.init(named: "radio_unchecked")
            cardPaymentRadioImage?.image = UIImage.init(named: "radio_checked")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "PaymentModePage", screenClass: "PaymentModeViewController")
    }
    
    //MARK: - Button Action
    @IBAction func btnDoneAction(_ sender: Any) {
        if (AppConstant.selectedPaymentModeStatus == "1") {
            self.delegate?.selectedPaymentMode(pMode: "Cash")
        }
        else {
            self.delegate?.selectedPaymentMode(pMode: "Card")
        }
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnPaymentModeAction(_ sender: UIButton) {
        if (sender.tag == 1) {
            cashPaymentRadioImage?.image = UIImage.init(named: "radio_checked")
            cardPaymentRadioImage?.image = UIImage.init(named: "radio_unchecked")
            AppConstant.selectedPaymentModeStatus = "1"
        }
        else if (sender.tag == 2) {
            cashPaymentRadioImage?.image = UIImage.init(named: "radio_unchecked")
            cardPaymentRadioImage?.image = UIImage.init(named: "radio_checked")
            AppConstant.selectedPaymentModeStatus = "2"
        }
    }
    
}

