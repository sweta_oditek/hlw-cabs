//
//  NotificationsViewController.swift
//  HLW
//
//  Created by ODITEK on 01/08/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import CoreData

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnClearAll: UIButton!
    @IBOutlet weak var tblViewNotificationsList: UITableView!
    @IBOutlet weak var lblNoNotifications: UILabel!
    
    var arrNotification = [Notifications]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initDesign()
        retriveData()
        
        print("Date Format \(Date())")
    }
    
    func initDesign(){
        tblViewNotificationsList.delegate = self
        tblViewNotificationsList.dataSource = self
        tblViewNotificationsList.tableFooterView = UIView()
        
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
        }
        
        btnClearAll.isHidden = true
        lblNoNotifications.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "NotificationPage", screenClass: "NotificationsViewController")
    }
    
    func retriveData(){
        let fetchRequest: NSFetchRequest<Notifications> = Notifications.fetchRequest()
        
        do {
            let notificationData = try PersistantStorage.context.fetch(fetchRequest)
            self.arrNotification = notificationData
            self.tblViewNotificationsList.reloadData()
        } catch {}
        
        if arrNotification.count == 0{
            btnClearAll.isHidden = true
            lblNoNotifications.isHidden = false
        }else{
            btnClearAll.isHidden = false
            lblNoNotifications.isHidden = true
        }
    }
    
    func deleteAll(){
        let fetchRequest: NSFetchRequest<Notifications> = Notifications.fetchRequest()
        fetchRequest.includesPropertyValues = false
        
        do {
            let notificationData = try PersistantStorage.context.fetch(fetchRequest)
            for item in notificationData {
                //guard let obj = item as? NSManagedObject else{continue}
                PersistantStorage.context.delete(item)
            }
            // Save Changes
            try PersistantStorage.context.save()
            self.retriveData()
        } catch {}
    }
    
    // MARK: - Button Actions
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnClearAllAction(_ sender: Any) {
        self.showAlertForDelete()
    }
    
    func showAlertForDelete() {
        let alert = UIAlertController(title: "Do You Want To Clear All The Data", message: "", preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.deleteAll()
        })
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        alert.addAction(cancelAction)
        
        alert.preferredAction = deleteAction
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsLists", for: indexPath as IndexPath) as! NotificationsListTableViewCell
        cell.selectionStyle = .none
        
        let notifyBo = arrNotification[indexPath.row]
        if notifyBo.notification_type == "5"{
            cell.lblTitle.text = notifyBo.title
            cell.lblTitleData.text = notifyBo.subject
            cell.lblDateAndTime.text = AppConstant.formattedDate(date: notifyBo.date! as Date, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter5)
            
            let image: String = notifyBo.image_url!
            if image == ""{
                cell.imgViewHeightConstraint.constant = 0
            }else{
                cell.imgViewImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: ""))
            }
            
        }
        
        return cell
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
