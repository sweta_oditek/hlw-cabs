//
//  BillViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 09/02/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Cosmos

class BillViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblGreen: UILabel!
    @IBOutlet weak var lblRed: UILabel!
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTotalPay: UILabel!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var lblDropAddress: UILabel!
    
    @IBOutlet weak var tblViewBillingDetails: UITableView!
    @IBOutlet weak var tblViewBillingHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    var bookingStatusBo = BookingStatusBO()
    
    var tableViewheight: CGFloat {
        tblViewBillingDetails.layoutIfNeeded()
        return tblViewBillingDetails.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        tblViewBillingDetails.delegate = self
        tblViewBillingDetails.dataSource = self
        tblViewBillingDetails.tableFooterView = UIView()
        
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
            btnHeightConstraint.constant = 60
        }
        
        tblViewBillingHeightConstraint.constant = self.tableViewheight
        
        self.lblGreen.layer.cornerRadius = self.lblGreen.frame.height/2
        self.lblGreen.layer.cornerRadius = self.lblGreen.frame.width/2
        self.lblGreen.clipsToBounds = true
        
        self.lblRed.layer.cornerRadius = self.lblRed.frame.height/2
        self.lblRed.layer.cornerRadius = self.lblRed.frame.width/2
        self.lblRed.clipsToBounds = true
    
        self.lblPickUpAddress.text = self.bookingStatusBo.bookingInfoBo.pick_pnt
        self.lblDropAddress.text = bookingStatusBo.bookingInfoBo.drop_pnt
        
        self.lblTotalPay.text = "₹\(bookingStatusBo.rideFareBo.totalCost)"
        self.lblPaymentMode.text = bookingStatusBo.rideFareBo.payMode
        if bookingStatusBo.rideFareBo.payMode == "Cash"{
            btnOk.setTitle("OK", for: .normal)
        }else{
            btnOk.setTitle("PAY NOW", for: .normal)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "BillingPage", screenClass: "BillViewController")
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOkAction(_ sender: Any) {
        if bookingStatusBo.rideFareBo.payMode == "Cash"{
            self.performSegue(withIdentifier: "feedback", sender: self)
        }else{
            //Show CCAvenue for payment
            
            let controller =  self.storyboard?.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
            controller.merchantId = StringConstant.merchantId
            controller.amount =  "1.0"//bookingStatusBo.rideFareBo.totalCost //"1.0"
            controller.currency = StringConstant.currency
            controller.orderId = bookingStatusBo.bookingInfoBo.bookId
            controller.bookingStatusBo = self.bookingStatusBo
            
            controller.accessCode = StringConstant.liveCCAvenueAccessCode
            controller.redirectUrl = StringConstant.liveRedirectUrl
            controller.cancelUrl = StringConstant.liveCancelUrl
            controller.rsaKeyUrl = StringConstant.liversaKeyUrl
            
            print("Bookid::::\(bookingStatusBo.bookingInfoBo.bookId)")
            print("Total cost::::\(bookingStatusBo.rideFareBo.totalCost)")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "feedback"{
            let vc = segue.destination as! FeedbackViewController
            vc.bookingStatusBo = self.bookingStatusBo
        }
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookingStatusBo.rideFareBo.arrFareDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingDetails", for: indexPath as IndexPath) as! BillingDetailsTableViewCell
        let fareRideBo = bookingStatusBo.rideFareBo.arrFareDetails[indexPath.row]
        
        cell.lblRideFare.text = fareRideBo.title + "  :  "
        if fareRideBo.type == 0{
            cell.lblBillRideFare.text = String(Int(Double(fareRideBo.value)!))
        }else{
            cell.lblBillRideFare.text = "₹\(fareRideBo.value)"
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
  
}
