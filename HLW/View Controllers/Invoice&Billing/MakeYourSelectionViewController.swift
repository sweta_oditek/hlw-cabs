//
//  MakeYourSelectionViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 21/05/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit

protocol selectionDelegate: class{
    func selectedOption(customBo: CustomObject, type: String)
}

class MakeYourSelectionViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var mainViewContainer: UIView!
    @IBOutlet weak var tblViewSelection: UITableView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet var tblViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var viewContainerHeight: NSLayoutConstraint!
    
    weak var delegate: selectionDelegate?
    var type: String = ""
    var arrSelection = [CustomObject]()
    
    var tableViewheight: CGFloat {
        tblViewSelection.layoutIfNeeded()
        return tblViewSelection.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        
        tblViewHeightConstraint.constant = self.tableViewheight
        viewContainerHeight.constant = tblViewHeightConstraint.constant + 48
        
        self.viewContainer.layer.cornerRadius = 5
        self.viewContainer.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "MakeYourSelectionPage", screenClass: "MakeYourSelectionViewController")
    }
    
    //MARK: Tableview Degates & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSelection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTableViewCell", for: indexPath as IndexPath) as! SelectionTableViewCell
        
        let customBo = arrSelection[indexPath.row]
        cell.lblTitle.text = customBo.name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate?.selectedOption(customBo: arrSelection[indexPath.row], type: type)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDismissAction(_sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
