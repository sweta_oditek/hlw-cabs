//
//  CancelRidePopupViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 18/01/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class CancelRidePopupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblCancelRide: UITableView!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var btnDontCancel: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var popUpViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpViewTrailingConstraint: NSLayoutConstraint!
    
    var selectedIndex: Int = -1
    var arrCancelRideDesc = [CancelList]()
    var listPolicyId = CancelList()

    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        viewPopup.layer.cornerRadius = 10
        viewPopup.clipsToBounds = true
        
        popUpViewLeadingConstraint.constant = (AppConstant.screenSize.width * 10)/100
        popUpViewTrailingConstraint.constant = (AppConstant.screenSize.width * 10)/100
        
        tblCancelRide.tableFooterView = UIView()
        
        self.serviceCallToGetCancelList()
        btnCancel.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "CancelRidePage", screenClass: "CancelRidePopupViewController")
    }
    
    //MARK: Tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCancelRideDesc.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelRideTableViewCell", for: indexPath as IndexPath) as! CancelRideTableViewCell
        cell.selectionStyle = .none
        
        if indexPath.row == self.arrCancelRideDesc.count - 1{
            cell.viewSeparator.isHidden = true
        }
        
        let cancelListBo = self.arrCancelRideDesc[indexPath.row]
        cell.lblCancelRideDesc.text = cancelListBo.reasonText
        cell.imgViewRadio.image = (indexPath.row == selectedIndex) ? UIImage.init(named: "radio_checked") : UIImage.init(named: "radio_unchecked")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        listPolicyId = self.arrCancelRideDesc[indexPath.row]
        btnCancel.isEnabled = true
        tableView.reloadData()
        
    }
    
    //MARK: - Button Action
    @IBAction func btnDontCancelRideAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.removeFromParent()
                self.view.removeFromSuperview()
            }
        })
        
    }
    @IBAction func btnCancelRideAction(_ sender: UIButton) {
        self.serviceCallToCancelBooking()
    }
    
    //MARK: - Service Call Method
    func serviceCallToGetCancelList() {
        AppConstant.isBookingConfirm = false
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id), "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("url===\(AppConstant.getCancelationUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.getCancelationUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int{
                                if status == 1{
                                    if let dictData = dict["data"] as? [[String: Any]]{
                                        for dict in dictData{
                                            let cancelListData = CancelList()
                                            if let reason_text = dict["reason_text"] as? String{
                                                cancelListData.reasonText = reason_text
                                            }
                                            if let due_time = dict["due_time"] as? String{
                                                cancelListData.dueTime = due_time
                                            }
                                            if let amount = dict["amount"] as? String{
                                                cancelListData.amount = amount
                                            }
                                            if let policy_id = dict["id"] as? String{
                                                cancelListData.policyId = policy_id
                                            }
                                            self.arrCancelRideDesc.append(cancelListData)
                                        }
                                    }
                                    self.tblCancelRide.reloadData()
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToCancelBooking() {
        AppConstant.isBookingConfirm = false
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id), "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken), "policy_id": listPolicyId.policyId, "book_id": AppConstant.retrievFromDefaults(key: StringConstant.book_id)]
            
            print("url===\(AppConstant.cancelBookingUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.cancelBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{
                                    self.showAlert(title: "Success", desc: "Ride has been cancelled")
                                    
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    //MARK: Alert Method
    func showAlert(title: String, desc: String){
        
        let alert = UIAlertController(title: title, message: desc, preferredStyle: .alert)
        
        let okBtn = UIAlertAction(title: "OK", style: .default){ (action)in
            UIView.animate(withDuration: 0.25, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                self.view.alpha = 0.0
            }, completion: {(finished : Bool) in
                if(finished)
                {
                    self.willMove(toParent: nil)
                    self.removeFromParent()
                    self.view.removeFromSuperview()
                    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.goToMainScreen()
                }
            })
        }
        alert.addAction(okBtn)
        self.present(alert, animated: true, completion: nil)
    }

}
