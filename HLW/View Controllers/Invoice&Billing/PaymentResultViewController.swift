//
//  PaymentResultViewController.swift
//  HLW
//
//  Created by ODITEK on 06/11/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit

class PaymentResultViewController: UIViewController {

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgViewImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    var resultStatus: String = ""
    var resultDesc: String = ""
    var bookingStatusBo = BookingStatusBO()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        
        btnDone.layer.cornerRadius = 10
        btnDone.clipsToBounds = true
        
        if resultStatus == StringConstant.payuResultStatusSuccess {//Payment Success
            viewTop.backgroundColor = UIColor.init(hexString: "#2dd784")
            imgViewImage.image = UIImage(named: "payment_success")
            btnDone.backgroundColor = UIColor.init(hexString: "#2dd784")
            btnDone.setTitle("DONE", for: .normal)
        }else{//Payment Cancel
            viewTop.backgroundColor = UIColor.init(hexString: "#e34c5e")
            imgViewImage.image = UIImage(named: "payment_cancel")
            btnDone.backgroundColor = UIColor.init(hexString: "#e34c5e")
            btnDone.setTitle("GO BACK", for: .normal)
        }
        lblTitle.text = resultStatus
        lblData.text = resultDesc
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "PaymentResultPage", screenClass: "PaymentResultViewController")
    }
    
    //MARK: Button Action
    @IBAction func btnDoneAction(_ sender: Any) {
        if resultStatus == StringConstant.payuResultStatusSuccess {
            self.performSegue(withIdentifier: "FeedBack", sender: self)
        }else{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: BillViewController.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FeedBack"{
            let vc = segue.destination as! FeedbackViewController
            vc.bookingStatusBo = self.bookingStatusBo
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
