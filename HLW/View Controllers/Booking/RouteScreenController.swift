//
//  RouteScreenController.swift
//  Taxi Booking
//
//  Created by Chinmaya Sahu on 27/01/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import SwiftyJSON
import GooglePlaces
import DSGradientProgressView
import Windless
import SkeletonView
import NVActivityIndicatorView

class RouteScreenController: UIViewController , GMSMapViewDelegate , CLLocationManagerDelegate, GMSAutocompleteViewControllerDelegate, UITextFieldDelegate, ChooseDelegate, ChooseSeatsDelegate, ChoosePaymentModeDelegate, ApplyCouponDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var btnBookLater: UIButton!
    @IBOutlet weak var lblBookLaterInfo: UILabel!
    
    @IBOutlet weak var tblViewVehicleType: UITableView!
    @IBOutlet weak var btnBookNowWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblScheduleInfo: UILabel!
    @IBOutlet weak var lblScheduleHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblPickUpLocation: UILabel!
    @IBOutlet weak var lblDropLocation: UILabel!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet var viewPickUpLocation: UIView!
    @IBOutlet var viewDropLocation: UIView!
//    @IBOutlet var webViewGoogleMap: WKWebView!
    @IBOutlet var viewFindingDriver: UIView!
    @IBOutlet var viewConfirmBooking: UIView!
    @IBOutlet var progressBar: DSGradientProgressView!
    @IBOutlet var btnCancelRequest: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bookRideForView: UIView!
    @IBOutlet weak var paymentModeView: UIView!
    @IBOutlet weak var applyCouponView: UIView!
    @IBOutlet var viewContainerBottom: NSLayoutConstraint!
    @IBOutlet var viewDatePicker: UIView!
    @IBOutlet var viewDatePickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var dateTimePicker: UIDatePicker!
    @IBOutlet var viewbackground: UIView!
    @IBOutlet weak var lblBookRideFor: UILabel!
    @IBOutlet weak var lblBookRideForContact: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblApplyCoupon: UILabel!
    @IBOutlet weak var lblAppliedCoupon: UILabel!
    
    @IBOutlet var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var viewBottomBottomSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet var viewDriverInfo: UIView!

    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblVehicleNumber: UILabel!
    @IBOutlet weak var cancelRideBtn: UIButton!
    @IBOutlet weak var imgViewCoupon: UIImageView!
    @IBOutlet weak var lblPaymentModeOption: UILabel!
    @IBOutlet weak var lblPageTitle: UILabel!
    
    var currentCity : String!
    var vehicleType : String!
    
    var cityId : String = ""
    var vehicleTypeId : String = ""
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    var dropCoordinate : CLLocationCoordinate2D? = nil
    var pickUpAddress : String!
    var dropAddress : String!
    var locationManager = CLLocationManager()
    var arrLocations = [CLLocationCoordinate2D]()
    var arrCabInfo : [CabLocationBO] = []
    var arrCabTiming = [CabTimingBO]()
    
    var selectedVehicleTypeIndex: Int! = 0
    var bookNow_Status: String = ""
    var ride_Now: String = "0"
    var arrCabTypeInfo = [VehicleTypeBO]()
    var vehicleTypeBo = VehicleTypeBO()
    
    var bokingStatusBo = BookingStatusBO()
    var isAddressChanged : Bool = false
    var isForBookLater : Bool = false
    var isForBookShareRide : Bool = false
    var isCouponApplied: Bool = false
    var pickupTime: String = ""
    var waitingTime: String = ""
    var promoCode = CouponBO()
    
    //Draw animated path
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var index = 0
    var driverPins = [GMSMarker]()
    let driverPin = GMSMarker()
    var arrCabInfoBackUp = [CabLocationBO]()
    
    var oldCabCoordinate : CLLocationCoordinate2D? = nil
    
    var movingcar = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
        
        print("View Confirm Booking = \(viewConfirmBooking.frame.size.height)")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleMapView.clear()
        
        //Draw route between two locations
        drawPolinine()
        
        setBoundsForMap(forInitial: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookingErrorNotification(notification:)), name: Notification.Name("BookingErrorNotification"), object: nil)
        
        AppConstant.setTrackScreenName(screenName: "OnewayBookingPage", screenClass: "RouteScreenController")
    }
    
    override func viewDidLayoutSubviews() {
        topView.setShadowWithCornerRadius(corners: 20.0)
    }
    
    func initDesign(){
        
        lblPaymentModeOption.text = AppConstant.selectedPaymentMode
        
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            navBarHeightConstraint.constant = 92
            viewBottomBottomSpaceConstraint.constant = 34
        }
        
        mapViewHeightConstraint.constant = AppConstant.screenSize.height / 3
        
        self.topView.isHidden = true
        
        tblViewVehicleType.delegate = self
        tblViewVehicleType.dataSource = self
        tblViewVehicleType.tableFooterView = UIView()
        
        lblScheduleHeightConstraint.constant = 0
        
        //Set MySelf default for Book ride
        AppConstant.bookRideForContactSelectedStatus = "1"
        
        //Hide PickupTime view
        if (isForBookLater == false) && (isForBookShareRide == false){
            viewContainerBottom.constant = 0
        }
        
        //Service call to get cab locations
        self.serviceCallToGetAllCabLocation()
        
        //Hide date Picker initially
        viewDatePicker.isHidden = true
        viewDatePickerBottomConstraint.constant = -190
        
        googleMapView.delegate = self
//        lblDropLocation.delegate = self
        
        self.lblPickUpLocation.text = self.pickUpAddress
        self.lblDropLocation.text = self.dropAddress
        
        viewPickUpLocation.layer.cornerRadius = 3
        viewPickUpLocation.clipsToBounds = true
        viewDropLocation.layer.cornerRadius = 3
        viewDropLocation.clipsToBounds = true
        
        googleMapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        self.googleMapView.settings.myLocationButton = false
        
        arrLocations.append(pickUpCoordinate!)
        arrLocations.append(dropCoordinate!)
        
        if (AppConstant.currentBookRideForSelectedType == "") {
            lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
            lblBookRideForContact?.text = "Myself"
        }
        else {
            if (AppConstant.currentBookRideForSelectedType == "book_ride_for") {
                lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                lblBookRideForContact?.text = "Others"
            }
            else {
                lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
                lblBookRideForContact?.text = "Myself"
            }
        }
        
        if (AppConstant.screenSize.height <= 568) {
            lblApplyCoupon.font = UIFont.init(name: "Poppins-Regular", size: 9.0)
            lblPaymentMode.font = UIFont.init(name: "Poppins-Regular", size: 9.0)
            lblBookRideFor.font = UIFont.init(name: "Poppins-Regular", size: 9.0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(RouteScreenController.getBookingConfirmation(notification:)), name: Notification.Name("reloadNotification"), object: nil)
        
        var tap1 = UITapGestureRecognizer(target: self, action: #selector(self.setPickupTime))
        if isForBookShareRide {
            tap1 = UITapGestureRecognizer(target: self, action: #selector(self.chooseSeats))
        }
        
        // Adding webView content
//        do {
//            guard let filePath = Bundle.main.path(forResource: "googleMapTrafficDirection", ofType: "html")
//                else {
//                    // File Error
//                    print ("File reading error")
//                    return
//            }
//
//            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
//            let baseUrl = URL(fileURLWithPath: filePath)
//            webViewGoogleMap.loadHTMLString(contents as String, baseURL: baseUrl)
//        }
//        catch {
//            print ("File HTML error")
//        }
        
        selectedVehicleTypeIndex = 0
    }
    
    func setCabType(id: String) -> VehicleTypeBO{
        let vehicleBo = VehicleTypeBO()
        switch id {
        case "16":
            vehicleBo.name = "Auto"
            vehicleBo.selectedImage = StringConstant.auto_blue
            break
        case "2":
            vehicleBo.name = "Micro"
            vehicleBo.selectedImage = StringConstant.car_micro_blue
            break
        case "3":
            vehicleBo.name = "Mini"
            vehicleBo.selectedImage = StringConstant.car_mini_blue
            break
        case "5":
            vehicleBo.name = "Sedan"
            vehicleBo.selectedImage = StringConstant.car_sedan_blue
            break
        case "15":
            vehicleBo.name = "SUV"
            vehicleBo.selectedImage = StringConstant.car_suv_blue
            break
        default:
            break
        }
        return vehicleBo
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TableView Delegates and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCabTypeInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleType", for: indexPath as IndexPath) as! OnewayCabTypeTableViewCell
        
        let vehicleTypeBo = arrCabTypeInfo[indexPath.row]
        
        let vehicle = self.setCabType(id: vehicleTypeBo.vehicleId)
        cell.lblCabName.text = vehicle.name
        cell.imgViewCab.image = UIImage.init(named: vehicle.selectedImage)
        
        cell.lblNoOfCabs.text = vehicleTypeBo.vehicle_Cnt
        cell.lblCabTiming.text = vehicleTypeBo.minTime == "0" ? "No \(vehicle.name!) Available" : "\(vehicleTypeBo.minTime) away"
        waitingTime = cell.lblCabTiming.text!
        
        cell.btnTotalFare.tag = indexPath.row
        cell.btnTotalFare.addTarget(self, action: #selector(btnTotalFareAction(_:)), for: .touchUpInside)
        
        cell.lblTotalTax.text = "₹ \(vehicleTypeBo.totalFare)"
        
        if indexPath.row == selectedVehicleTypeIndex{
            cell.viewCabContents.backgroundColor = AppConstant.colorThemeLightBlue
        }else{
            cell.viewCabContents.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedVehicleTypeIndex = indexPath.row
        self.tblViewVehicleType.reloadData()
        self.serviceCallToGetCabLocation()
    }
    
    func showGooglePlacesAutoCompleteViewController() {
        let placeholderAttributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
        let attributedPlaceholder = NSAttributedString(string: "Enter drop location", attributes: placeholderAttributes)
        if let aClass = [UISearchBar.self] as? [UIAppearanceContainer.Type] {
            if #available(iOS 9.0, *) {
                UITextField.appearance(whenContainedInInstancesOf: aClass).attributedPlaceholder = attributedPlaceholder
            } else {
                // Fallback on earlier versions
            }
        }
        
        let acController = GMSAutocompleteViewController()
        let bounds: GMSCoordinateBounds = getCoordinateBounds(latitude: (pickUpCoordinate?.latitude)!, longitude: (pickUpCoordinate?.longitude)!)
//        acController.autocompleteBounds = GMSCoordinateBounds(coordinate: bounds.northEast, coordinate: bounds.southWest)
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    /* Returns Bounds */
    func getCoordinateBounds(latitude:CLLocationDegrees ,
                             longitude:CLLocationDegrees,
                             distance:Double = 0.01)->GMSCoordinateBounds{
        let center = CLLocationCoordinate2D(latitude: latitude,
                                            longitude: longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + distance, longitude: center.longitude + distance)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - distance, longitude: center.longitude - distance)
        
        return GMSCoordinateBounds(coordinate: northEast,
                                   coordinate: southWest)
        
    }
    
    // MARK: - AutocompleteViewController Delegate
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("Place latitude: \(String(describing: place.coordinate.latitude))")
        print("Place longitude: \(String(describing: place.coordinate.longitude))")
        self.lblDropLocation.text = place.formattedAddress
        self.dropCoordinate = place.coordinate
        
        //Update the location array
        arrLocations.removeLast()
        arrLocations.append(dropCoordinate!)
        
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude ,longitude: place.coordinate.longitude , zoom: 13)
        self.googleMapView.animate(to: camera)
        
        self.isAddressChanged = true
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // handle the error.
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Google Map Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.myLocationButton = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        locationManager.stopUpdatingLocation()
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                print(location)
                
                if error != nil {
                    print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    return
                }
                
                if (placemarks!.count) > 0 {
                    let pm = placemarks?[0]
                    print("you are in city ->",String(describing: pm!.locality))
                    self.currentCity = pm!.locality
                    
                    if (self.isAddressChanged == true) {
                        self.isAddressChanged = false
//                        self.serviceCallToGetFareDetails()
                    }
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func handleUserCurrentLocationTap(_ sender: UITapGestureRecognizer? = nil) {
        
        guard let lat = self.googleMapView.myLocation?.coordinate.latitude,
            let lng = self.googleMapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 13)
        self.googleMapView.animate(to: camera)
        
    }
    
    func selectVehicleType(index: Int){
        googleMapView.clear()
        //draw polyline
        drawPolinine()
        isForBookShareRide = false
        
        for cabBo in arrCabInfo {
            let coordinateNew = CLLocationCoordinate2D(latitude: Double(cabBo.newLatitude!)!
                , longitude: Double(cabBo.newLongitude!)!)
            
            if oldCabCoordinate == nil{
                oldCabCoordinate = coordinateNew
            }
            
            movingcar = self.image(with: getCabImageFromId(id: arrCabTypeInfo[selectedVehicleTypeIndex].vehicleId), scaledTo: CGSize(width: 35.0, height: 35.0))
            
            animateVehiceLocation(oldCoodinate: oldCabCoordinate!, newCoodinate: coordinateNew, image: movingcar, pin: cabBo.vehiclepin, bearing: Int(cabBo.bearing!)!)
        }
        
        selectedVehicleTypeIndex = index
        self.tblViewVehicleType.reloadData()
    }
    
    func getCabImageFromId(id: String) -> UIImage{
        var image: UIImage = UIImage.init(named: StringConstant.micro_car_map_pin)!
        
        if id == "2"{
            image = UIImage.init(named: StringConstant.micro_car_map_pin)!
        }else if id == "3"{
            image = UIImage.init(named: StringConstant.mini_car_map_pin)!
        }else if id == "5"{
            image = UIImage.init(named: StringConstant.sedan_car_map_pin)!
        }else if id == "15"{
            image = UIImage.init(named: StringConstant.suv_car_map_pin)!
        }else{
            image = UIImage.init(named: StringConstant.auto_gray_map_pin)!
        }
        return image
    }
    
    // MARK: function for create a marker pin on map
    
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: String, longitude: String) {
        let lat = Double(latitude)
        let lon = Double(longitude)

        let coordinates = CLLocationCoordinate2D(latitude:lat!
            , longitude:lon!)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
        marker.title = titleMarker
        marker.icon = self.image(with: iconMarker, scaledTo: CGSize(width: 14.0, height: 30.0))
        marker.map = googleMapView
    }

    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    // MARK: - Animation Method
    func animateVehiceLocation(oldCoodinate: CLLocationCoordinate2D , newCoodinate: CLLocationCoordinate2D, image: UIImage, pin : GMSMarker, bearing: Int) {
        index = index + 1
        print("Index ===\(index)")
        print("Driver location New ===\(newCoodinate)")
        print("Driver location Old ===\(oldCoodinate)")
        
        pin.icon = image
        pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        pin.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
        //found bearing value by calculation when marker add
//        pin.position = oldCoodinate
        //this can be old position to make car movement to new position
        pin.map = self.googleMapView
        //marker movement animation
        CATransaction.begin()
        CATransaction.setValue(Int(5.0), forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
            pin.rotation = CDouble(bearing)
            
        })
        pin.position = newCoodinate
        //this can be new position after car moved from old position to new position with animation
        pin.map = self.googleMapView
        pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        pin.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
        pin.rotation = CDouble(bearing)
        //found bearing value by calculation
        CATransaction.commit()
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    
    // MARK: Set bounds for Map
    func setBoundsForMap(forInitial: Bool) {
        var bounds = GMSCoordinateBounds()
        for var i in (0..<arrLocations.count)
        {
            let location = arrLocations[i]
            
            let latitude = location.latitude
            let longitude = location.longitude
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude:latitude, longitude:longitude)
            marker.map = self.googleMapView
            if i == 1 {
                marker.icon = UIImage.init(named: StringConstant.dropPin)
            }else{
                marker.icon = UIImage.init(named: StringConstant.pickUpPin)
            }
            
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 10)
        
        if forInitial{
            self.googleMapView.animate(with: update)
        }
        
    }
    
    //MARK: - Draw direction path, from start location to desination location
    func drawPolinine(){
        //Draw route between two locations
        if self.dropCoordinate != nil{//Manas
            self.drawPath(startLocation: self.pickUpCoordinate!, endLocation: self.dropCoordinate!)
        }
    }
    func drawPath(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D)
    {
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(AppConstant.GoogleMapApiKey)"
        
        AF.request(url).responseJSON { response in
            
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result as Any)   // result of response serialization
            
            if let json = try? JSON(data: response.data!) {
                let routes = json["routes"].arrayValue
                
                // print route using Polyline
                for route in routes
                {
                    let arrlegs = route["legs"].arrayValue
                    let dict = arrlegs[0]
                    let distanceDict = dict["distance"].dictionary
                    let distance = distanceDict!["text"]?.string
                    print(distance as Any)
                    let durationDict = dict["duration"].dictionary
                    let duration = durationDict!["text"]?.string
                    print(duration as Any)
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    self.path = GMSPath.init(fromEncodedPath: points!)!
                    self.polyline = GMSPolyline.init(path: self.path)
                    self.polyline.strokeWidth = 3
                    self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
                    
                    self.polyline.map = self.googleMapView
                }
            }
            
        }
    }
    
    @objc func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.googleMapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    /*
    func showMultipleRoute(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D)
    {
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(AppConstant.GoogleMapApiKey)"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.googleMapView.clear()
                    
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 3
                            
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.googleMapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            
                            polyline.map = self.googleMapView
                            
                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
    }*/
    
    //MARK: - Button Action
    @objc func bookingErrorNotification(notification: Notification){
        mapViewHeightConstraint.constant = 200
        self.viewFindingDriver.isHidden = true
        self.viewConfirmBooking.isHidden = false
        lblPageTitle.text = "One Way"
    }
    
    @IBAction func btnBookNowAction(_ sender: Any) {
        let vehicleType = self.setCabType(id: arrCabTypeInfo[selectedVehicleTypeIndex].vehicleId)
        
        if (waitingTime == "No \(vehicleType.name!) Available") {
            self.showAlert(alertTitle: "Alert!", alertMessage: "No Cab available of \(vehicleType.name!) category.Please check cab in other category")
            return
        }
        
        if btnBookNow.title(for: .normal) == "BOOK NOW" {
            bookNow_Status = "1"
            ride_Now = "0"
            
            btnBookNow.isEnabled = true
            //Move to DriverInfo Screen
            mapViewHeightConstraint.constant = AppConstant.screenSize.height - (titleView.frame.size.height + viewFindingDriver.frame.size.height)
            
            self.viewConfirmBooking.isHidden = true
            self.viewFindingDriver.isHidden = false
            self.progressBar.wait()
            lblPageTitle.text = "Finding a ride"

            viewDriverInfo.showAnimatedGradientSkeleton()
            lblDriverName.showAnimatedGradientSkeleton()
            lblVehicleName.showAnimatedGradientSkeleton()
            lblVehicleNumber.showAnimatedGradientSkeleton()
            lblRating.showAnimatedGradientSkeleton()
            cancelRideBtn.showAnimatedGradientSkeleton()
            
        }else{
            bookNow_Status = "2"
            ride_Now = "1"
        }
        
        self.serviceCallToConfirmBooking()
    }
    
    @IBAction func btnBookLaterAction(_ sender: Any) {

        if isForBookLater == true{//Cancel
            isForBookLater = false
            lblScheduleInfo.isHidden = true
            lblScheduleHeightConstraint.constant = 0
            btnBookNow.setTitle("BOOK NOW", for: .normal)
            btnBookLater.setTitle("BOOK LATER", for: .normal)
            lblBookLaterInfo.isHidden = false
            self.serviceCallToGetAllCabLocation()
        }else{//BookLater
            dateTimePicker.minimumDate = Date()
            dateTimePicker.maximumDate = Date().add(days: 2)//User can book a cab before 2 days only
            viewDatePicker.animShow()
            
            viewbackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            viewbackground.isHidden = false
        }
    }
    
    @IBAction func btnDropLocationSearchAction(_ sender: Any) {
        showGooglePlacesAutoCompleteViewController()
    }
    
    
    @IBAction func btnCancelRequestAction(_ sender: Any) {
        self.showAlertForCancelRequest(strTitle: "Alert!", strDescription: "Are you sure you want to cancel the ride?", delegate: self)
    }

    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func totalFareViewAction(_ sender: Any) {
    }
    
    @objc func btnTotalFareAction(_ sender: UIButton){
        vehicleTypeBo = arrCabTypeInfo[sender.tag]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FareDetailsViewController") as! FareDetailsViewController
        vc.arrFareDettails = vehicleTypeBo.arrFareDettails
        vc.totalFare = vehicleTypeBo.totalFare
        vc.info = vehicleTypeBo.info
        vc.taxValue = vehicleTypeBo.taxValue
        vc.vehicleTypeId = vehicleTypeBo.vehicleId
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        vc.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        vc.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            vc.view.alpha = 1.0
            vc.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        self.addChild(vc)
        self.view.addSubview(vc.view)
    }
    
    @IBAction func viewBookRideForAction(_ sender: Any) {
        viewbackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewbackground.isHidden = false
        self.performSegue(withIdentifier: "book_ride_for", sender: self)
    }
    
    @IBAction func viewPaymentModeAction(_ sender: Any) {
        viewbackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewbackground.isHidden = false
        self.performSegue(withIdentifier: "payment_mode", sender: self)
    }
    
    @IBAction func viewApplyCouponAction(_ sender: Any) {
        self.performSegue(withIdentifier: "apply_coupon", sender: self)
    }
    
    @objc func chooseSeats() {
        viewbackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewbackground.isHidden = false
        self.performSegue(withIdentifier: "choose_seats", sender: self)
    } 
    
    @objc func setPickupTime() {
        dateTimePicker.minimumDate = Date()
        dateTimePicker.maximumDate = Date().add(days: 2)//User can book a cab before 2 days only
        viewDatePicker.animShow()
        
        viewbackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewbackground.isHidden = false
    }
    
    @IBAction func datePickerCancelBtnAction(_ sender: Any) {
        viewDatePicker.animHide()
        viewbackground.isHidden = true
    }
    
    @IBAction func datePickerDoneBtnAction(_ sender: Any) {
        btnBookNow.setTitle("SCHEDULE NOW", for: .normal)
        btnBookLater.setTitle("CANCEL", for: .normal)
        lblBookLaterInfo.isHidden = true
        
        viewDatePicker.animHide()
        viewbackground.isHidden = true
        isForBookLater = true

        pickupTime = AppConstant.formattedDate(date: dateTimePicker.date, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter4)!

        let attributedText = NSMutableAttributedString(string: "Schedule Date and Time:   ", attributes: [NSAttributedString.Key.font: UIFont.init(name: StringConstant.appFontPoppinsBold, size: 13)!])
        let attributedTxt = NSMutableAttributedString(string: pickupTime, attributes: [NSAttributedString.Key.font: UIFont.init(name: StringConstant.appFontPoppinsRegular, size: 12)!])
        attributedText.append(attributedTxt)
        
        lblScheduleInfo.attributedText = attributedText
        
        lblScheduleInfo.isHidden = false
        lblScheduleHeightConstraint.constant = 35
    }
    
    @objc func getBookingConfirmation(notification: Notification){
        self.serviceCallToGetBookingStatusDetails()
    }
    
    //MARK: Choose Contact Protocol Delegates
    func selectedObject(obj: BookRideForContactBO, type: String) {
        viewbackground.isHidden = true
        AppConstant.currentBookRideForSelectedType = type
        if (type == "book_ride_for") {
            if (obj.name == nil) {
                if (AppConstant.bookRideForContactName == "") {
                    self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
                    self.lblBookRideForContact?.text = "Myself"
                    AppConstant.bookRideForContactSelectedStatus = "1"
                }
                else {
                    self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                    self.lblBookRideForContact?.text = "Others"
                    AppConstant.bookRideForContactSelectedStatus = "2"
                }
            }
            else {
                self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                self.lblBookRideForContact?.text = obj.name!
                AppConstant.bookRideForContactSelectedStatus = "2"
            }
        }
        else if (type == "Myself") {
            self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
            self.lblBookRideForContact?.text = "Myself"
            AppConstant.bookRideForContactSelectedStatus = "1"
        }
    }
    
    //MARK: Choose Seats Protocol Delegates
    func selectedSeats(seats: Int) {
        viewbackground.isHidden = true
//        lblPickupTime.text = String(seats)
    }
    
    //MARK: Applied Coupon Protocol Delegates
    func appliedCoupon(couponBo: CouponBO) {
        promoCode = couponBo
        isCouponApplied = true
        lblAppliedCoupon.text = couponBo.title
        imgViewCoupon.isHidden = true
        lblAppliedCoupon.isHidden = false
        lblApplyCoupon.text = "Coupon Applied"
        self.serviceCallToGetAllCabLocation()
    }
    
    //MARK: Choose Payment Mode Protocol Delegates
    func selectedPaymentMode(pMode: String) {
        viewbackground.isHidden = true
        
        AppConstant.selectedPaymentMode = pMode
        lblPaymentModeOption?.text = pMode
    }
    
    // MARK: - Show Animation Method
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    // MARK: - Service Call Method
    func serviceCallToGetAllCabLocation() {
        if AppConstant.hasConnectivity() {
             AppConstant.showHUD()
            
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            let dropLat : NSNumber = NSNumber(value: (dropCoordinate?.latitude)!)
            let dropLng : NSNumber = NSNumber(value: (dropCoordinate?.longitude)!)
            
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "city": self.currentCity!,
                "src_lat": String(describing: pickLat),
                "src_lon": String(describing: pickLng),
                "des_lat": String(describing: dropLat),
                "des_lon": String(describing: dropLng),
                "ride_id": "0",
                "promo_code": isCouponApplied ? promoCode.title : "",
                "ride_now": self.ride_Now,
                "schedule_date": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter2)! : "",
                "schedule_time": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter6)! : ""
            ]
            
            print("params===\(params)")
            print("Url===\(AppConstant.allCabLocationUrl)")
            
            
            AF.request(AppConstant.allCabLocationUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int{
                                self.arrCabTypeInfo.removeAll()
                                if status == 1{
                                    if let arrCabDetails = dict["data"] as? [[String: Any]]{
                                        for dictData in arrCabDetails{
                                            var vehicleDetailsDataBo = VehicleTypeBO()
                                            if let cat_Id = dictData["cat_id"] as? String{
                                                vehicleDetailsDataBo.vehicleId = cat_Id
                                            }
                                            if let vehicle_cnt = dictData["vehicle_cnt"] as? Int{
                                                vehicleDetailsDataBo.vehicle_Cnt = String(vehicle_cnt)
                                            }
                                            if let time = dictData["time"] as? String{
                                                vehicleDetailsDataBo.minTime = time
                                            }
                                            if let esti_Data = dictData["esti_data"] as? [String: Any]{
                                                if let status = esti_Data["status"] as? String{
                                                    if status == "1"{
                                                        if let totalFare = esti_Data["cost"] as? Int {
                                                            vehicleDetailsDataBo.totalFare = String(totalFare)
                                                            //   self.lblTotalFare.text = String(format: "₹ %@/-", self.totalFare)
                                                        }
                                                        if let taxValue = esti_Data["tax"] as? String {
                                                            vehicleDetailsDataBo.taxValue = taxValue
                                                        }
                                                        if let info = esti_Data["info_str"] as? String {
                                                            vehicleDetailsDataBo.info = info
                                                        }
                                                        vehicleDetailsDataBo.arrFareDettails.removeAll()
                                                        if let fareInfoArray = esti_Data["info_arry"] as? [[String: Any]]{
                                                            for item in fareInfoArray {
                                                                let fareDetailsBo = FareDetails()
                                                                
                                                                if let titleKey = item["key"] as? String {
                                                                    fareDetailsBo.title = titleKey.replacingOccurrences(of: "&#x20b9;", with: "₹ ")
                                                                }
                                                                if let value = item["value"] as? String {
                                                                    fareDetailsBo.price = value
                                                                }
                                                                if let value = item["value"] as? Double {
                                                                    fareDetailsBo.price = String(value)
                                                                }
                                                                if let type = item["type"] as? Int{
                                                                    fareDetailsBo.type = type
                                                                }
                                                                vehicleDetailsDataBo.arrFareDettails.append(fareDetailsBo)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            self.arrCabTypeInfo.append(vehicleDetailsDataBo)

                                        }
                                    }
                                    self.serviceCallToGetCabLocation()
                                }
                            }
                            self.tblViewVehicleType.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetCabLocation() {
        if arrCabTypeInfo.count == 0{
            return
        }
        
        if AppConstant.hasConnectivity() {
            //AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "cat_id" : arrCabTypeInfo[selectedVehicleTypeIndex].vehicleId,
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!)
            ]
            
            print("url===\(AppConstant.cabLocationInfoUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.cabLocationInfoUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    //AppConstant.hideHUD()
                    debugPrint("Cab Location : \(response)")
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            print(AppConstant.cabLocationInfoUrl)
                            debugPrint(dict)
                            
                            if let status = dict["status"] as? Int {
                                if status == 1 {
                                    self.arrCabInfo.removeAll()
                                    if let arrCabLocationInfo = dict["data"] as? [[String: Any]]{
                                        if(arrCabLocationInfo.count > 0){
                                            for dictInfo in arrCabLocationInfo{
                                                
                                                let cabInfoBo = CabLocationBO()
                                                if let accuracy = dictInfo["accuracy"] as? String {
                                                    cabInfoBo.accuracy = accuracy
                                                }
                                                if let bearing = dictInfo["bearing"] as? String {
                                                    cabInfoBo.bearing = bearing
                                                }
                                                if let latitude = dictInfo["latitude"] as? String {
                                                    cabInfoBo.newLatitude = latitude
                                                }
                                                if let longitude = dictInfo["longitude"] as? String {
                                                    cabInfoBo.newLongitude = longitude
                                                }
                                                self.arrCabInfo.append(cabInfoBo)
                                                
                                            }
                                            //Set default Vehicle Type
                                            
                                            if (self.arrCabInfo.count > 0){
                                                self.selectVehicleType(index: self.selectedVehicleTypeIndex)
                                            }
                                        }else{
                                            self.googleMapView.clear()
                                            
                                            //draw polyline
                                            self.drawPolinine()
                                        }
                                    }
                                    
                                    self.setBoundsForMap(forInitial: false)
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                            self.tblViewVehicleType.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToConfirmBooking() {
        if AppConstant.hasConnectivity() {
            if bookNow_Status == "2" {
                AppConstant.showHUD()
            }else{
                DispatchQueue.main.async {
                    self.viewFindingDriver.isHidden = false
                    self.progressBar.wait()
                }
            }
            
            let pickUPAddress = self.lblPickUpLocation.text!
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            let dropAddress = self.lblDropLocation.text!
            let dropLat : NSNumber = NSNumber(value: (dropCoordinate?.latitude)!)
            let dropLng : NSNumber = NSNumber(value: (dropCoordinate?.longitude)!)
            
            let params: Parameters = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "book_now" : bookNow_Status,
                "book_for" : lblBookRideForContact?.text == "Myself" ? "1" : "2",
                "pay_mode" : AppConstant.selectedPaymentMode == "Cash" ? "1" : "2",
                "ride_id" : "0",
                "cat_id" : arrCabTypeInfo[selectedVehicleTypeIndex].vehicleId,
                "city" : self.currentCity!,
                "src_lat" : String(describing: pickLat),
                "src_lon" : String(describing: pickLng),
                "des_lat" : String(describing: dropLat),
                "des_lon" : String(describing: dropLng),
                "src_loc" : String(describing: pickUPAddress),
                "des_loc" : String(describing: dropAddress),
                "promo_code" : isCouponApplied ? promoCode.title : "",
                "oth_mobile": lblBookRideForContact?.text == "Myself" ? "" : AppConstant.bookRideForContactNumber,
                "schedule_date": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter2)! : "",
                "schedule_time": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter6)! : "",
                "fare": vehicleTypeBo.totalFare
            ]
            
            print("Url===\(AppConstant.confirmBookingUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.confirmBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    
                    if self.bookNow_Status == "2" {
                        AppConstant.hideHUD()
                    }else{
                        DispatchQueue.main.async {
                            self.viewFindingDriver.isHidden = false
                            self.progressBar.signal()
                        }
                    }
                    
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                        debugPrint(dict)
                            
                        if let status = dict["status"] as? String {
                                if(status == "0"){
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                    
                                }else  if(status == "1"){//BookNow
                                    if let bookingId = dict["book_id"] as? String {
                                        AppConstant.saveInDefaults(key: StringConstant.book_id, value: bookingId)
                                    }
                                    if let waitTime = dict["wait_time"] as? Int {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 90) { // change 30 to desired number of seconds
                                            //Hide Loader
                                            self.viewFindingDriver.isHidden = true
                                            self.progressBar.signal()
                                            
                                            if(!AppConstant.isBookingConfirm){
                                                //Call api to confirm your booking
                                                self.serviceCallToGetBookingStatusDetails()
                                                
                                            }
                                        }
                                    }
                                }else  if(status == "2"){//Schedule Booking
                                    let msg = dict["msg"] as? String
                                    let alert = UIAlertController(title: msg, message: "", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .cancel) { action in
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.goToMainScreen()
                                    })
                                    alert.view.tintColor = AppConstant.colorThemeBlue
                                    self.present(alert, animated: true)
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToCancelBooking() {
        AppConstant.isBookingConfirm = false
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id), "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken), "policy_id": "0", "book_id": AppConstant.retrievFromDefaults(key: StringConstant.book_id)]
            
            print("url===\(AppConstant.cancelBookingUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.cancelBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    DispatchQueue.main.async {
                        self.viewFindingDriver.isHidden = true
                        self.progressBar.signal()
                    }
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                    self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetBookingStatusDetails() {
        if AppConstant.hasConnectivity() {
            DispatchQueue.main.async {
                self.viewFindingDriver.isHidden = false
                self.progressBar.wait()
            }
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "book_id" : AppConstant.retrievFromDefaults(key: StringConstant.book_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("params===\(params!)")
            print("Api===\(AppConstant.getBookingStatusDetailsUrl)")
            
            AF.request( AppConstant.getBookingStatusDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    DispatchQueue.main.async {
                        self.viewConfirmBooking.isHidden = false
                        self.mapViewHeightConstraint.constant = 200
                        self.viewFindingDriver.isHidden = true
                        self.progressBar.signal()
                    }
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if(status == "1"){//Success
                                    if let bookStatus = dict["book_status"] as? String{
                                        if bookStatus == StringConstant.cancelByAdmin{
                                            if let book_Status_str = dict["book_status_str"] as? String{
                                                self.viewConfirmBooking.isHidden = false
                                                self.mapViewHeightConstraint.constant = 200
                                                self.showAlert(alertTitle: book_Status_str, alertMessage: "")
                                            }
                                        }else if bookStatus == StringConstant.bookingConfirm {
                                            if let dictBookInfo = dict["book_info"] as? [String: Any]{
                                                if let bookOtp = dictBookInfo["book_otp"] as? String{
                                                    self.bokingStatusBo.driverBo.otp = bookOtp
                                                }
                                                if let pickPnt = dictBookInfo["pick_pnt"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.pick_pnt = pickPnt
                                                }
                                                if let dropPnt = dictBookInfo["drop_pnt"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.drop_pnt = dropPnt
                                                }
                                                if let srcLat = dictBookInfo["src_lat"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.src_lat = srcLat
                                                }
                                                if let srcLon = dictBookInfo["src_lon"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.src_lon = srcLon
                                                }
                                                if let desLat = dictBookInfo["des_lat"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.des_lat = desLat
                                                }
                                                if let desLon = dictBookInfo["des_lon"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.des_lon = desLon
                                                }
                                                if let rideId = dictBookInfo["ride_id"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.ride_id = rideId
                                                }
                                                if let rideName = dictBookInfo["ride_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.ride_name = rideName
                                                }
                                                if let catName = dictBookInfo["cat_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.cat_name = catName
                                                }
                                                if let waitTime = dictBookInfo["wait_time"] as? Int{
                                                    self.bokingStatusBo.bookingInfoBo.wait_time = waitTime
                                                }
                                                if let rideCompleted = dictBookInfo["ride_completed"] as? String{
                                                    if rideCompleted == ""{
                                                        self.bokingStatusBo.bookingInfoBo.ride_completed = 0
                                                    }else{
                                                    self.bokingStatusBo.bookingInfoBo.ride_completed = Int(rideCompleted)!
                                                    }
                                                }
                                                
                                            }
                                            if let dictDriverInfo = dict["driver_info"] as? [String: Any]{
                                                if let driverId = dictDriverInfo["driver_id"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_id = driverId
                                                }
                                                if let driverName = dictDriverInfo["driver_name"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_name = driverName
                                                }
                                                if let driverMobile = dictDriverInfo["driver_mobile"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_mobile = driverMobile
                                                }
                                                if let driverRating = dictDriverInfo["driver_rating"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_rating = driverRating
                                                }
                                                if let driverImage = dictDriverInfo["driver_image"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_image = driverImage
                                                }
                                                if let vehcile_reg_No = dictDriverInfo["vehcile_reg_no"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_registration_number = vehcile_reg_No
                                                }
                                                if let vehcile_brand_Name = dictDriverInfo["vehcile_brand_name"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_brand_name = vehcile_brand_Name
                                                }
                                                if let vehcile_model_Name = dictDriverInfo["vehcile_model_name"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_model_name = vehcile_model_Name
                                                }
                                                if let vehicleImage = dictDriverInfo["vehicle_image"] as? String {
                                                    self.bokingStatusBo.driverBo.vehicle_image = vehicleImage
                                                }
                                            }
                                            
                                            //Move to DriverInfo Screen
                                            self.performSegue(withIdentifier: "driverToReachPickupPoint", sender: self)
                                            
                                        }else{
                                            self.viewConfirmBooking.isHidden = false
                                            self.mapViewHeightConstraint.constant = 200
                                        }
                                        
                                    }
                                    
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToVerifyToken() {
        if AppConstant.hasConnectivity() {
            //AppConstant.showHUD()
            var params: Parameters!
            params = [
                "token" : AppConstant.retrievFromDefaults(key: "token"),
                "user_id" : AppConstant.retrievFromDefaults(key: "user_id")
            ]
            print("params===\(params)")
            
            AF.request( AppConstant.tokenVarificationUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [[String: Any]]{
                            //debugPrit(dict)
                            let dataArray = dict[0]
                            
                            if let status = dataArray["status"] as? Int {
                                if(status == 0){
                                    let msg = dataArray["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                }else  if(status == 1){
                                   // self.serviceCallToGetFairDetails()
                                }else  if(status == 2){
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.goToLandingScreen()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    //MARK: Alert Method
    func showAlertForCancelRequest(strTitle: String,strDescription: String,delegate: AnyObject?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: strTitle, message: strDescription, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (action) in
                self.serviceCallToCancelBooking()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "driverToReachPickupPoint"{
            let vc = segue.destination as! DriverToReachPickUpPointViewController
            vc.pickUpCoordinate = self.pickUpCoordinate
            vc.dropCoordinate = self.dropCoordinate
            vc.pickUpAddress = self.lblPickUpLocation.text
            vc.dropAddress = self.lblDropLocation.text
            vc.bookingStatusBo = self.bokingStatusBo
        }
        else if (segue.identifier == "book_ride_for"){
            let vc = segue.destination as! BookRideForViewController
            vc.delegate = self
            vc.type = segue.identifier!
        }
        else if (segue.identifier == "choose_seats"){
            let vc = segue.destination as! ChooseSeatsViewController
            vc.delegate = self
        }
        else if (segue.identifier == "payment_mode"){
            let vc = segue.destination as! PaymentModeViewController
            vc.delegate = self
        }
        else if (segue.identifier == "apply_coupon"){
            let vc = segue.destination as! ApplyCouponViewController
            vc.delegate = self
            vc.selectedCouponBo = self.promoCode
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
 //       self.timer.invalidate()
    }
}

extension UIView {
    func dropShadow(offsetX: CGFloat, offsetY: CGFloat, color: UIColor, opacity: Float, radius: CGFloat, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: offsetX, height: offsetY)
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
