//
//  RentalBookingRidesViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 1/7/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import DSGradientProgressView
import NVActivityIndicatorView

class RentalBookingRidesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ChooseDelegate, ChoosePaymentModeDelegate, ApplyCouponDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var rentalBookingRidesTableView: UITableView!
    @IBOutlet var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var dateTimePicker: UIDatePicker!
    @IBOutlet var viewDatePicker: UIView!
    @IBOutlet var viewDatePickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var viewBackground: UIView!
    
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var btnBookLater: UIButton!
    @IBOutlet weak var lblBookLaterInfo: UILabel!
    
    @IBOutlet weak var btnBookNowWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleTypeCollectionView: UICollectionView!
    
    @IBOutlet weak var lblScheduleInfo: UILabel!
    @IBOutlet weak var lblScheduleHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewContainerBottom: UIView!
    @IBOutlet weak var viewBookRideFor: UIView!
    @IBOutlet weak var viewPaymentMode: UIView!
    @IBOutlet weak var viewApplyCoupon: UIView!
    @IBOutlet weak var btnTotalFare: UIButton!
    
    @IBOutlet weak var lblBookRideFor: UILabel!
    @IBOutlet weak var lblBookRideForContact: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblPaymentModeOption: UILabel!
    @IBOutlet weak var lblApplyCoupon: UILabel!
    @IBOutlet weak var lblAppliedCoupon: UILabel!
    @IBOutlet weak var imgViewCoupon: UIImageView!
    @IBOutlet weak var lblTotalFare: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    
    @IBOutlet weak var viewFindingDriver: UIView!
    @IBOutlet weak var progressBar: DSGradientProgressView!
    @IBOutlet weak var viewDriverInfo: UIView!
    @IBOutlet weak var btnCancelRequest: UIButton!
    
    @IBOutlet weak var viewConfirmBooking: UIView!
    
    var bokingStatusBo = BookingStatusBO()
    var currentCity : String!
    var bookNow_Status: String = ""
    var ride_Now: String = "0"
    
    var arrRentalBookingPackages = [RentalBookingPackages]()
    var arrRentalBookingCabTypes = [VehicleTypeBO]()
    var pickUpLocation : String = ""
    var selectedPackageIndex = 0
    var selectedCabIndex = 0
    var isForBooklater: Bool = false
    var pickupTime: String = ""
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    var selectedPackageId = ""
    var arrFareDettails = [FareDetails]()
    var info : String = ""
    var taxValue : String = ""
    var cityId : String = ""
    var totalFare : String = "--"
    var vehicleType: String = ""
    var selectedPackage: String = ""
    var isCouponApplied: Bool = false
    var promoCode = CouponBO()
    
    var vehicleTypeId : String = ""
    var waitingTime: String = ""
    
    var arrCabInfo : [CabLocationBO] = []
    var arrCabTiming = [CabTimingBO]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesigns()
        
    }
    
    override func viewDidLayoutSubviews() {
        viewBookRideFor.layer.cornerRadius = 5
        viewBookRideFor.clipsToBounds = true
        viewPaymentMode.layer.cornerRadius = 5
        viewPaymentMode.clipsToBounds = true
        viewApplyCoupon.layer.cornerRadius = 5
        viewApplyCoupon.clipsToBounds = true
        btnTotalFare.layer.cornerRadius = 5
        btnTotalFare.clipsToBounds = true
        
    }
    
    func initDesigns() {
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            navBarHeightConstraint.constant = 92
        }
        
        //Manage for iPhone 5 and Below
        if AppConstant.screenSize.height <= 568{
            lblBookRideFor.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            lblPaymentMode.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            lblApplyCoupon.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            lblFare.font = UIFont.systemFont(ofSize: 9, weight: .medium)
        }

        viewBackground.isHidden = true
        viewDatePicker.isHidden = true
        viewDatePickerBottomConstraint.constant = -190
        
        lblScheduleHeightConstraint.constant = 0
        
        //Set MySelf default for Book ride
        AppConstant.bookRideForContactSelectedStatus = "1"
        
        //loadRentalBookingPackages()
        
        //service call to get rate card
        serviceCallToGetRateCard()
        
        //service call to get Vehicle Category
        serviceCallToGetVehicleCategory()
        
        //service call to get cab timing
        self.serviceCallToGetCabTiming()
        
        NotificationCenter.default.addObserver(self, selector: #selector(RentalBookingRidesViewController.getBookingConfirmation(notification:)), name: Notification.Name("reloadNotification"), object: nil)
        
    }
    
    @objc func getBookingConfirmation(notification: Notification){
        self.serviceCallToGetBookingDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "RentalBookingRidesPage", screenClass: "RentalBookingRidesViewController")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Collectionview Delegates and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrRentalBookingCabTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = vehicleTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "VehicleTypeCollectionViewCell", for: indexPath as IndexPath) as! VehicleTypeCollectionViewCell
        
        let vehicleTypeBo = arrRentalBookingCabTypes[indexPath.row]
        
        //Set Waiting Time
        if self.arrCabTiming.count > 0{
            let timingBo = self.arrCabTiming[indexPath.row]
            cell.lblVehicleTypeTime.text = timingBo.time == "" ? "No \(vehicleTypeBo.name!)" : timingBo.time
        }
        
        cell.lblVehicleTypeName.text = vehicleTypeBo.name
        //cell.lblVehicleTypeTime.text = vehicleTypeBo.minTime
        if (vehicleTypeBo.isSelected == true) {
            cell.lblVehicleTypeName?.font = UIFont.init(name: StringConstant.poppinsBold, size: 12.0)
            
            if vehicleTypeBo.name == StringConstant.auto{
                cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.auto_blue)
            }else if vehicleTypeBo.name == StringConstant.car_micro{
                cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_micro_blue)
            }else if vehicleTypeBo.name == StringConstant.car_mini{
                cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_mini_blue) 
            }else if vehicleTypeBo.name == StringConstant.car_sedan{
                cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_sedan_blue)
            }else if vehicleTypeBo.name == StringConstant.car_suv{
                cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_suv_blue)
            }
            
            //cell.imgViewVehicleType?.sd_setImage(with: URL(string: vehicleTypeBo.selectedImage), placeholderImage: UIImage(named: vehicleTypeBo.selectedImage))
            cell.lblVehicleTypeTime?.font = UIFont.init(name: StringConstant.poppinsSemibold, size: 10.0)
            vehicleType = vehicleTypeBo.name!
            vehicleTypeId = vehicleTypeBo.vehicleId
            waitingTime = cell.lblVehicleTypeTime.text!
            
        }else{
            if (cell.lblVehicleTypeTime?.text == "No \(vehicleTypeBo.name!)"){
                
                cell.lblVehicleTypeName?.font = UIFont.init(name: StringConstant.poppinsMedium, size: 12.0)
                
                if vehicleTypeBo.name == StringConstant.auto{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.auto_gray)
                }else if vehicleTypeBo.name == StringConstant.car_micro{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_micro_gray)
                }
                else if vehicleTypeBo.name == StringConstant.car_mini{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_mini_gray)
                }
                else if vehicleTypeBo.name == StringConstant.car_sedan{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_sedan_gray)
                }else if vehicleTypeBo.name == StringConstant.car_suv{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_suv_gray)
                }
                cell.lblVehicleTypeTime?.font = UIFont.init(name: StringConstant.poppinsRegular, size: 10.0)
            }else{
                cell.lblVehicleTypeName?.font = UIFont.init(name: StringConstant.poppinsMedium, size: 12.0)
                
                if vehicleTypeBo.name == StringConstant.auto{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.auto_white)
                }else if vehicleTypeBo.name == StringConstant.car_micro{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_micro_white)
                }
                else if vehicleTypeBo.name == StringConstant.car_mini{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_mini_white)
                }
                else if vehicleTypeBo.name == StringConstant.car_sedan{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_sedan_white)
                }else if vehicleTypeBo.name == StringConstant.car_suv{
                    cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_suv_white)
                }
                cell.lblVehicleTypeTime?.font = UIFont.init(name: StringConstant.poppinsRegular, size: 10.0)
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
  
        selectedCabIndex = indexPath.row
        
        for item in self.arrRentalBookingCabTypes{
            item.isSelected = false
        }
        let cabBo = self.arrRentalBookingCabTypes[indexPath.row]
        
        cabBo.isSelected = true
        self.vehicleType = cabBo.name!
        self.vehicleTypeCollectionView.reloadData()
        
        //Service call to get fare details
        self.serviceCallToGetFareDetails()
        //Service call to update cab Timing
        self.serviceCallToGetCabTiming()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellHeight = 75
        var cellWidth = 94
        
        if AppConstant.arrVehicleType.count <= 5 {
            cellHeight = 75
            cellWidth = Int(AppConstant.screenSize.width) / AppConstant.arrVehicleType.count
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    //MARK: - Button Actions
    @IBAction func btnBookNowAction(_ sender: Any) {
        if (waitingTime == "No \(vehicleType)"){
            //btnBookNow.isEnabled = false
            self.showAlert(alertTitle: "Alert!", alertMessage: "No Cab available of \(vehicleType) category.Please check cab in other category")
            return
        }
        if btnBookNow.title(for: .normal) == "BOOK NOW" {
            bookNow_Status = "1"
            ride_Now = "0"
            
            btnBookNow.isEnabled = true
            //Move to DriverInfo Screen
            
            //self.viewConfirmBooking.isHidden = true
            self.viewFindingDriver.isHidden = false
            self.progressBar.wait()
            
            viewDriverInfo.showAnimatedGradientSkeleton()
        }else{
            bookNow_Status = "2"
            ride_Now = "1"
        }
        
        self.serviceCallToConfirmBooking()
    }
    
    @IBAction func btnBookLaterAction(_ sender: Any) {
        if isForBooklater == true{//Cancel
            isForBooklater = false
            lblScheduleInfo.isHidden = true
            lblScheduleHeightConstraint.constant = 0
            btnBookNow.setTitle("BOOK NOW", for: .normal)
            btnBookLater.setTitle("BOOK LATER", for: .normal)
            lblBookLaterInfo.isHidden = false
        }else{//BookLater
            dateTimePicker.minimumDate = Date()
            dateTimePicker.maximumDate = Date().add(days: 2)//User can book a cab before 2 days only
            viewDatePicker.animShow()
            
            viewBackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            viewBackground.isHidden = false
        }
    }
    
    @IBAction func btnCancelRequestAction(_ sender: Any) {
        self.showAlertForCancelRequest(strTitle: "Alert!", strDescription: "Are you sure you want to cancel the ride?", delegate: self)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func totalFareViewAction(_ sender: Any) {
        if self.totalFare != "0"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FareDetailsViewController") as! FareDetailsViewController
            vc.arrFareDettails = self.arrFareDettails
            vc.totalFare = self.totalFare
            vc.info = self.info
            vc.taxValue = self.taxValue
            vc.vehicleTypeId = self.vehicleTypeId
            vc.isForRental = true
            vc.package = self.selectedPackage
            
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            
            vc.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            vc.view.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                vc.view.alpha = 1.0
                vc.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })

            self.addChild(vc)
            self.view.addSubview(vc.view)
        }
    }
    
    @IBAction func viewBookRideForAction(_ sender: UIButton) {
        viewBackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewBackground.isHidden = false
        self.performSegue(withIdentifier: "book_ride_for", sender: self)
    }
    
     @IBAction func viewPaymentModeAction(_ sender: UIButton) {
        viewBackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewBackground.isHidden = false
        self.performSegue(withIdentifier: "payment_mode", sender: self)
    }
    
    @IBAction func viewApplyCouponAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "apply_coupon", sender: self)
    }
    
    @objc func setPickupTime() {
        dateTimePicker.minimumDate = Date()
        dateTimePicker.maximumDate = Date().add(days: 2)//User can book a cab before 2 days only
        viewDatePicker.animShow()
        
        viewDatePicker.isHidden = false
        viewDatePickerBottomConstraint.constant = 0
    
        viewBackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewBackground.isHidden = false
    }
    
    @IBAction func datePickerCancelBtnAction(_ sender: Any) {
        viewDatePicker.animHide()
        viewBackground.isHidden = true
    }
    
    @IBAction func datePickerDoneBtnAction(_ sender: Any) {
        btnBookNow.setTitle("SCHEDULE NOW", for: .normal)
        btnBookLater.setTitle("CANCEL", for: .normal)
        lblBookLaterInfo.isHidden = true
        
        viewDatePicker.animHide()
        viewBackground.isHidden = true
        isForBooklater = true
        
        pickupTime = AppConstant.formattedDate(date: dateTimePicker.date, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter4)!
        
        let attributedText = NSMutableAttributedString(string: "Schedule Date and Time:   ", attributes: [NSAttributedString.Key.font: UIFont.init(name: "Poppins-Medium", size: 12)])
        let attributedTxt = NSMutableAttributedString(string: pickupTime, attributes: [NSAttributedString.Key.font: UIFont.init(name: "Poppins-Regular", size: 12)])
        attributedText.append(attributedTxt)
        
        lblScheduleInfo.attributedText = attributedText
        
        rentalBookingRidesTableView.reloadData()
        
        lblScheduleInfo.isHidden = false
        lblScheduleHeightConstraint.constant = 35
    }
    
    //MARK: Choose Contact Protocol Delegates
    func selectedObject(obj: BookRideForContactBO, type: String) {
        viewBackground.isHidden = true
        
        if (type == "book_ride_for") {
            AppConstant.currentBookRideForSelectedType = type
            if (obj.name == nil) {
                if (AppConstant.bookRideForContactName == "") {
                    self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
                    self.lblBookRideForContact?.text = "Myself"
                    AppConstant.bookRideForContactSelectedStatus = "1"
                }
                else {
                    self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                    self.lblBookRideForContact?.text = "Others"
                    AppConstant.bookRideForContactSelectedStatus = "2"
                }
            }
            else {
                self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                self.lblBookRideForContact?.text = obj.name!
                AppConstant.bookRideForContactSelectedStatus = "2"
            }
        }
        else if (type == "Myself") {
            AppConstant.currentBookRideForSelectedType = type
            self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
            self.lblBookRideForContact?.text = "Myself"
            AppConstant.bookRideForContactSelectedStatus = "1"
        }
    }
    
    //MARK: Applied Coupon Protocol Delegates
    func appliedCoupon(couponBo: CouponBO) {
        promoCode = couponBo
        isCouponApplied = true
        lblAppliedCoupon.text = couponBo.title
        imgViewCoupon.isHidden = true
        lblAppliedCoupon.isHidden = false
        lblApplyCoupon.text = "Coupon Applied"
        
        self.serviceCallToGetFareDetails()
    }
    
    //MARK: Choose Payment Mode Protocol Delegates
    func selectedPaymentMode(pMode: String) {
        viewBackground.isHidden = true
        AppConstant.selectedPaymentMode = pMode
        self.lblPaymentModeOption?.text = pMode
    }
    
    //MARK: Tableview Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 1
        }
        else if (section == 1) {
            return arrRentalBookingPackages.count + 1
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RentalPickUpLocationTableViewCell", for: indexPath as IndexPath) as! RentalPickUpLocationTableViewCell
            
            cell.selectionStyle = .none
            cell.lblPickUpLocation?.text = self.pickUpLocation
            
            return cell
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RentalTitleTableViewCell", for: indexPath as IndexPath) as! RentalTitleTableViewCell
                
                cell.selectionStyle = .none
                cell.lblRentalBookingTitle?.text = "SELECT  PACKAGE"
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RentalPackageTableViewCell", for: indexPath as IndexPath) as! RentalPackageTableViewCell
                
                cell.selectionStyle = .none
                let rentalBookingPackagesBo = arrRentalBookingPackages[indexPath.row - 1]
                cell.lblRentalBookingPackages?.text = rentalBookingPackagesBo.packageName
                cell.radioImage?.image = selectedPackageIndex == indexPath.row - 1 ? UIImage.init(named: "radio_checked") : UIImage.init(named: "radio_unchecked")
                
                return cell
            }
        }
        
       return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return CGFloat.leastNonzeroMagnitude
        }else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 1) {//Select Package
            selectedPackageIndex = indexPath.row - 1
            let packageBo = self.arrRentalBookingPackages[indexPath.row - 1]
            self.selectedPackage = packageBo.packageName
        }else if (indexPath.section == 2) {//Choose a  cab
            selectedCabIndex = indexPath.row - 1
            
            let cabBo = self.arrRentalBookingCabTypes[indexPath.row - 1]
            self.vehicleType = cabBo.name!
        }
        self.serviceCallToGetFareDetails()
    }
    
    //MARK: Service Call
    
    func serviceCallToConfirmBooking() {
        if AppConstant.hasConnectivity() {
            if bookNow_Status == "2" {
                AppConstant.showHUD()
            }else{
                DispatchQueue.main.async {
                    self.viewFindingDriver.isHidden = false
                    self.progressBar.wait()
                }
            }
            
            let pickUPAddress = self.pickUpLocation
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            
            let params: Parameters = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "book_now" : bookNow_Status,
                "book_for" : lblBookRideForContact?.text == "Myself" ? "1" : "2",
                "pay_mode" : AppConstant.selectedPaymentMode == "Cash" ? "1" : "2",
                "ride_id" : "1",
                "rental_id" : arrRentalBookingPackages[selectedPackageIndex].packageId,
                "cat_id" : arrRentalBookingCabTypes[selectedCabIndex].vehicleId,
                "city" : self.currentCity!,
                "src_lat" : String(describing: pickLat),
                "src_lon" : String(describing: pickLng),
                "src_loc" : String(describing: pickUPAddress),
                "oth_mobile": lblBookRideForContact?.text == "Myself" ? "" : AppConstant.bookRideForContactNumber,
                "promo_code" : isCouponApplied ? promoCode.title : "",
                "schedule_date": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter2)! : "",
                "schedule_time": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter6)! : "",
                "fare": self.totalFare
            ]
            
            print("Url===\(AppConstant.confirmBookingUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.confirmBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    
                    if self.bookNow_Status == "2" {
                        AppConstant.hideHUD()
                    }else{
                        DispatchQueue.main.async {
                            self.viewFindingDriver.isHidden = false
                            self.progressBar.signal()
                        }
                    }
                    
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            
                            if let status = dict["status"] as? String {
                                if status == "0" {
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                    
                                    //Hide Loader
                                    DispatchQueue.main.async {
                                        self.viewFindingDriver.isHidden = true
                                        self.progressBar.signal()
                                    }
                                    
                                }else  if status == "1" {//BookNow
                                    if let bookingId = dict["book_id"] as? String {
                                        AppConstant.saveInDefaults(key: StringConstant.book_id, value: bookingId)
                                    }
                                    if let waitTime = dict["wait_time"] as? Int {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 90) { // change 30 to desired number of seconds
                                            
                                            if(!AppConstant.isBookingConfirm){
                                                //Call api to confirm your booking
                                                self.serviceCallToGetBookingDetails()
                                            }
                                        }
                                    }
                                    
                                }else  if status == "2" {//Schedule Booking
                                    let msg = dict["msg"] as? String
                                    let alert = UIAlertController(title: msg, message: "", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .cancel) { action in
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.goToMainScreen()
                                    })
                                    alert.view.tintColor = AppConstant.colorThemeBlue
                                    self.present(alert, animated: true)
                                }
                            }
                            
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetCabTiming() {
        if AppConstant.arrVehicleType.count == 0{
            return
        }
        if AppConstant.hasConnectivity() {
            // Loader NVActivityIndicatorView
            let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: vehicleTypeCollectionView.frame.origin.x, y: vehicleTypeCollectionView.frame.origin.y + 50, width: AppConstant.screenSize.width, height: 25), type: .ballPulse, color: AppConstant.colorThemeBlue, padding: 3)
            activityIndicatorView.backgroundColor = UIColor.white
            self.vehicleTypeCollectionView.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
            
            //AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!)
            ]
            
            print("url===\(AppConstant.getCabTimeUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getCabTimeUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Cab Timing : \(response)")
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            
                            if let status = dict["status"] as? Int {
                                if(status == 1){
                                    if let arrTiming = dict["data"] as? [[String: Any]]{
                                        for dictData in arrTiming{
                                            var cabTimingBo = CabTimingBO()
                                            if let cat_id = dictData["cat_id"] as? String{
                                                cabTimingBo.cat_id = cat_id
                                            }
                                            if let cat_name = dictData["cat_name"] as? String{
                                                cabTimingBo.cat_name = cat_name
                                            }
                                            if let count = dictData["count"] as? Int{
                                                cabTimingBo.count = count
                                            }
                                            if let time = dictData["time"] as? String{
                                                cabTimingBo.time = time
                                            }
                                            
                                            self.arrCabTiming.append(cabTimingBo)
                                        }
                                    }
                                    self.vehicleTypeCollectionView.reloadData()
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                            activityIndicatorView.stopAnimating()
                            self.vehicleTypeCollectionView.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetBookingDetails() {
        if AppConstant.hasConnectivity() {
            DispatchQueue.main.async {
                self.viewFindingDriver.isHidden = false
                self.progressBar.wait()
            }
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "book_id" : AppConstant.retrievFromDefaults(key: StringConstant.book_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("params===\(params!)")
            print("Api===\(AppConstant.getBookingStatusDetailsUrl)")
            
            AF.request( AppConstant.getBookingStatusDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    DispatchQueue.main.async {
                        self.viewFindingDriver.isHidden = true
                        self.progressBar.signal()
                    }
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if(status == "1"){//Success
                                    if let bookStatus = dict["book_status"] as? String{
                                        if bookStatus == StringConstant.cancelByAdmin {
                                            if let book_Status_str = dict["book_status_str"] as? String{
                                                self.viewConfirmBooking.isHidden = false
                                                self.showAlert(alertTitle: book_Status_str, alertMessage: "")
                                            }
                                        }else{
                                            if let dictBookInfo = dict["book_info"] as? [String: Any]{
                                                if let bookOtp = dictBookInfo["book_otp"] as? String{
                                                    self.bokingStatusBo.driverBo.otp = bookOtp
                                                }
                                                if let pickPnt = dictBookInfo["pick_pnt"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.pick_pnt = pickPnt
                                                }
                                                if let dropPnt = dictBookInfo["drop_pnt"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.drop_pnt = dropPnt
                                                }
                                                if let srcLat = dictBookInfo["src_lat"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.src_lat = srcLat
                                                }
                                                if let srcLon = dictBookInfo["src_lon"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.src_lon = srcLon
                                                }
                                                if let desLat = dictBookInfo["des_lat"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.des_lat = desLat
                                                }
                                                if let desLon = dictBookInfo["des_lon"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.des_lon = desLon
                                                }
                                                if let rideId = dictBookInfo["ride_id"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.ride_id = rideId
                                                }
                                                if let rideName = dictBookInfo["ride_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.ride_name = rideName
                                                }
                                                if let rentalName = dictBookInfo["rental_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.rental_name = rentalName
                                                }
                                                if let catName = dictBookInfo["cat_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.cat_name = catName
                                                }
                                                if let waitTime = dictBookInfo["wait_time"] as? Int{
                                                    self.bokingStatusBo.bookingInfoBo.wait_time = waitTime
                                                }
                                                if let rideCompleted = dictBookInfo["ride_completed"] as? String{
                                                    if rideCompleted == ""{
                                                        self.bokingStatusBo.bookingInfoBo.ride_completed = 0
                                                    }else{
                                                        self.bokingStatusBo.bookingInfoBo.ride_completed = Int(rideCompleted)!
                                                    }
                                                }
                                                
                                            }
                                            if let dictDriverInfo = dict["driver_info"] as? [String: Any]{
                                                if let driverId = dictDriverInfo["driver_id"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_id = driverId
                                                }
                                                if let driverName = dictDriverInfo["driver_name"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_name = driverName
                                                }
                                                if let driverMobile = dictDriverInfo["driver_mobile"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_mobile = driverMobile
                                                 }
                                                if let driverRating = dictDriverInfo["driver_rating"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_rating = driverRating
                                                }
                                                if let driverImage = dictDriverInfo["driver_image"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_image = driverImage
                                                }
                                                if let vehcile_reg_No = dictDriverInfo["vehcile_reg_no"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_registration_number = vehcile_reg_No
                                                }
                                                if let vehcile_brand_Name = dictDriverInfo["vehcile_brand_name"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_brand_name = vehcile_brand_Name
                                                }
                                                if let vehcile_model_Name = dictDriverInfo["vehcile_model_name"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_model_name = vehcile_model_Name
                                                }
                                                if let vehicleImage = dictDriverInfo["vehicle_image"] as? String {
                                                    self.bokingStatusBo.driverBo.vehicle_image = vehicleImage
                                                }
                                            }
                                            
                                            //Move to DriverInfo Screen
                                            self.performSegue(withIdentifier: "driverToReachPickupPoint", sender: self)
                                        }
                                        
                                    }
                                    
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToCancelBooking() {
        AppConstant.isBookingConfirm = false
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "policy_id": "0",
                "book_id": AppConstant.retrievFromDefaults(key: StringConstant.book_id)]
            
            print("url===\(AppConstant.cancelBookingUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.cancelBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    DispatchQueue.main.async {
                        self.viewFindingDriver.isHidden = true
                        self.progressBar.signal()
                    }
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetRateCard() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "city_name" : AppConstant.cityName,
                "ride_type" : 1
            ]
            
            print("url===\(AppConstant.getRateCardUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getRateCardUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Rate Card : \(response)")
                    switch(response.result) {
                    case let .success(value):
                        
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//Success
                                    self.arrRentalBookingPackages.removeAll()
                                    if let dictRateCard = dict["rateCard"] as? [String: Any] {
                                        if let arrCategory = dictRateCard["category"] as? [[String: Any]] {
                                            for dict in arrCategory{
                                                let packageBo = RentalBookingPackages()
                                                if let rental_id = dict["rental_id"] as? String {
                                                    packageBo.packageId = rental_id
                                                }
                                                if let rental_type = dict["rental_type"] as? String {
                                                    packageBo.packageName = rental_type
                                                }
                                                
                                                self.arrRentalBookingPackages.append(packageBo)
                                            }
                                        }
                                    }
                                    if self.arrRentalBookingPackages.count > 0 && self.arrRentalBookingCabTypes.count > 0 {
                                        let packageBo = self.arrRentalBookingPackages[0]
                                        self.selectedPackage = packageBo.packageName
                                        self.selectedPackageId = packageBo.packageId
                                        self.serviceCallToGetFareDetails()
                                    }
                                    self.rentalBookingRidesTableView.reloadData()
                                        
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        AppConstant.showAlertForLogout(strTitle: "Logout", strDescription: errorMsg, delegate: self)
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetVehicleCategory() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!)
            ]
            
            print("url===\(AppConstant.getVehicleCategoryUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.getVehicleCategoryUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    //AppConstant.hideHUD()
                    print("url===\(AppConstant.getVehicleCategoryUrl)")
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//Success
                                    self.arrRentalBookingCabTypes.removeAll()
                                    if let arrVehicleCategoryInfo = dict["data"] as? [[String: Any]] {
                                        var index: Int = -1
                                        if (arrVehicleCategoryInfo.count > 0) {
                                            for dictInfo in arrVehicleCategoryInfo {
                                                index = index + 1
                                                let vehicleTypeBo = VehicleTypeBO()
                                                
                                                if let vehicleId = dictInfo["cat_id"] as? String {
                                                    vehicleTypeBo.vehicleId = vehicleId
                                                }
                                                if let vehicleName = dictInfo["cat_name"] as? String {
                                                    vehicleTypeBo.name = vehicleName
                                                }
                                                if let vehicleSelectedImage = dictInfo["image_blue"] as? String {
                                                    vehicleTypeBo.selectedImage = vehicleSelectedImage
                                                }
                                                if let vehicleUnselectedImage = dictInfo["image_white"] as? String {
                                                    vehicleTypeBo.unselectedImage = vehicleUnselectedImage
                                                }
                                                if let minTime = dictInfo["min_time"] as? String {
                                                    vehicleTypeBo.minTime = minTime
                                                }
                                                vehicleTypeBo.rowIndex = index
                                                if (vehicleTypeBo.rowIndex == 0) {//Default Selected Vehicle
                                                    vehicleTypeBo.isSelected = true
                                                }else {
                                                    vehicleTypeBo.isSelected = false
                                                }
                                                self.arrRentalBookingCabTypes.append(vehicleTypeBo)
                                            }
                                            if self.arrRentalBookingCabTypes.count > 0 && self.arrRentalBookingPackages.count > 0{
                                                self.serviceCallToGetFareDetails()
                                            }
                                            
                                            self.vehicleTypeCollectionView.reloadData()
                                        }
                                    }
                                    AppConstant.hideHUD()
                                    
                                }else if (status == 3){
                                    AppConstant.hideHUD()
                                }else{//Logout from the app
                                    AppConstant.hideHUD()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    
    func serviceCallToGetFareDetails() {
        if AppConstant.hasConnectivity() {
            // Loader NVActivityIndicatorView
            let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: vehicleTypeCollectionView.frame.width/2 - 100, y: vehicleTypeCollectionView.frame.origin.y + 50, width: 200, height: 30), type: .ballPulse, color: AppConstant.colorThemeBlue, padding: 3)
            activityIndicatorView.backgroundColor = UIColor.white
            self.vehicleTypeCollectionView.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
            
           // AppConstant.showHUD()
            
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "src_lat" : String(describing: pickLat),
                "src_lon" : String(describing: pickLng),
                "ride_id" : "1",
                "ride_now" : self.ride_Now,
                "rental_id": arrRentalBookingPackages[selectedPackageIndex].packageId,
                "cat_id" : arrRentalBookingCabTypes[selectedCabIndex].vehicleId,
                "city" : AppConstant.cityName,
                "promo_code" : isCouponApplied ? promoCode.title : "",
                "schedule_date": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter2)! : "",
                "schedule_time": bookNow_Status == "2" ? AppConstant.formattedDateFromString(dateString: pickupTime, withFormat: StringConstant.dateFormatter4, ToFormat: StringConstant.dateFormatter6)! : ""
            ]
            
            
            print("url===\(AppConstant.getFareDetailsUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getFareDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            //                        let dict = dataArray![0] as Dictionary
                            self.arrFareDettails.removeAll()
                            if let status = dict["status"] as? String {
                                if(status == "0"){
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                }else  if(status == "1"){
                                    if let totalFare = dict["cost"] as? Int {
                                        self.totalFare = "\(totalFare)"
                                    }
                                    if let taxValue = dict["tax"] as? Int {
                                        self.taxValue = "\(taxValue)"
                                    }
                                    if let info = dict["info_str"] as? String {
                                        self.info = info
                                        debugPrint(self.info)
                                    }
                                    if let fareInfoArray = dict["info_arry"] as? [[String: Any]]{
                                        debugPrint(fareInfoArray)
                                        for item in fareInfoArray {
                                            let fareDetailsBo = FareDetails()
                                            
                                            if let titleKey = item["key"] as? String {
                                                fareDetailsBo.title = titleKey.replacingOccurrences(of: "&#x20b9;", with: "₹ ")
                                            }
                                            if let value = item["value"] as? Int {
                                                fareDetailsBo.price = String(value)
                                            }
                                            if let value = item["value"] as? String {
                                                fareDetailsBo.price = value
                                            }
                                            if let type = item["type"] as? Int{
                                                fareDetailsBo.type = type
                                            }
                                            self.arrFareDettails.append(fareDetailsBo)
                                        }
                                    }
                                    
                                    self.lblTotalFare.text = self.totalFare == "0" ? "--" : "₹ \(self.totalFare)/-"
                                    
                                }else  if(status == "2"){
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.goToLandingScreen()
                                }
                            }
                            
                            activityIndicatorView.stopAnimating()
                            self.rentalBookingRidesTableView.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    //MARK: Alert Method
    
    func showAlertForCancelRequest(strTitle: String,strDescription: String,delegate: AnyObject?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: strTitle, message: strDescription, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (action) in
                self.serviceCallToCancelBooking()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.view.endEditing(true)
        if segue.identifier == "driverToReachPickupPoint"{
            let vc = segue.destination as! DriverToReachPickUpPointViewController
            vc.pickUpCoordinate = self.pickUpCoordinate
            vc.pickUpAddress = self.pickUpLocation
            vc.bookingStatusBo = self.bokingStatusBo
        }else if (segue.identifier == "book_ride_for"){
            let vc = segue.destination as! BookRideForViewController
            vc.delegate = self
            vc.type = segue.identifier!
        }else if (segue.identifier == "payment_mode"){
            let vc = segue.destination as! PaymentModeViewController
            vc.delegate = self
        }else if (segue.identifier == "apply_coupon"){
            let vc = segue.destination as! ApplyCouponViewController
            vc.delegate = self
            vc.selectedCouponBo = self.promoCode
        }
    }
    

}
