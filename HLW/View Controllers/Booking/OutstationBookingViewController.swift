//
//  OutstationBookingViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 22/04/22.
//  Copyright © 2022 ODITEK. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import GooglePlacesSearchController
import GooglePlaces
import DSGradientProgressView
import SkeletonView

class OutstationBookingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ChooseDelegate, ChoosePaymentModeDelegate, ApplyCouponDelegate, ChooseDateTimeDelegate, UITextFieldDelegate, GMSAutocompleteViewControllerDelegate {
    
    @IBOutlet var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblViewOutstationBooking: UITableView!
    @IBOutlet weak var vehicleTypeCollectionView: UICollectionView!
    @IBOutlet var buttomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewContainerBottom: UIView!
    @IBOutlet weak var viewBookRideFor: UIView!
    @IBOutlet weak var viewPaymentMode: UIView!
    @IBOutlet weak var viewApplyCoupon: UIView!
    @IBOutlet weak var btnTotalFare: UIButton!
    
    @IBOutlet weak var lblBookRideFor: UILabel!
    @IBOutlet weak var lblBookRideForContact: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblPaymentModeOption: UILabel!
    @IBOutlet weak var lblApplyCoupon: UILabel!
    @IBOutlet weak var lblAppliedCoupon: UILabel!
    @IBOutlet weak var imgViewCoupon: UIImageView!
    @IBOutlet weak var lblTotalFare: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    @IBOutlet var viewBackground: UIView!
    @IBOutlet weak var viewConfirmBooking: UIView!
    @IBOutlet var viewFindingDriver: UIView!
    @IBOutlet var progressBar: DSGradientProgressView!
    @IBOutlet var viewDriverInfo: UIView!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblVehicleNumber: UILabel!
    @IBOutlet weak var cancelRideBtn: UIButton!
    @IBOutlet var btnCancelRequest: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    
    var pickUpLocation : String = ""
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    var pickupTime: String = ""
    var currentCity : String!
    var cityId : String = ""
//    var bookNow_Status: String = ""
    var ride_Now: String = "0"
    
    var arrOutstationBookingCabTypes = [VehicleTypeBO]()
    var arrCabTiming = [CabTimingBO]()
    var arrFareDettails = [FareDetails]()
    var info : String = ""
    var taxValue : String = ""
    var totalFare : String = "--"
    var vehicleType: String = ""
    var vehicleTypeId : String = ""
    var waitingTime: String = ""
    var selectedCabIndex = 0
    var selectedPackageIndex = 0
    var selectedPackage: String = ""
    var isCouponApplied: Bool = false
    var promoCode = CouponBO()
    
    var isShowBtnContinue = true
    var isSelectedDateTime = false
    var isOnewaySelected = Bool()
    var isStartDateSelected = false
    
    var strStartDateTime: String = ""
    var strEndDateTime: String = ""
    var startDateTime = Date()
    var endDateTime = Date()
    
    var dropCityFullName: String = ""
    var dropCityName: String = ""
    var dropCoordinate: CLLocationCoordinate2D? = nil
    
    var estimatedKm: String = ""
    var estimatedKmVal: Int = 0
    var tripType: Int = 0
    
    var bokingStatusBo = BookingStatusBO()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    override func viewDidLayoutSubviews() {
        viewBookRideFor.layer.cornerRadius = 5
        viewBookRideFor.clipsToBounds = true
        viewPaymentMode.layer.cornerRadius = 5
        viewPaymentMode.clipsToBounds = true
        viewApplyCoupon.layer.cornerRadius = 5
        viewApplyCoupon.clipsToBounds = true
        btnTotalFare.layer.cornerRadius = 5
        btnTotalFare.clipsToBounds = true
    }
    
    func initDesign(){
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            navBarHeightConstraint.constant = 92
            buttomViewHeightConstraint.constant = 60
        }
        
        //Manage for iPhone 5 and Below
        if AppConstant.screenSize.height <= 568{
            lblBookRideFor.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            lblPaymentMode.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            lblApplyCoupon.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            lblFare.font = UIFont.systemFont(ofSize: 9, weight: .medium)
        }
        
        //Set MySelf default for Book ride
        AppConstant.bookRideForContactSelectedStatus = "1"
        
        viewBackground.isHidden = true
        viewConfirmBooking.isHidden = true
        
        tblViewOutstationBooking.delegate = self
        tblViewOutstationBooking.dataSource = self
        tblViewOutstationBooking.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "OutstationBookingPage", screenClass: "OutstationBookingViewController")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookingErrorNotification(notification:)), name: Notification.Name("BookingErrorNotification"), object: nil)
    }
    
    //MARK: Choose Contact Protocol Delegates
    func selectedObject(obj: BookRideForContactBO, type: String) {
        viewBackground.isHidden = true
        AppConstant.currentBookRideForSelectedType = type
        if (type == "book_ride_for") {
            if (obj.name == nil) {
                if (AppConstant.bookRideForContactName == "") {
                    self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
                    self.lblBookRideForContact?.text = "Myself"
                    AppConstant.bookRideForContactSelectedStatus = "1"
                }
                else {
                    self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                    self.lblBookRideForContact?.text = "Others"
                    AppConstant.bookRideForContactSelectedStatus = "2"
                }
            }
            else {
                self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 14.0)
                self.lblBookRideForContact?.text = obj.name!
                AppConstant.bookRideForContactSelectedStatus = "2"
            }
        }
        else if (type == "Myself") {
            self.lblBookRideForContact?.font = UIFont.init(name: "Poppins-Semibold", size: 15.0)
            self.lblBookRideForContact?.text = "Myself"
            AppConstant.bookRideForContactSelectedStatus = "1"
        }
    }
    
    //MARK: Applied Coupon Protocol Delegates
    func appliedCoupon(couponBo: CouponBO) {
        promoCode = couponBo
        isCouponApplied = true
        lblAppliedCoupon.text = couponBo.title
        imgViewCoupon.isHidden = true
        lblAppliedCoupon.isHidden = false
        lblApplyCoupon.text = "Coupon Applied"
        
        self.serviceCallToGetFareDetails()
    }
    
    //MARK: Choose Payment Mode Protocol Delegates
    func selectedPaymentMode(pMode: String) {
        viewBackground.isHidden = true
        AppConstant.selectedPaymentMode = pMode
        self.lblPaymentModeOption?.text = pMode
    }
    
    //MARK: Choose DateTime Protocol Delegates
    func selectedDateTime(totalTripTime: Date) {
        viewBackground.isHidden = true
        if isStartDateSelected == true{
            self.startDateTime = totalTripTime
            self.strStartDateTime = AppConstant.formattedDate(date: totalTripTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter7)!
        }else{
            self.endDateTime = totalTripTime
            self.strEndDateTime = AppConstant.formattedDate(date: totalTripTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter7)!
        }
        serviceCallToGetFareDetails()
        tblViewOutstationBooking.reloadData()
    }
    
    //MARK: Tableview Delegate & Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSelectedDateTime == false && isShowBtnContinue == true{
            return 3
        }else{
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return 1
        }else if section == 2{
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "outstationPickupLocation", for: indexPath as IndexPath) as! OutstationPickupLocationTableViewCell
            cell.selectionStyle = .none
            
            let attributedText = NSMutableAttributedString(string: "Current City:   ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .semibold)])
            let attributedTxt = NSMutableAttributedString(string: self.currentCity, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)])
            attributedText.append(attributedTxt)
            
            cell.lblCurrentCity.attributedText = attributedText
            cell.lblPickupLocation.text = self.pickUpLocation
            
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectDropCity", for: indexPath as IndexPath) as! SelectDropCityTableViewCell
            cell.selectionStyle = .none
            
            if self.dropCityFullName == ""{
                cell.lblDropLocName.textColor = UIColor.darkGray
                cell.lblDropLocName.text = "Select City"
            }else{
                cell.lblDropLocName.textColor = UIColor.black
                cell.lblDropLocName.text = self.dropCityFullName
            }
            
            var steEstKm = ""
            if self.tripType != 1{//Round Trip
                steEstKm = self.estimatedKm.replacingOccurrences(of: "kms", with: "")
                steEstKm = String(Int(steEstKm.trim() == "" ? "0": steEstKm.trim())! * 2) + " kms"
            }else{
                steEstKm = self.estimatedKm
            }
            
            let attributedText = NSMutableAttributedString(string: "Estimated Kilometer:   ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .semibold)])
            let attributedTxt = NSMutableAttributedString(string: steEstKm, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)])
            attributedText.append(attributedTxt)
            
            cell.lblEstimatedKm.attributedText = attributedText
            
            
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnContinue", for: indexPath as IndexPath) as! ContinueBtnTableViewCell
            cell.selectionStyle = .none
            
            cell.btnContinue.tag = indexPath.row
            cell.btnContinue.addTarget(self, action: #selector(btnContinueAction(_:)), for: .touchUpInside)
            
            if isShowBtnContinue == false{
                cell.btnContinue.isHidden = true
                cell.btnContinueHeightConstraint.constant = 0
            }else{
                cell.btnContinue.isHidden = false
                cell.btnContinueHeightConstraint.constant = 40
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectDateTime", for: indexPath as IndexPath) as! SelectDateTimeTableViewCell
            cell.selectionStyle = .none
            
            cell.btnOneway.tag = indexPath.row
            cell.btnOneway.addTarget(self, action: #selector(radioBtnOnewayAction(_:)), for: .touchUpInside)
            cell.btnRoundTrip.tag = indexPath.row
            cell.btnRoundTrip.addTarget(self, action: #selector(radioBtnRoundTripAction(_:)), for: .touchUpInside)
            
            cell.btnOneway.isEnabled = self.tripType == 0 ? false : true
            cell.lblOneway.textColor = self.tripType == 0 ? UIColor.gray : UIColor.black
            cell.lblOnewayDisableMsg.isHidden = self.tripType == 0 ? false : true
            cell.lblDisableMsgHeightConstraint.constant = self.tripType == 0 ? 30 : 0
            
            if isOnewaySelected == true{
                cell.radioBtnOneway.isSelected = true
                cell.radioBtnRoundTrip.isSelected = false
                cell.viewEndDateTime.isHidden = true
                cell.viewEndDateTimeHeightConstraint.constant = 0
                cell.viewContentHeightConstraint.constant = self.tripType == 0 ? 140 : 110
            }else{
                cell.radioBtnOneway.isSelected = false
                cell.radioBtnRoundTrip.isSelected = true
                cell.viewEndDateTime.isHidden = false
                cell.viewEndDateTimeHeightConstraint.constant = 35
                cell.viewContentHeightConstraint.constant = self.tripType == 0 ? 180 : 145
            }
            
            cell.btnSelectStartDateTime.tag = 0
            cell.btnSelectStartDateTime.addTarget(self, action: #selector(btnSelectDateTimeAction(_:)), for: .touchUpInside)
            cell.btnSelectEndDateTime.tag = 1
            cell.btnSelectEndDateTime.addTarget(self, action: #selector(btnSelectDateTimeAction(_:)), for: .touchUpInside)
            
            cell.lblStartDateTime.text = self.strStartDateTime
            if self.strEndDateTime != ""{
                cell.lblEndDateTime.text = self.strEndDateTime
                cell.btnSelectEndDateTime.setTitle("", for: .normal)
            }
            
            return cell
        }
    }
    
    //MARK: Collectionview Delegate & Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return arrOutstationBookingCabTypes.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = vehicleTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "VehicleTypeCollectionViewCell", for: indexPath as IndexPath) as! VehicleTypeCollectionViewCell
          
          let vehicleTypeBo = arrOutstationBookingCabTypes[indexPath.row]
          
          //Set Waiting Time
          if self.arrCabTiming.count > 0{
              let timingBo = self.arrCabTiming[indexPath.row]
              cell.lblVehicleTypeTime.text = timingBo.time == "" ? "No \(vehicleTypeBo.name!)" : timingBo.time
          }
          
          cell.lblVehicleTypeName.text = vehicleTypeBo.name
          //cell.lblVehicleTypeTime.text = vehicleTypeBo.minTime
          if (vehicleTypeBo.isSelected == true) {
              cell.lblVehicleTypeName?.font = UIFont.init(name: StringConstant.poppinsBold, size: 12.0)
              
              if vehicleTypeBo.name == StringConstant.car_micro{
                  cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_micro_blue)
              }else if vehicleTypeBo.name == StringConstant.car_mini{
                  cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_mini_blue)
              }else if vehicleTypeBo.name == StringConstant.car_sedan{
                  cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_sedan_blue)
              }else if vehicleTypeBo.name == StringConstant.car_suv{
                  cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_suv_blue)
              }
            
              cell.lblVehicleTypeTime?.font = UIFont.init(name: StringConstant.poppinsSemibold, size: 10.0)
              vehicleType = vehicleTypeBo.name!
              vehicleTypeId = vehicleTypeBo.vehicleId
              waitingTime = cell.lblVehicleTypeTime.text!
              
          }else{
              if (cell.lblVehicleTypeTime?.text == "No \(vehicleTypeBo.name!)"){
                  
                  cell.lblVehicleTypeName?.font = UIFont.init(name: StringConstant.poppinsMedium, size: 12.0)
                  
                  if vehicleTypeBo.name == StringConstant.car_micro{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_micro_gray)
                  }
                  else if vehicleTypeBo.name == StringConstant.car_mini{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_mini_gray)
                  }
                  else if vehicleTypeBo.name == StringConstant.car_sedan{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_sedan_gray)
                  }else if vehicleTypeBo.name == StringConstant.car_suv{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_suv_gray)
                  }
                  cell.lblVehicleTypeTime?.font = UIFont.init(name: StringConstant.poppinsRegular, size: 10.0)
              }else{
                  cell.lblVehicleTypeName?.font = UIFont.init(name: StringConstant.poppinsMedium, size: 12.0)
                  
                  if vehicleTypeBo.name == StringConstant.car_micro{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_micro_white)
                  }
                  else if vehicleTypeBo.name == StringConstant.car_mini{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_mini_white)
                  }
                  else if vehicleTypeBo.name == StringConstant.car_sedan{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_sedan_white)
                  }else if vehicleTypeBo.name == StringConstant.car_suv{
                      cell.imgViewVehicleType.image = UIImage.init(named: StringConstant.car_suv_white)
                  }
                  cell.lblVehicleTypeTime?.font = UIFont.init(name: StringConstant.poppinsRegular, size: 10.0)
              }
              
          }
          
          return cell
      }
      
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.lblTotalFare.text != "--"{
            selectedCabIndex = indexPath.row
            
            for item in self.arrOutstationBookingCabTypes{
                item.isSelected = false
            }
            let cabBo = self.arrOutstationBookingCabTypes[indexPath.row]
            
            cabBo.isSelected = true
            self.vehicleType = cabBo.name!
            self.vehicleTypeCollectionView.reloadData()
            
            //Service call to get fare details
            self.serviceCallToGetFareDetails()
            //Service call to update cab Timing
            self.serviceCallToGetCabTiming()
        }else {
            if isOnewaySelected == false && self.strEndDateTime == ""{
                self.showAlert(alertTitle: "Alert!", alertMessage: "Select end date and time")
            }
        }
        
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          var cellHeight = 75
          var cellWidth = 100
          
          if AppConstant.arrVehicleType.count <= 4 {
              cellHeight = 75
              cellWidth = Int(AppConstant.screenSize.width) / AppConstant.arrVehicleType.count
          }
          return CGSize(width: cellWidth, height: cellHeight)
      }
    
    // MARK: AutocompleteViewController Delegate
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        self.dropCityFullName = String(format: "%@", place.formattedAddress!)
        self.dropCityName = String(format: "%@", place.name!)
        self.dropCoordinate = place.coordinate
        
        dismiss(animated: true, completion: nil)
        
        //Service call to get CityDistance
        self.serviceCallToGetEstimatedCityDistance()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) { // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Button Actions
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectDropCityAction(_ sender: Any) {
        isShowBtnContinue = true
        isSelectedDateTime = false
        viewConfirmBooking.isHidden = true
        self.dropCityFullName = ""
        self.estimatedKm = "0 kms"
        tblViewOutstationBooking.reloadData()
        
        if isShowBtnContinue == true{
            presentGSMAutocompleteViewController()
        }
    }
    
    @objc func btnContinueAction(_ sender: UIButton) {
        if self.dropCityFullName == ""{
            self.showAlert(alertTitle: "Select drop city to continue", alertMessage: "")
        }else{
            self.startDateTime = Date().add(hours: 1)!
            print("StartDate ==== \(self.startDateTime)")
            self.strStartDateTime = AppConstant.formattedDate(date: self.startDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter7)!
            
            //service call to get Vehicle Category
            serviceCallToGetVehicleCategory()
            //Service call to update cab Timing
            self.serviceCallToGetCabTiming()
            
            isShowBtnContinue = false
            isSelectedDateTime = true
            viewConfirmBooking.isHidden = false
            tblViewOutstationBooking.reloadData()
        }
        
    }
    
    @objc func radioBtnOnewayAction(_ sender: UIButton){
        isOnewaySelected = true
        if self.tripType != 0{
            self.tripType = 1
        }
        tblViewOutstationBooking.reloadData()
        serviceCallToGetFareDetails()
    }
    
    @objc func radioBtnRoundTripAction(_ sender: UIButton){
        isOnewaySelected = false
        if self.tripType != 0{
            self.tripType = 2
        }
        tblViewOutstationBooking.reloadData()
        if isOnewaySelected == false && self.strEndDateTime != ""{
            serviceCallToGetFareDetails()
        }else{
            self.lblTotalFare.text  = "--"
            self.showAlert(alertTitle: "Alert!", alertMessage: "Select end date and time for rates")
        }
    }
    
    @objc func btnSelectDateTimeAction(_ sender: UIButton){
        if sender.tag == 0{
            self.isStartDateSelected = true
        }else{
            self.isStartDateSelected = false
        }
        
        self.performSegue(withIdentifier: "datePicker", sender: self)
    }
    
    @IBAction func viewBookRideForAction(_ sender: UIButton) {
        viewBackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewBackground.isHidden = false
        self.performSegue(withIdentifier: "book_ride_for", sender: self)
    }
    
     @IBAction func viewPaymentModeAction(_ sender: UIButton) {
        viewBackground.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewBackground.isHidden = false
        self.performSegue(withIdentifier: "payment_mode", sender: self)
    }
    
    @IBAction func viewApplyCouponAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "apply_coupon", sender: self)
    }
    
    @IBAction func totalFareViewAction(_ sender: Any) {
        if self.lblTotalFare.text != "--"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FareDetailsViewController") as! FareDetailsViewController
            vc.arrFareDettails = self.arrFareDettails
            vc.totalFare = self.totalFare
            vc.info = self.info
            vc.taxValue = self.taxValue
            vc.vehicleTypeId = self.vehicleTypeId
            
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            
            vc.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            vc.view.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                vc.view.alpha = 1.0
                vc.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })

            self.addChild(vc)
            self.view.addSubview(vc.view)
        }else{
            if isOnewaySelected == false && self.strEndDateTime == ""{
                self.showAlert(alertTitle: "Total Fare", alertMessage: "Make sure that end date and time must be selected!")
            }
        }
    }
    
    @IBAction func btnConfirmBookingAction(_ sender: Any) {
        let vehicleTypeBo = arrOutstationBookingCabTypes[selectedCabIndex]
        
        if (self.lblTotalFare.text  == "--") || (isOnewaySelected == false && self.strEndDateTime == ""){
            self.showAlert(alertTitle: "Alert!", alertMessage: "Select end date and time for rates")
        }else{
            if waitingTime == "No \(vehicleTypeBo.name!)"{
                self.showAlert(alertTitle: "Alert!", alertMessage: "No Cab available of \(vehicleTypeBo.name!) category.Please check cab in other category")
                return
            }else{
                self.viewConfirmBooking.isHidden = true
                self.viewFindingDriver.isHidden = false
                self.progressBar.wait()
                lblPageTitle.text = "Finding a ride"
                
                viewDriverInfo.showAnimatedGradientSkeleton()
                lblDriverName.showAnimatedGradientSkeleton()
                lblVehicleName.showAnimatedGradientSkeleton()
                lblVehicleNumber.showAnimatedGradientSkeleton()
                lblRating.showAnimatedGradientSkeleton()
                cancelRideBtn.showAnimatedGradientSkeleton()
                
                self.serviceCallToConfirmBooking()
            }
            
        }
    }
    
    @IBAction func btnCancelRequestAction(_ sender: Any) {
        self.showAlertForCancelRequest(strTitle: "Alert!", strDescription: "Are you sure you want to cancel the ride?", delegate: self)
    }
    
    @objc func bookingErrorNotification(notification: Notification){
        self.viewFindingDriver.isHidden = true
        self.viewConfirmBooking.isHidden = false
        lblPageTitle.text = "Book Your Outstation Ride"
    }
    
    //MARK: Api Service Call Method
    func serviceCallToGetVehicleCategory() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!),
                "ride_id": "2"
            ]
            
            print("url===\(AppConstant.getOutstationVehicleCategoryUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.getOutstationVehicleCategoryUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
//                    print("url===\(AppConstant.getOutstationVehicleCategoryUrl)")
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//Success
                                    self.arrOutstationBookingCabTypes.removeAll()
                                    if let arrVehicleCategoryInfo = dict["data"] as? [[String: Any]] {
                                        var index: Int = -1
                                        if (arrVehicleCategoryInfo.count > 0) {
                                            for dictInfo in arrVehicleCategoryInfo {
                                                index = index + 1
                                                let vehicleTypeBo = VehicleTypeBO()

                                                if let vehicleId = dictInfo["cat_id"] as? String {
                                                    vehicleTypeBo.vehicleId = vehicleId
                                                }
                                                if let vehicleName = dictInfo["cat_name"] as? String {
                                                    vehicleTypeBo.name = vehicleName
                                                }
                                                if let vehicleSelectedImage = dictInfo["image_blue"] as? String {
                                                    vehicleTypeBo.selectedImage = vehicleSelectedImage
                                                }
                                                if let vehicleUnselectedImage = dictInfo["image_white"] as? String {
                                                    vehicleTypeBo.unselectedImage = vehicleUnselectedImage
                                                }
                                                if let minTime = dictInfo["min_time"] as? String {
                                                    vehicleTypeBo.minTime = minTime
                                                }
                                                if let vehicle_cnt = dictInfo["vehicle_cnt"] as? String {
                                                    vehicleTypeBo.vehicle_Cnt = vehicle_cnt
                                                }
                                                vehicleTypeBo.rowIndex = index
                                                if (vehicleTypeBo.rowIndex == 0) {//Default Selected Vehicle
                                                    vehicleTypeBo.isSelected = true
                                                }else {
                                                    vehicleTypeBo.isSelected = false
                                                }
                                                self.arrOutstationBookingCabTypes.append(vehicleTypeBo)
                                            }
                                            if self.tripType == 1{
                                                if self.arrOutstationBookingCabTypes.count > 0 && self.strStartDateTime != "" {
                                                    self.serviceCallToGetFareDetails()
                                                }
                                            }else{
                                                if self.arrOutstationBookingCabTypes.count > 0 && (self.strStartDateTime != "" && self.strEndDateTime != ""){
                                                    self.serviceCallToGetFareDetails()
                                                }else if self.isOnewaySelected == false && self.strEndDateTime == ""{
                                                    self.lblTotalFare.text  = "--"
                                                    self.showAlert(alertTitle: "Alert!", alertMessage: "Select end date & time for rates")
                                                }
                                            }
                                            
                                            self.vehicleTypeCollectionView.reloadData()
                                        }
                                    }
                                    AppConstant.hideHUD()

                                }else if (status == 3){
                                    AppConstant.hideHUD()
                                }else{//Logout from the app
                                    AppConstant.hideHUD()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    
    func serviceCallToGetCabTiming() {
        if AppConstant.arrVehicleType.count == 0{
            return
        }
        if AppConstant.hasConnectivity() {
            // Loader NVActivityIndicatorView
            let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: vehicleTypeCollectionView.frame.origin.x, y: vehicleTypeCollectionView.frame.origin.y + 50, width: AppConstant.screenSize.width, height: 25), type: .ballPulse, color: AppConstant.colorThemeBlue, padding: 3)
            activityIndicatorView.backgroundColor = UIColor.white
            self.vehicleTypeCollectionView.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
            
            //AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!),
                "ride_id": "2"
            ]
            
            print("url===\(AppConstant.getOutstationCabTimeUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getOutstationCabTimeUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Cab Timing : \(response)")
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            
                            if let status = dict["status"] as? Int {
                                if(status == 1){
                                    if let arrTiming = dict["data"] as? [[String: Any]]{
                                        for dictData in arrTiming{
                                            var cabTimingBo = CabTimingBO()
                                            if let cat_id = dictData["cat_id"] as? String{
                                                cabTimingBo.cat_id = cat_id
                                            }
                                            if let cat_name = dictData["cat_name"] as? String{
                                                cabTimingBo.cat_name = cat_name
                                            }
                                            if let count = dictData["count"] as? Int{
                                                cabTimingBo.count = count
                                            }
                                            if let time = dictData["time"] as? String{
                                                cabTimingBo.time = time
                                            }
                                            
                                            self.arrCabTiming.append(cabTimingBo)
                                        }
                                    }
                                    self.vehicleTypeCollectionView.reloadData()
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                            activityIndicatorView.stopAnimating()
                            self.vehicleTypeCollectionView.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetFareDetails() {
        if AppConstant.hasConnectivity() {
            // Loader NVActivityIndicatorView
            let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: vehicleTypeCollectionView.frame.width/2 - 100, y: vehicleTypeCollectionView.frame.origin.y + 50, width: 200, height: 30), type: .ballPulse, color: AppConstant.colorThemeBlue, padding: 3)
            activityIndicatorView.backgroundColor = UIColor.white
            self.vehicleTypeCollectionView.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
            
           // AppConstant.showHUD()
            
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            let dropLat : NSNumber = NSNumber(value: (dropCoordinate?.latitude)!)
            let dropLng : NSNumber = NSNumber(value: (dropCoordinate?.longitude)!)
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "ride_id" : "2",
                "trip_type": tripType == 1 ? 1 : 2,
                "start_date": AppConstant.formattedDate(date: self.startDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter2)!,
                "start_time": AppConstant.formattedDate(date: self.startDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter6)!,
                "end_date": tripType == 1 ? "" : AppConstant.formattedDate(date: self.endDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter2)!,
                "end_time": tripType == 1 ? "" : AppConstant.formattedDate(date: self.endDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter6)!,
                "dest_city": self.dropCityName,
                "cat_id" : arrOutstationBookingCabTypes[selectedCabIndex].vehicleId,
                "city" : AppConstant.cityName,
                "src_lat" : String(describing: pickLat),
                "src_lon" : String(describing: pickLng),
                "des_lat" : String(describing: dropLat),
                "des_lon" : String(describing: dropLng),
                "promo_code" : isCouponApplied ? promoCode.title : ""
            ]
            
            
            print("url===\(AppConstant.getEstimatedCostUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getEstimatedCostUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            self.arrFareDettails.removeAll()
                            if let status = dict["status"] as? String {
                                if(status == "0"){
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                }else  if(status == "1"){
                                    if let totalFare = dict["cost"] as? Int {
                                        self.totalFare = "\(totalFare)"
                                    }
                                    if let taxValue = dict["tax"] as? Int {
                                        self.taxValue = "\(taxValue)"
                                    }
                                    if let info = dict["info_str"] as? String {
                                        self.info = info
                                        debugPrint(self.info)
                                    }
                                    if let fareInfoArray = dict["info_arry"] as? [[String: Any]]{
                                        debugPrint(fareInfoArray)
                                        for item in fareInfoArray {
                                            let fareDetailsBo = FareDetails()
                                            
                                            if let titleKey = item["key"] as? String {
                                                fareDetailsBo.title = titleKey.replacingOccurrences(of: "&#x20b9;", with: "₹ ")
                                            }
                                            if let value = item["value"] as? Int {
                                                fareDetailsBo.price = String(value)
                                            }
                                            if let value = item["value"] as? String {
                                                fareDetailsBo.price = value
                                            }
                                            if let type = item["type"] as? Int{
                                                fareDetailsBo.type = type
                                            }
                                            
                                            self.arrFareDettails.append(fareDetailsBo)
                                        }
                                    }
                                    
                                    self.lblTotalFare.text = self.totalFare == "--" ? "--" : "₹ \(self.totalFare)/-"
                                    
                                }else  if(status == "2"){
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.goToLandingScreen()
                                }
                            }
                            
                            activityIndicatorView.stopAnimating()
                            self.tblViewOutstationBooking.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetEstimatedCityDistance() {
        if AppConstant.hasConnectivity() {
           // AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "src_city" : self.currentCity!,
                "dst_city": self.dropCityName
            ]
            
            print("url===\(AppConstant.getCityDistanceUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getCityDistanceUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if status == "0"{
                                    if let errMsg = dict["error"] as? String{
                                        self.showAlert(alertTitle: errMsg, alertMessage: "")
                                    }
                                }else  if status == "1"{
                                    if let km = dict["km"] as? String {
                                        self.estimatedKm = km
                                    }
                                    if let kmVal = dict["km_val"] as? Int {
                                        self.estimatedKmVal = kmVal
                                    }
                                    if let oneWay = dict["one_way"] as? Int {
                                        self.tripType = oneWay
                                    }
                                    self.isOnewaySelected = self.tripType == 1 ? true : false
                                }
                            }
                            self.tblViewOutstationBooking.reloadData()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToConfirmBooking() {
        if AppConstant.hasConnectivity() {
            //AppConstant.showHUD()
            DispatchQueue.main.async {
                self.viewFindingDriver.isHidden = false
                self.progressBar.wait()
            }
            
            let pickUPAddress = self.pickUpLocation
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            let dropLat : NSNumber = NSNumber(value: (dropCoordinate?.latitude)!)
            let dropLng : NSNumber = NSNumber(value: (dropCoordinate?.longitude)!)
            
            let params: Parameters = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "book_now" : "1",
                "book_for" : lblBookRideForContact?.text == "Myself" ? "1" : "2",
                "ride_id" : "2",
                "cat_id" : arrOutstationBookingCabTypes[selectedCabIndex].vehicleId,
                "src_loc" : String(describing: pickUPAddress),
                "src_lat" : String(describing: pickLat),
                "src_lon" : String(describing: pickLng),
                "dest_city": self.dropCityName,
                "des_lat": String(describing: dropLat),
                "des_lon": String(describing: dropLng),
                "schedule_date": AppConstant.formattedDateFromString(dateString: self.strStartDateTime, withFormat: StringConstant.dateFormatter7, ToFormat: StringConstant.dateFormatter2)!,
                "schedule_time": AppConstant.formattedDateFromString(dateString: self.strStartDateTime, withFormat: StringConstant.dateFormatter7, ToFormat: StringConstant.dateFormatter6)!,
                "trip_type": tripType == 1 ? 1 : 2,
                "end_date": tripType == 1 ? "" : AppConstant.formattedDateFromString(dateString: self.strEndDateTime, withFormat: StringConstant.dateFormatter7, ToFormat: StringConstant.dateFormatter2)!,
                "end_time": tripType == 1 ? "" : AppConstant.formattedDateFromString(dateString: self.strEndDateTime, withFormat: StringConstant.dateFormatter7, ToFormat: StringConstant.dateFormatter6)!,
                "pay_mode" : AppConstant.selectedPaymentMode == "Cash" ? "1" : "2",
                "oth_mobile": lblBookRideForContact?.text == "Myself" ? "" : AppConstant.bookRideForContactNumber,
                "promo_code" : isCouponApplied ? promoCode.title : "",
                "fare": self.totalFare
            ]
            
            print("Url===\(AppConstant.confirmBookingUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.confirmBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    //AppConstant.hideHUD()
                    DispatchQueue.main.async {
                        self.viewFindingDriver.isHidden = false
                        self.progressBar.signal()
                    }
                        
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            
                            if let status = dict["status"] as? String {
                                if status == "0" {
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                    
                                }else  if(status == "1"){
                                    if let bookingId = dict["book_id"] as? String {
                                        AppConstant.saveInDefaults(key: StringConstant.book_id, value: bookingId)
                                    }
                                    if let waitTime = dict["wait_time"] as? Int {
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 90) { // change 30 to desired number of seconds
                                            //Hide Loader
                                            self.viewFindingDriver.isHidden = true
                                            self.progressBar.signal()
                                            
                                            if(!AppConstant.isBookingConfirm){
                                                //Call api to confirm your booking
                                                self.serviceCallToGetBookingStatusDetails()
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                }else  if(status == "2"){
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                }
                            }
                            
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    func serviceCallToGetBookingStatusDetails() {
        if AppConstant.hasConnectivity() {
            DispatchQueue.main.async {
                self.viewFindingDriver.isHidden = false
                self.progressBar.wait()
            }
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "book_id" : AppConstant.retrievFromDefaults(key: StringConstant.book_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("params===\(params!)")
            print("Api===\(AppConstant.getBookingStatusDetailsUrl)")
            
            AF.request( AppConstant.getBookingStatusDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    DispatchQueue.main.async {
                        self.viewConfirmBooking.isHidden = false
                        self.viewFindingDriver.isHidden = true
                        self.progressBar.signal()
                    }
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if(status == "1"){//Success
                                    if let bookStatus = dict["book_status"] as? String{
                                        if bookStatus == StringConstant.cancelByAdmin{
                                            if let book_Status_str = dict["book_status_str"] as? String{
                                                self.viewConfirmBooking.isHidden = false
                                                self.showAlert(alertTitle: book_Status_str, alertMessage: "")
                                            }
                                        }else if bookStatus == StringConstant.bookingConfirm {
                                            if let dictBookInfo = dict["book_info"] as? [String: Any]{
                                                if let bookOtp = dictBookInfo["book_otp"] as? String{
                                                    self.bokingStatusBo.driverBo.otp = bookOtp
                                                }
                                                if let pickPnt = dictBookInfo["pick_pnt"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.pick_pnt = pickPnt
                                                }
                                                if let dropPnt = dictBookInfo["drop_pnt"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.drop_pnt = dropPnt
                                                }
                                                if let srcLat = dictBookInfo["src_lat"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.src_lat = srcLat
                                                }
                                                if let srcLon = dictBookInfo["src_lon"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.src_lon = srcLon
                                                }
                                                if let desLat = dictBookInfo["des_lat"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.des_lat = desLat
                                                }
                                                if let desLon = dictBookInfo["des_lon"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.des_lon = desLon
                                                }
                                                if let rideId = dictBookInfo["ride_id"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.ride_id = rideId
                                                }
                                                if let rideName = dictBookInfo["ride_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.ride_name = rideName
                                                }
                                                if let catName = dictBookInfo["cat_name"] as? String{
                                                    self.bokingStatusBo.bookingInfoBo.cat_name = catName
                                                }
                                                if let waitTime = dictBookInfo["wait_time"] as? Int{
                                                    self.bokingStatusBo.bookingInfoBo.wait_time = waitTime
                                                }
                                                if let rideCompleted = dictBookInfo["ride_completed"] as? String{
                                                    if rideCompleted == ""{
                                                        self.bokingStatusBo.bookingInfoBo.ride_completed = 0
                                                    }else{
                                                    self.bokingStatusBo.bookingInfoBo.ride_completed = Int(rideCompleted)!
                                                    }
                                                }
                                                
                                            }
                                            if let dictDriverInfo = dict["driver_info"] as? [String: Any]{
                                                if let driverId = dictDriverInfo["driver_id"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_id = driverId
                                                }
                                                if let driverName = dictDriverInfo["driver_name"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_name = driverName
                                                }
                                                if let driverMobile = dictDriverInfo["driver_mobile"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_mobile = driverMobile
                                                }
                                                if let driverRating = dictDriverInfo["driver_rating"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_rating = driverRating
                                                }
                                                if let driverImage = dictDriverInfo["driver_image"] as? String {
                                                    self.bokingStatusBo.driverBo.driver_image = driverImage
                                                }
                                                if let vehcile_reg_No = dictDriverInfo["vehcile_reg_no"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_registration_number = vehcile_reg_No
                                                }
                                                if let vehcile_brand_Name = dictDriverInfo["vehcile_brand_name"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_brand_name = vehcile_brand_Name
                                                }
                                                if let vehcile_model_Name = dictDriverInfo["vehcile_model_name"] as? String {
                                                    self.bokingStatusBo.driverBo.vehcile_model_name = vehcile_model_Name
                                                }
                                                if let vehicleImage = dictDriverInfo["vehicle_image"] as? String {
                                                    self.bokingStatusBo.driverBo.vehicle_image = vehicleImage
                                                }
                                            }
                                            
                                            //Move to DriverInfo Screen
                                            self.performSegue(withIdentifier: "driverToReachPickupPoint", sender: self)
                                            
                                        }else{
                                            self.viewConfirmBooking.isHidden = false
                                        }
                                        
                                    }
                                    
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToCancelBooking() {
        AppConstant.isBookingConfirm = false
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id), "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken), "policy_id": "0", "book_id": AppConstant.retrievFromDefaults(key: StringConstant.book_id)]
            
            print("url===\(AppConstant.cancelBookingUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.cancelBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    DispatchQueue.main.async {
                        self.viewFindingDriver.isHidden = true
                        self.progressBar.signal()
                    }
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                    self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    //MARK: Present GSMAutocompleteViewController Method
    func presentGSMAutocompleteViewController() {
        if AppConstant.hasConnectivity(){
            if pickUpCoordinate?.latitude == nil{
                return
            }
            
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = GMSPlacesAutocompleteTypeFilter.city
            acController.autocompleteFilter = filter
            
            let bounds: GMSCoordinateBounds = getCoordinateBounds(latitude: (pickUpCoordinate?.latitude)!, longitude: (pickUpCoordinate?.longitude)!)
//            acController.autocompleteBounds = GMSCoordinateBounds(coordinate: bounds.northEast, coordinate: bounds.southWest)
            present(acController, animated: true, completion: nil)
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    /* Returns Bounds */
    func getCoordinateBounds(latitude:CLLocationDegrees ,
                             longitude:CLLocationDegrees,
                             distance:Double = 0.01)->GMSCoordinateBounds{
        let center = CLLocationCoordinate2D(latitude: latitude,
                                            longitude: longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + distance, longitude: center.longitude + distance)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - distance, longitude: center.longitude - distance)
        
        return GMSCoordinateBounds(coordinate: northEast,
                                   coordinate: southWest)
        
    }
    
    //MARK: Alert Method
    func showAlertForCancelRequest(strTitle: String,strDescription: String,delegate: AnyObject?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: strTitle, message: strDescription, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (action) in
                self.serviceCallToCancelBooking()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.view.endEditing(true)
        if (segue.identifier == "book_ride_for"){
            let vc = segue.destination as! BookRideForViewController
            vc.delegate = self
            vc.type = segue.identifier!
        }else if (segue.identifier == "payment_mode"){
            let vc = segue.destination as! PaymentModeViewController
            vc.delegate = self
        }else if (segue.identifier == "apply_coupon"){
            let vc = segue.destination as! ApplyCouponViewController
            vc.delegate = self
            vc.selectedCouponBo = self.promoCode
        }else if (segue.identifier == "datePicker"){
            let vc = segue.destination as! DatePickerViewController
            vc.delegate = self
            if self.isStartDateSelected == true {
                vc.selectedDate = self.startDateTime
            }else{
                vc.selectedDate = self.endDateTime
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
