//
//  HomeScreenController.swift
//  Taxi Booking
//
//  Created by OdiTek Solutions on 26/12/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import GooglePlacesSearchController
import Alamofire
import SDWebImage
import DeviceGuru
import FirebaseAnalytics

class HomeScreenController: UIViewController , CLLocationManagerDelegate , GMSMapViewDelegate , UITextFieldDelegate , GMSAutocompleteViewControllerDelegate , SlideMenuControllerDelegate, ChoosePreviousBookedLocationDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var oneWayBtn: UIButton!
    @IBOutlet weak var rentalBtn: UIButton!
    @IBOutlet weak var outStationBtn: UIButton!
    @IBOutlet weak var btnGoForBooking: UIButton!
    
    @IBOutlet weak var viewActiveBooking: UIView!
    @IBOutlet weak var lblActiveBookingNo: UILabel!
    @IBOutlet weak var lblActiveBookings: UILabel!

    @IBOutlet weak var myLocationView: UIView!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var mapPinImage: UIImageView!
    @IBOutlet weak var pickUpLocationTf: UITextField!
    @IBOutlet weak var dropLocationTf: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet weak var mapPinView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet var viewNavBar: UIView!
    @IBOutlet var dropLocationView: UIView!
    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var dropLocationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imgViewMyCurrentLocation: UIImageView!
    @IBOutlet var statusBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var viewServiceUnavailable: UIView!
    @IBOutlet var viewServiceUnavailableVehicle: UIView!
    @IBOutlet var lblServiceUnavailableMsg: UILabel!
    
    var currentCity : String = ""
    var vehicleType : String = ""
    var vehicleTypeId : String = ""
    var totalFare : String = ""
    var info : String = ""
    var fareInfoArray = [Dictionary<String, Any>]()
    var cityId : String = ""
    var userid : String = ""
    var token : String = ""
    var selectedTf : String!
    var isPinShowPickUp : Bool = true
    var isPinShowDrop : Bool = false
    var hasGotDriverLocations : Bool = false
    var isFromAutoCompleteViewController : Bool = false
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    var dropCoordinate : CLLocationCoordinate2D? = nil
    var currentCoordinate : CLLocationCoordinate2D? = nil
    var oldCabCoordinate : CLLocationCoordinate2D? = nil
    var arrCabInfo = [CabLocationBO]()
    var driverProfileBo = BookingStatusBO()
    var arrTime = [Int]()
    var myCurrentLocation: CLLocation? = nil
    var isForBookLater : Bool = false
    var isForBookShareRide : Bool = false
    var isForOneWay : Bool = true
    var pickupTime: String = ""
    var lastZoom: CGFloat = 15
    var currentAddress: String = ""
    var selectedVehicleTypeIndex: Int! = 0
    var waitingTime: String = ""
    var isFromPrevBookLoc : Bool = false
    var activeBookingCount = "0"
    var isServiceAvailable : Bool = true
    var timer = Timer()
    var index = 0
    var driverPins = [GMSMarker]()
    var driverPin = GMSMarker()
    var arrCabInfoBackUp = [CabLocationBO]()
    var selectedButtonIndex: Int = 0
    
    var movingcar: UIImage = UIImage.init(named: StringConstant.mini_car_map_pin)!
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        _locationManager.activityType = . automotiveNavigation
        _locationManager.startMonitoringSignificantLocationChanges()
        _locationManager.allowsBackgroundLocationUpdates = true// allow in background

        return _locationManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
        
        // Scheduling timer to Call the function with the interval of 5 seconds
        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.serviceCallToGetCabLocation), userInfo: nil, repeats: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    override func viewDidLayoutSubviews() {
        topView.setShadowWithCornerRadius(corners: 20.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppConstant.setTrackScreenName(screenName: "HomePage", screenClass: "HomeScreenController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initDesign(){
   //     self.vehicleTypeCollectionView.isHidden = true
        self.addressLabel.text = "Getting address..."
        
        //Manage for iPhone X
        if (AppConstant.screenSize.height >= 812) {
            statusBarHeightConstraint.constant = 44
        }
        
        if AppConstant.screenSize.width > 320{ //Above iPhone 5
            navBarHeightConstraint.constant = 50
        }
        
        viewActiveBooking.layer.cornerRadius = 5
        viewActiveBooking.clipsToBounds = true
        viewActiveBooking.layer.borderColor = AppConstant.colorThemeSeparatorGray.cgColor
        viewActiveBooking.layer.borderWidth = 0.5
        
        lblActiveBookingNo.layer.cornerRadius = lblActiveBookingNo.frame.size.width/2
        lblActiveBookingNo.clipsToBounds = true
        
        self.viewServiceUnavailable.isHidden = true
        self.viewActiveBooking.isHidden = true
        
        googleMapView.delegate = self
        pickUpLocationTf.delegate = self
        dropLocationTf.delegate = self
        
        oneWayBtn.layer.cornerRadius = oneWayBtn.frame.size.width/2
        oneWayBtn.layer.borderColor = UIColor.init(hexString: "1565C0").cgColor
        oneWayBtn.layer.borderWidth = 2
        oneWayBtn.clipsToBounds = true

        rentalBtn.layer.cornerRadius = rentalBtn.frame.size.width/2
        rentalBtn.layer.borderColor = UIColor.white.cgColor
        rentalBtn.layer.borderWidth = 2
        rentalBtn.clipsToBounds = true
        
        outStationBtn.layer.cornerRadius = outStationBtn.frame.size.width/2
        outStationBtn.layer.borderColor = UIColor.white.cgColor
        outStationBtn.layer.borderWidth = 2
        outStationBtn.clipsToBounds = true
        outStationBtn.titleLabel?.textAlignment = .center
        outStationBtn.setTitle("Out \n station", for: .normal)
        
        viewServiceUnavailableVehicle.layer.cornerRadius = viewServiceUnavailableVehicle.frame.size.width / 2
        viewServiceUnavailableVehicle.clipsToBounds = true
        
        myLocationView.layer.borderColor = UIColor.lightGray.cgColor
        myLocationView.layer.borderWidth = 1
        myLocationView.layer.cornerRadius = myLocationView.frame.size.width / 2
        myLocationView.clipsToBounds = true
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        selectedTf = "Pickup"
        
        let userCurrentLocation = UITapGestureRecognizer(target: self, action: #selector(self.handleUserCurrentLocationTap(_:)))
        self.myLocationView?.isUserInteractionEnabled = true
        self.myLocationView?.addGestureRecognizer(userCurrentLocation)
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.disableDragFromNotification(notification:)), name: Notification.Name("DisableMapFromDragNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBookingStatus), name: Notification.Name(StringConstant.notification_bookStatus), object: nil)
        
//        loadVehicleType()
        selectedVehicleTypeIndex = 0
        
        //Enable left swipe to back
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    // MARK: - Button Actions
    @IBAction func btnOneWayAction(_ sender: UIButton) {
        selectedButtonIndex = 0
        btnGoForBooking.setTitleWithOutAnimation(title: "Go for One Way Booking")
        topViewHeightConstraint.constant = 91
        dropLocationViewHeightConstraint.constant = 45
        oneWayBtn.backgroundColor = UIColor.init(hexString: "FFC107")
        oneWayBtn.layer.borderColor = UIColor.init(hexString: "1565C0").cgColor
        oneWayBtn.setTitleColor(AppConstant.colorThemeBlue, for: .normal)
        rentalBtn.backgroundColor = UIColor.init(hexString: "1565C0")
        rentalBtn.layer.borderColor = UIColor.white.cgColor
        rentalBtn.setTitleColor(UIColor.white, for: .normal)
        outStationBtn.backgroundColor = UIColor.init(hexString: "1565C0")
        outStationBtn.layer.borderColor = UIColor.white.cgColor
        outStationBtn.setTitleColor(UIColor.white, for: .normal)
        dropLocationView.isHidden = false
    }
    
    @IBAction func btnRentalAction(_ sender: UIButton) {
        selectedButtonIndex = 1
        btnGoForBooking.setTitleWithOutAnimation(title: "Go for Rental Booking")
        topViewHeightConstraint.constant = 46
        dropLocationViewHeightConstraint.constant = 0
        oneWayBtn.backgroundColor = UIColor.init(hexString: "1565C0")
        oneWayBtn.layer.borderColor = UIColor.white.cgColor
        oneWayBtn.setTitleColor(UIColor.white, for: .normal)
        rentalBtn.backgroundColor = UIColor.init(hexString: "FFC107")
        rentalBtn.layer.borderColor = UIColor.init(hexString: "1565C0").cgColor
        rentalBtn.setTitleColor(AppConstant.colorThemeBlue, for: .normal)
        outStationBtn.backgroundColor = UIColor.init(hexString: "1565C0")
        outStationBtn.layer.borderColor = UIColor.white.cgColor
        outStationBtn.setTitleColor(UIColor.white, for: .normal)
        dropLocationView.isHidden = true
    }
    
    @IBAction func btnOutstationAction(_ sender: UIButton) {
        selectedButtonIndex = 2
        btnGoForBooking.setTitleWithOutAnimation(title: "Go for Outstation Booking")
        topViewHeightConstraint.constant = 46
        dropLocationViewHeightConstraint.constant = 0
        oneWayBtn.backgroundColor = UIColor.init(hexString: "1565C0")
        oneWayBtn.layer.borderColor = UIColor.white.cgColor
        oneWayBtn.setTitleColor(UIColor.white, for: .normal)
        rentalBtn.backgroundColor = UIColor.init(hexString: "1565C0")
        rentalBtn.layer.borderColor = UIColor.white.cgColor
        rentalBtn.setTitleColor(UIColor.white, for: .normal)
        outStationBtn.backgroundColor = UIColor.init(hexString: "FFC107")
        outStationBtn.layer.borderColor = UIColor.init(hexString: "1565C0").cgColor
        outStationBtn.setTitleColor(AppConstant.colorThemeBlue, for: .normal)
        dropLocationView.isHidden = true
    }
    
    @IBAction func btnGoForOnewayAction(_ sender: Any) {
        if selectedButtonIndex == 0{
            if (self.dropLocationTf.text == ""){
                self.showAlert(alertTitle: "Drop location is required!", alertMessage: "")
            }else{
                self.performSegue(withIdentifier: "route", sender: self)
            }
        }else if selectedButtonIndex == 1{
            self.performSegue(withIdentifier: "rental_booking_rides", sender: self)
        }else{
            self.performSegue(withIdentifier: "outStationBooking", sender: self)
        }
        
    }
    
//    @IBAction func btnSearchLocationAction(_ sender: UIButton) {
//        if (sender.tag == 201){
//            selectedTf = "Pickup"
//        }
//        else {
//            selectedTf = "Drop"
//        }
//        self.presentGSMAutocompleteViewController()
//    }
    
    @IBAction func btnSlideMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnPreviousBookedLocation(_ sender: Any) {
        self.performSegue(withIdentifier: "previous_booked_locations", sender: self)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "camera.zoom" {
            // this static variable will hold the last value between invocations.
            let currentZoom: CGFloat = CGFloat(googleMapView.camera.zoom)
            
            if lastZoom != currentZoom {
                //Zoom level has actually changed!
                print("Zoom level has actually changed")
                if let zoom = googleMapView?.camera.zoom {
                    self.lastZoom = CGFloat(zoom)
                }
            }
        }
    }
    
    @IBAction func activeBookingsBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "total_trips", sender: self)
    }
    
    // MARK: - Choose Previous Booked Location Delegate
    func selectedObject(obj: PreviousBookedLocationBO) {
        dropLocationTf?.text = obj.addressName
        addressLabel?.text = obj.addressName
        selectedTf = "Drop"
        isPinShowDrop = true
        isFromPrevBookLoc = true
        self.pickUpLocationTf.textColor = AppConstant.colorThemeLightGray
        self.dropLocationTf.textColor = AppConstant.colorThemeBlack
        
        dropCoordinate = CLLocationCoordinate2D(latitude: (obj.latitude) , longitude: (obj.longitude) )
        let camera = GMSCameraPosition.camera(withLatitude: (dropCoordinate?.latitude)! ,longitude: (dropCoordinate?.longitude)! , zoom: 15)
        self.googleMapView.animate(to: camera)
        
    }
    
    // MARK: - Google Map Delegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            googleMapView.isMyLocationEnabled = true
//            googleMapView.settings.myLocationButton = true
            break
        case .denied:
            AppConstant.showAlertToEnableLocation()
            break
        default:
            break
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        print("My current location \(location)")
        myCurrentLocation = location
        self.serviceCallToRegisterDeviceInfo()
        self.serviceCallToGetCabLocation()
        
        googleMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (AppConstant.isSlideMenu == false){
            googleMapView.isMyLocationEnabled = true
            
            DispatchQueue.main.async {
                if (gesture) {
                    self.addressLabel.text = "Getting address..."
                    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {() -> Void in
                        self.view.layoutIfNeeded()
        
                        //Hide Active Booking View
                        self.viewActiveBooking.isHidden = true
                        
                        var topViewFrame = self.topView.frame
                        topViewFrame.origin.y = topViewFrame.origin.y - 250
                        self.topView.frame = topViewFrame
                        
                        var bottomViewFrame = self.bottomView.frame
                        bottomViewFrame.origin.y = bottomViewFrame.origin.y + bottomViewFrame.size.height + 250
                        self.bottomView.frame = bottomViewFrame
                        
                    }, completion: {(_ finished: Bool) -> Void in
                    })
                    self.myLocationView.isHidden = true
                }
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        guard let currCoordinate = self.googleMapView.myLocation?.coordinate else {
            return
        }
        
        self.currentCoordinate = self.googleMapView.myLocation?.coordinate
        
        print("pickup coordinate: \(coordinate)")
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            if(self.selectedTf == "Pickup"){
                self.pickUpCoordinate = coordinate
                self.pickUpLocationTf.text = lines.joined(separator: "\n")
                self.addressLabel.text = lines.joined(separator: "\n")
                
                print("pickup Loc = \(self.addressLabel.text!)")
                
                //Service call to show velicle listing
                self.serviceCallToGetVehicleCategory()
                
                //Set current address
                if self.currentAddress == ""{
                    self.currentAddress = self.addressLabel.text!
                    self.serviceCallToRegisterDeviceInfo()
                }else{
                    //Api call to get cab location
                    //self.serviceCallToGetCabLocation()
                }
                
                let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                print("-------------------")
                print(self.myCurrentLocation)
                print(location)
                
                CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                    print(location)
                    
                    if error != nil {
                        print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                        return
                    }
                    
                    if (placemarks!.count) > 0 {
                        let pm = placemarks?[0]
                        print("you are in city ->",String(describing: pm!.locality))
                        self.currentCity = pm!.locality!
                        AppConstant.cityName = self.currentCity
                        
                        if (pm?.locality) != nil{
                            //Save in UserDefaults
                            var address: String = ""
                            // City
                            if let city = pm?.locality as NSString? {
                                address = city as String
                            }
                            // Country
                            if let country = pm?.country as NSString? {
                                address = address + ", " + (country as String)
                            }
                            AppConstant.saveInDefaults(key: StringConstant.current_address, value: address)
                        }
                        
                    }
                    else {
                        print("Problem with the data received from geocoder")
                    }
                })
                Analytics.setUserProperty(self.currentCity, forName: "CITY")
            }
            else {
                if self.isFromPrevBookLoc == false{
                    DispatchQueue.main.async {
                        self.dropCoordinate = coordinate
                        self.dropLocationTf.text = lines.joined(separator: "\n")
                        self.addressLabel.text = lines.joined(separator: "\n")
                    }
                }
            }
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {() -> Void in
                self.view.layoutIfNeeded()
                if(self.bottomView.frame.origin.y == (self.bottomView.frame.origin.y + self.bottomView.frame.size.height)){
                    
                    //Show Active Booking View
                    if self.isServiceAvailable{
                        self.viewActiveBooking.isHidden = self.activeBookingCount == "0" ? true : false
                    }
                    
                    var topViewFrame = self.topView.frame
                    topViewFrame.origin.y = topViewFrame.origin.y + 250
                    self.topView.frame = topViewFrame
                    
                    var bottomViewFrame = self.bottomView.frame
                    bottomViewFrame.origin.y = bottomViewFrame.origin.y - bottomViewFrame.size.height
                    self.bottomView.frame = bottomViewFrame
                    
                }
                self.myLocationView.isHidden = false
            }, completion: {(_ finished: Bool) -> Void in
            })
        }
    }
    // Touch drag and lift
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !isFromAutoCompleteViewController {
            reverseGeocodeCoordinate(position.target)
        }
        isFromAutoCompleteViewController = false
        print("Touch drag and lift")
    }
    // Tap
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
    }
    
    @objc func handleUserCurrentLocationTap(_ sender: UITapGestureRecognizer? = nil) {
        
        guard let lat = self.googleMapView.myLocation?.coordinate.latitude,
            let lng = self.googleMapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15)
        self.googleMapView.animate(to: camera)
        
    }
    
    func showCabsInMap(){
        googleMapView.clear()
        isForBookShareRide = false
        
        for index in 0..<self.arrCabInfo.count{
            let cabBo = self.arrCabInfo[index]
            let coordinateNew = CLLocationCoordinate2D(latitude: Double(cabBo.newLatitude!)!
                , longitude: Double(cabBo.newLongitude!)!)
            var coordinateOld = CLLocationCoordinate2D(latitude: Double(cabBo.newLatitude!)!
                , longitude: Double(cabBo.newLongitude!)!)
            if self.arrCabInfoBackUp.count > 0{
                let oldCabBo = self.arrCabInfoBackUp[index]
                coordinateOld = CLLocationCoordinate2D(latitude: Double(oldCabBo.newLatitude!)!
                    , longitude: Double(oldCabBo.newLongitude!)!)
            }
            
            movingcar = self.image(with: getCabImageFromId(id: cabBo.category_id!), scaledTo: CGSize(width: 35.0, height: 35.0))
            animateVehiceLocation(oldCoodinate: coordinateOld, newCoodinate: coordinateNew, image: getCabImageFromId(id: cabBo.category_id!), pin: cabBo.vehiclepin, bearing: Int(cabBo.bearing!)!)
            
            self.arrCabInfoBackUp = self.arrCabInfo
        }
        
    }
    
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: String, longitude: String) {
        let lat = Double(latitude)
        let lon = Double(longitude)
        
        let coordinates = CLLocationCoordinate2D(latitude:lat!
            , longitude:lon!)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
        marker.title = titleMarker
        marker.icon = self.image(with: iconMarker, scaledTo: CGSize(width: 14.0, height: 30.0))
        marker.map = googleMapView
    }
    
    @objc func disableDragFromNotification(notification: Notification){
        //Take Action on Notification
        if (AppConstant.isSlideMenu) {
            googleMapView.settings.scrollGestures = false
        }else{
            googleMapView.settings.scrollGestures = true
        }
        
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    func getCabImageFromId(id: String) -> UIImage{
        var image: UIImage = UIImage.init(named: StringConstant.micro_car_map_pin)!
        if id == "2"{
            image = UIImage.init(named: StringConstant.micro_car_map_pin)!
        }else if id == "3"{
            image = UIImage.init(named: StringConstant.mini_car_map_pin)!
        }else if id == "5"{
            image = UIImage.init(named: StringConstant.sedan_car_map_pin)!
        }else if id == "15"{
            image = UIImage.init(named: StringConstant.suv_car_map_pin)!
        }else{
            image = UIImage.init(named: StringConstant.auto_gray_map_pin)!
        }
        return image
    }
    
    // MARK: - TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.isFromPrevBookLoc = false
        if (textField == self.pickUpLocationTf){
            
            self.dropLocationTf.textColor = AppConstant.colorThemeLightGray
            self.pickUpLocationTf.textColor = AppConstant.colorThemeBlack
            
            selectedTf = "Pickup"
            let placeholderAttributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
            let attributedPlaceholder = NSAttributedString(string: "Enter pickup location", attributes: placeholderAttributes)
            if let aClass = [UISearchBar.self] as? [UIAppearanceContainer.Type] {
                if #available(iOS 9.0, *) {
                    UITextField.appearance(whenContainedInInstancesOf: aClass).attributedPlaceholder = attributedPlaceholder
                } else {
                    // Fallback on earlier versions
                }
            }
            if ((self.dropLocationTf.text == "") || (self.pickUpLocationTf.text == "")){
                presentGSMAutocompleteViewController()
            }
            else {
                
                if !isPinShowPickUp {
                    presentGSMAutocompleteViewController()
                }else{
                    self.addressLabel.text = self.pickUpLocationTf.text
                    let camera = GMSCameraPosition.camera(withLatitude: (pickUpCoordinate?.latitude)! ,longitude: (pickUpCoordinate?.longitude)! , zoom: 15)
                    self.googleMapView.animate(to: camera)
                    isPinShowPickUp = false
                    if !isPinShowDrop {
                        isPinShowDrop = true
                    }
                }
                
            }
            
        }
        else {//Drop Location
            
            self.pickUpLocationTf.textColor = AppConstant.colorThemeLightGray
            self.dropLocationTf.textColor = AppConstant.colorThemeBlack

            
            selectedTf = "Drop"
            let placeholderAttributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
            let attributedPlaceholder = NSAttributedString(string: "Enter drop location", attributes: placeholderAttributes)
            if let aClass = [UISearchBar.self] as? [UIAppearanceContainer.Type] {
                if #available(iOS 9.0, *) {
                    UITextField.appearance(whenContainedInInstancesOf: aClass).attributedPlaceholder = attributedPlaceholder
                } else {
                    // Fallback on earlier versions
                }
            }
            if (self.dropLocationTf.text == ""){
                presentGSMAutocompleteViewController()
            }
            else {
                if !isPinShowDrop {
                    presentGSMAutocompleteViewController()
                }else{
                    self.addressLabel.text = self.dropLocationTf.text
                    let camera = GMSCameraPosition.camera(withLatitude: (dropCoordinate?.latitude)! ,longitude: (dropCoordinate?.longitude)! , zoom: 15)
                    self.googleMapView.animate(to: camera)
                    isPinShowDrop = false
                    if !isPinShowPickUp {
                        isPinShowPickUp = true
                    }
                }
                
            }
            
        }
        
    }
    
    // MARK: - AutocompleteViewController Delegate
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("Place latitude: \(String(describing: place.coordinate.latitude))")
        print("Place longitude: \(String(describing: place.coordinate.longitude))")
        
        isFromAutoCompleteViewController = true
        
        if (selectedTf == "Pickup"){
            self.pickUpLocationTf.text = String(format: "%@, %@", place.name!, place.formattedAddress!)
            self.addressLabel.text = String(format: "%@, %@", place.name!, place.formattedAddress!)
            
            self.dropLocationTf.textColor = AppConstant.colorThemeLightGray
            self.pickUpLocationTf.textColor = AppConstant.colorThemeBlack
            
            self.pickUpCoordinate = place.coordinate
            isPinShowPickUp = true
            
            //Service call to show velicle listing
            self.serviceCallToGetVehicleCategory()
        }
        else {
            self.dropLocationTf.text = String(format: "%@, %@", place.name!, place.formattedAddress!)
            self.addressLabel.text = String(format: "%@, %@", place.name!, place.formattedAddress!)
            self.dropCoordinate = place.coordinate
            isPinShowDrop = true
            
            self.pickUpLocationTf.textColor = AppConstant.colorThemeLightGray
            self.dropLocationTf.textColor = AppConstant.colorThemeBlack
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude ,longitude: place.coordinate.longitude , zoom: 15)
        self.googleMapView.animate(to: camera)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        if (selectedTf == "Pickup"){
            isPinShowPickUp = true
        }
        else {
            isPinShowDrop = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Notification Method
    @objc func updateBookingStatus(){
        //Service call to show cabs in map
        self.serviceCallToGetCabLocation()
    }
    
    // MARK: - Api Service Call Method
    func serviceCallToRegisterDeviceInfo(){
        guard let lat = myCurrentLocation?.coordinate.latitude else{
            return
        }
        if AppConstant.hasConnectivity() {
            
            var deviceName = "iPhone undefined"
            if let devicename = DeviceGuru().hardwareDescription(){
                deviceName = devicename
            }
            
            //AppConstant.showHUD()
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "mobile": AppConstant.retrievFromDefaults(key: StringConstant.mobile),
                "device_id": AppConstant.retrievFromDefaults(key: StringConstant.deviceToken),
                "device_type": StringConstant.iOS,
                "latitude": NSNumber(value: (myCurrentLocation?.coordinate.latitude)!),
                "longitude": NSNumber(value: (myCurrentLocation?.coordinate.longitude)!),
                "location": self.currentAddress,
                "mobile_id": UIDevice.current.identifierForVendor!.uuidString,
                "os_version": UIDevice.current.systemVersion,
                "app_version": AppConstant.getAppVersion(),
                "device_name": deviceName
            ]
            
            print("params===\(params)")
            print("Url === \(AppConstant.registerDeviceInfoUrl)")
            
            AF.request(AppConstant.registerDeviceInfoUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    //AppConstant.hideHUD()
//                    print("Url: \(AppConstant.registerDeviceInfoUrl)")
                    debugPrint(response)
                    
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//Success
                                    if let hlw_Key = dict["hlw_key"] as? String{
                                        AppConstant.GoogleMapApiKey = hlw_Key
                                    }
                                }else{}
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    
    @objc func serviceCallToGetCabLocation() {
        guard let lat = myCurrentLocation?.coordinate.latitude else{
            return
        }
        if AppConstant.hasConnectivity() {
            //AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (myCurrentLocation?.coordinate.latitude)!),
                "longitude" : NSNumber(value: (myCurrentLocation?.coordinate.longitude)!)
            ]
            
            print("url===\(AppConstant.allCabLocationInfoUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.allCabLocationInfoUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    //AppConstant.hideHUD()
                    debugPrint("Cab Location : \(response)")
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            print("Dict Data === \(dict)")
                                        print(AppConstant.cabLocationInfoUrl)
                                        debugPrint(dict)
                                        if let status = dict["status"] as? Int {
                                            self.viewServiceUnavailable.isHidden = true
                                            if(status == 1){
                                                if let active_booking =  dict["active_booking"] as? String{
                                                    if active_booking > "0"{
                                                        self.activeBookingCount = active_booking
                                                        self.lblActiveBookingNo.text = active_booking
                                                        self.viewActiveBooking.isHidden = false
                                                    }else{
                                                        self.viewActiveBooking.isHidden = true
                                                    }
                                                }
                                                self.arrCabInfo.removeAll()
                                                if let arrCabLocationInfo = dict["data"] as? [[String: Any]]{
                                                    if(arrCabLocationInfo.count > 0){
                                                        for dictInfo in arrCabLocationInfo{
                                                            
                                                            let cabInfoBo = CabLocationBO()
                                                            
                                                            if let driverId = dictInfo["driver_id"] as? String {
                                                                cabInfoBo.driver_id = driverId
                                                            }
                                                            if let catId = dictInfo["cat_id"] as? String {
                                                                cabInfoBo.category_id = catId
                                                            }
                                                            if let latitude = dictInfo["latitude"] as? String {
                                                                cabInfoBo.newLatitude
                                                                    = latitude
                                                            }
                                                            if let longitude = dictInfo["longitude"] as? String {
                                                                cabInfoBo.newLongitude = longitude
                                                            }
                                                            if let geoId = dictInfo["geoid"] as? String {
                                                                cabInfoBo.geo_id = geoId
                                                            }
                                                            if let bearing = dictInfo["bearing"] as? String {
                                                                cabInfoBo.bearing = bearing
                                                            }
                                                            if let accuracy = dictInfo["accuracy"] as? String {
                                                                cabInfoBo.accuracy = accuracy
                                                            }
                                                            self.arrCabInfo.append(cabInfoBo)
                                                        }
                                                    }
                                                }
                                                
                                                //Set default Vehicle Type
                                                
                                                if (self.arrCabInfo.count > 0){
                                                    self.showCabsInMap()
                                                }else{
                                                    self.googleMapView.clear()
                                                }
                                                
                                                
                                            }else{
                                                if let msg = dict["msg"] as? String{
                            //                  AppConstant.showAlert(strTitle: msg, strDescription: "", delegate: self)
                                                }
                                            }
                                        }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetVehicleCategory() {
        print("XYZ")
        if AppConstant.hasConnectivity() {
//            AppConstant.showHUD()
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!)
            ]
            
            print("url===\(AppConstant.getVehicleCategoryUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.getVehicleCategoryUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    //AppConstant.hideHUD()
                    print("url===\(AppConstant.getVehicleCategoryUrl)")
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                     //   self.vehicleTypeCollectionView.isHidden = false
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                                       if(status == 1){//Success
                                                           AppConstant.arrVehicleType.removeAll()
                                                        if let arrVehicleCategoryInfo = dict["data"] as? [[String: Any]] {
                                                               var index: Int = -1
                                                               if (arrVehicleCategoryInfo.count > 0) {
                                                                   for dictInfo in arrVehicleCategoryInfo {
                                                                       index = index + 1
                                                                       let vehicleTypeBo = VehicleTypeBO()
                                                                       
                                                                       if let vehicleId = dictInfo["cat_id"] as? String {
                                                                           vehicleTypeBo.vehicleId = vehicleId
                                                                       }
                                                                       if let vehicleName = dictInfo["cat_name"] as? String {
                                                                           vehicleTypeBo.name = vehicleName
                                                                       }
                                                                       if let vehicleSelectedImage = dictInfo["image_blue"] as? String {
                                                                           vehicleTypeBo.selectedImage = vehicleSelectedImage
                                                                       }
                                                                       if let vehicleUnselectedImage = dictInfo["image_white"] as? String {
                                                                           vehicleTypeBo.unselectedImage = vehicleUnselectedImage
                                                                       }
                                                                       if let minTime = dictInfo["min_time"] as? String {
                                                                           vehicleTypeBo.minTime = minTime
                                                                       }
                                                                       vehicleTypeBo.rowIndex = index
                                                                       if (vehicleTypeBo.rowIndex == 0) {
                                                                           vehicleTypeBo.isSelected = true
                                                                       }else {
                                                                           vehicleTypeBo.isSelected = false
                                                                       }
                                                                       AppConstant.arrVehicleType.append(vehicleTypeBo)
                                                                   }
                                                               }
                                                           }
                                                       }else if (status == 3){
                            //                               AppConstant.hideHUD()
                                                           self.viewServiceUnavailable.isHidden = true
                                                           self.bottomView.isHidden = true
                                                           self.viewActiveBooking.isHidden = true
                                                        if let msg = dict["msg"] as? String{
                                                             AppConstant.showAlertForLogout(strTitle: msg, strDescription: "", delegate: self)
                                                           }
                                                           return
                                                       }else{}
                                                   }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    
    func serviceCallToVerifyBookingStatus() {
        if AppConstant.hasConnectivity() {
            // AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: "user_id"),
            ]
            print("params===\(params!)")
            
            AF.request( AppConstant.bookingVarificationUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        //let arrData = AppConstant.convertToArray(text: response.result)
                        if let arrData = value as? [[String: Any]] {
                            let dict = arrData[0]
                            if let status = dict["status"] as? Int {
                                if(status == 0){
                                    
                                }else  if(status == 1){
                                    
                                }else  if(status == 2){
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.goToLandingScreen()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func calculateMinimumTime(vehicleTypeId : String) -> String {
        
        arrTime.removeAll()
        for cabInfoBo in self.arrCabInfo {
            if(cabInfoBo.category_id == vehicleTypeId){
                let replaced = cabInfoBo.time?.replacingOccurrences(of: " min", with: "")
                let timeVal = (replaced! as NSString).integerValue
                print(timeVal)
                arrTime.append(timeVal)
            }
        }
        if (arrTime.count > 0){
            let minValue = arrTime.min()
            print("array count = ", arrTime.count)
            return "\(String(describing: minValue!)) min"
            
        }else{
            
            return "Not Aval"
        }
        
        
    }
    
    func presentGSMAutocompleteViewController() {
        if AppConstant.hasConnectivity(){
            if pickUpCoordinate?.latitude == nil{
                return
            }
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            //Show near by places first
            let bounds: GMSCoordinateBounds = getCoordinateBounds(latitude: (pickUpCoordinate?.latitude)!, longitude: (pickUpCoordinate?.longitude)!)
            //acController.autocompleteBounds = GMSCoordinateBounds(coordinate: bounds.northEast, coordinate: bounds.southWest)
            //acController.autocompleteFilter?.locationBias = GMSPlaceLocationBias(bounds.northEast, bounds.southWest)
            present(acController, animated: true, completion: nil)
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    /* Returns Bounds */
    func getCoordinateBounds(latitude:CLLocationDegrees ,
                             longitude:CLLocationDegrees,
                             distance:Double = 0.01)->GMSCoordinateBounds{
        let center = CLLocationCoordinate2D(latitude: latitude,
                                            longitude: longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + distance, longitude: center.longitude + distance)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - distance, longitude: center.longitude - distance)
        
        return GMSCoordinateBounds(coordinate: northEast,
                                   coordinate: southWest)
        
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "route") {
            let vc = segue.destination as! RouteScreenController
            vc.pickUpCoordinate = self.pickUpCoordinate
            vc.dropCoordinate = self.dropCoordinate
            vc.pickUpAddress = self.pickUpLocationTf.text
            vc.dropAddress = self.dropLocationTf.text
            vc.vehicleType = self.vehicleType
            vc.vehicleTypeId = self.vehicleTypeId
            vc.cityId = self.cityId
            vc.currentCity = self.currentCity
//            vc.fareInfoArray = self.fareInfoArray
            vc.isForBookLater = isForBookLater
            vc.isForBookShareRide = isForBookShareRide
            vc.pickupTime = pickupTime
            vc.waitingTime = waitingTime
        }else if (segue.identifier == "driverToReachPickupPoint") {
            let vc = segue.destination as! DriverToReachPickUpPointViewController
            vc.pickUpCoordinate = self.pickUpCoordinate
            vc.dropCoordinate = self.dropCoordinate
            vc.pickUpAddress = self.pickUpLocationTf.text
            vc.dropAddress = self.dropLocationTf.text
            vc.bookingStatusBo = self.driverProfileBo
        }else if (segue.identifier == "rental_booking_rides") {
            let vc = segue.destination as! RentalBookingRidesViewController
            vc.pickUpLocation = self.pickUpLocationTf.text!
            vc.isForBooklater = self.isForBookLater
            vc.pickupTime = pickupTime
            vc.pickUpCoordinate = self.pickUpCoordinate
            vc.cityId = self.cityId
            vc.currentCity = self.currentCity
        }else if (segue.identifier == "previous_booked_locations") {
            let vc = segue.destination as! PreviousBookedLocationsViewController
            vc.delegate = self
        }else if segue.identifier == "outStationBooking" {
            let vc = segue.destination as! OutstationBookingViewController
            vc.pickUpLocation = self.pickUpLocationTf.text!
            vc.pickUpCoordinate = self.pickUpCoordinate
            vc.pickupTime = pickupTime
            vc.currentCity = self.currentCity
            vc.cityId = self.cityId
        }
    }
    
    // MARK: - Animation Method
    func animateVehiceLocation(oldCoodinate: CLLocationCoordinate2D , newCoodinate: CLLocationCoordinate2D, image: UIImage, pin : GMSMarker, bearing: Int) {
        index = index + 1
        print("Index ===\(index)")
        print("Driver location New ===\(newCoodinate)")
        print("Driver location Old ===\(oldCoodinate)")
        
        pin.icon = image
        pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        pin.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
        //found bearing value by calculation when marker add
        pin.position = oldCoodinate
        //this can be old position to make car movement to new position
        pin.map = self.googleMapView
        //marker movement animation
        CATransaction.begin()
        CATransaction.setValue(Int(3.0), forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
            //pin.rotation = CDouble(bearing)
            self.rotatewithAnimation(pin: pin, bearing: bearing)
            
            //New bearing value from backend after car movement is done
            //driverPin.map = nil
        })
        pin.position = newCoodinate
        //this can be new position after car moved from old position to new position with animation
        pin.map = self.googleMapView
        pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        pin.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
        //found bearing value by calculation
        CATransaction.commit()
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    
    func rotatewithAnimation(pin: GMSMarker, bearing: Int){
        CATransaction.begin()
        CATransaction.setValue(Int(2.0), forKey: kCATransactionAnimationDuration)
        pin.rotation = CDouble(bearing)
        CATransaction.commit()
    }
    
}

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension CALayer {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        mask = shape
    }
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer();
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.frame.width - thickness, y: 0, width: thickness, height:self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}

extension UIView {
    func addshadow(top: Bool,
                   left: Bool,
                   bottom: Bool,
                   right: Bool,
                   shadowRadius: CGFloat = 2.0) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1.0
        
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        // selecting top most point
        path.move(to: CGPoint(x: x, y: y))
        // Move to the Bottom Left Corner, this will cover left edges
        /*
         |☐
         */
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        // Move to the Bottom Right Corner, this will cover bottom edge
        /*
         ☐
         -
         */
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        // Move to the Top Right Corner, this will cover right edge
        /*
         ☐|
         */
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        // Move back to the initial point, this will cover the top edge
        /*
         _
         ☐
         */
        path.close()
        self.layer.shadowPath = path.cgPath
    }
}

extension UIView {
    
    func setShadowWithCornerRadius(corners : CGFloat){
        
        self.layer.cornerRadius = corners
        
        let shadowPath2 = UIBezierPath(rect: self.bounds)
        
        self.layer.masksToBounds = false
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        
        self.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
        
        self.layer.shadowOpacity = 0.5
        
        self.layer.shadowPath = shadowPath2.cgPath
        
    }
    
}

extension UIView{
    func animShow(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
}

extension Date {
    
    /// Returns a Date with the specified amount of components added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date? {
        let components = DateComponents(year: years, month: months, day: days, hour: hours, minute: minutes, second: seconds)
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    /// Returns a Date with the specified amount of components subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date? {
        return add(years: -years, months: -months, days: -days, hours: -hours, minutes: -minutes, seconds: -seconds)
    }
    
}
extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.x, y: -origin.y,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return rotatedImage ?? self
        }
        
        return self
    }
}
extension UIButton {
    func setTitleWithOutAnimation(title: String?) {
        UIView.setAnimationsEnabled(false)
        
        setTitle(title, for: .normal)
        
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}



