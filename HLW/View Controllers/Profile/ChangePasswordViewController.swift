//
//  ChangePasswordViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 1/18/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var oldPasswordTf: UITextField!
    @IBOutlet weak var newPasswordTf: UITextField!
    @IBOutlet weak var confirmPasswordTf: UITextField!

    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
            btnHeightConstraint.constant = 60
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "ChangePasswordPage", screenClass: "ChangePasswordViewController")
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdatePasswordAction(_ sender: Any) {
        let errTitleMessage : String = "Error"
        var errMessage : String?
        let oldPassword = self.oldPasswordTf?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let newPassword = self.newPasswordTf?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let confirmPassword = self.confirmPasswordTf?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(oldPassword == ""){
            errMessage = StringConstant.old_password_blank_validation_msg
        }else if(newPassword == ""){
            errMessage = StringConstant.password_blank_validation_TitleMsg
        }else if((newPassword?.count)! < 8){
            errMessage = StringConstant.password_validation_msg
        }else if(!AppConstant.validatePassword(phrase: newPassword!)){
            errMessage = StringConstant.password_validation_msg
        }else if(confirmPassword == ""){
            errMessage = StringConstant.cnf_password_blank_validation_msg
        }else if(newPassword != confirmPassword){
            errMessage = StringConstant.password_mismatch_validation_msg
        }
        
        if(errMessage != nil){
            self.showAlert(alertTitle: errTitleMessage, alertMessage: errMessage!)
        }else {
            serviceCallToUpdatePassword(oldPassword: oldPassword!, newPassword: newPassword!)
        }
    }
    
    // MARK: - Api Service Call Method
    func serviceCallToUpdatePassword(oldPassword: String, newPassword: String){
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "old_password": oldPassword,
                "new_password": newPassword
            ]
            
            print("url===\(AppConstant.changePasswordUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.changePasswordUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    
                    switch(response.result) {
                    case let .success(value):
                        
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//success
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                    _ = self.navigationController?.popViewController(animated: true)
                                }else{//Failure
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }

}
