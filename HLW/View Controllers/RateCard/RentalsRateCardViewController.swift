//
//  RentalsRateCardViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 3/15/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class RentalsRateCardViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate , UITableViewDataSource, selectionDelegate {
    
    @IBOutlet weak var rentalsTableView: UITableView!
    
    var itemInfo: IndicatorInfo = "RENTAL"
    var cells = SwiftyAccordionCells()
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?
    var arrRentalBookingCabTypes = [RentalBookingCabTypes]()
    var vehicleInfo = VehicleTypeBO()
    var arrCityList = [CustomObject]()
    var selectedCityBo = CustomObject()
    var selectedTripBo = CustomObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDesign()
    }
    
    func initDesign(){
        self.rentalsTableView.tableFooterView = UIView()
        self.rentalsTableView.rowHeight = UITableView.automaticDimension
        self.rentalsTableView.allowsMultipleSelection = true
        
        self.serviceCallToGetSelectTripOptions(isShowPupup: false)
        
        selectedCityBo.name = AppConstant.cityName
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "RentalsRateCardPage", screenClass: "RentalsRateCardViewController")
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    //MARK: Tableview Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 1
        }
        else if (section == 1) {
            return arrRentalBookingCabTypes.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RentalDetailsRateCardTableViewCell", for: indexPath as IndexPath) as! RentalDetailsRateCardTableViewCell
            
            cell.selectionStyle = .none
            
            cell.lblSelection.text = selectedTripBo.name
            
            cell.lblCityName.text = selectedCityBo.name
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.selectTripAction(_:)))
            cell.viewTripSelection.isUserInteractionEnabled = true
            cell.viewTripSelection.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.selectCityAction(_:)))
            cell.viewCitySelection.isUserInteractionEnabled = true
            cell.viewCitySelection.addGestureRecognizer(tap2)
            
            return cell
        }
        else if (indexPath.section == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RentalCabFareDetailsTableViewCell", for: indexPath as IndexPath) as! RentalCabFareDetailsTableViewCell
            cell.selectionStyle = .none
            
            let cabBo = self.arrRentalBookingCabTypes[indexPath.row]
            
            if AppConstant.screenSize.height <= 568{//Manage for iPhone 5 and Below
                cell.lblBaseFareTitle.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblAddKmFareTitle.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblAddTimeFareTitle.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblMinFareTitle.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblBaseFare.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblAddKmFare.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblAddTimeFare.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                cell.lblMinFare.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            }
            
            cell.lblBaseFare.text = "₹ \(cabBo.baseFare)"
            cell.lblAddKmFare.text = "₹ \(cabBo.addKmFare)"
            cell.lblAddTimeFare.text = "₹ \(cabBo.addTimeFare)"
            cell.lblMinFare.text = "₹ \(cabBo.minFare)"
            cell.lblCabName.text = cabBo.cabName
            
            cell.viewContainerHeightConstraint.constant = cabBo.isShowing ? 188.0 : 0
            cell.viewContainer.isHidden = cabBo.isShowing ? false : true
            cell.viewCab.tag = indexPath.row
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectCabFare(_:)))
            cell.viewCab.isUserInteractionEnabled = true
            cell.viewCab.addGestureRecognizer(tap)
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RentalExtraChargesRateCardTableViewCell", for: indexPath as IndexPath) as! RentalExtraChargesRateCardTableViewCell
            
            cell.lblTitle1.text = vehicleInfo.title1
            cell.lblInfo1.text = vehicleInfo.info1
            cell.lblTitle2.text = vehicleInfo.title2
            cell.lblInfo2.text = vehicleInfo.info2
            cell.lblTitle3.text = vehicleInfo.title3
            cell.lblInfo3.text = vehicleInfo.info3
            
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: Selection Delegate
    func selectedOption(customBo: CustomObject, type: String) {
        if type == "City"{
            selectedCityBo.name = customBo.name
            selectedCityBo.id = customBo.id
        }else if type == "Category"{
            selectedTripBo.name = customBo.name
            self.serviceCallToGetRentalTarif(rentalId: customBo.id)
        }
        self.rentalsTableView.reloadData()
    }
    
    //MARK: Button Action
    @objc func selectTripAction(_ sender: UITapGestureRecognizer) {
        serviceCallToGetSelectTripOptions(isShowPupup: true)
    }
    
    @objc func selectCityAction(_ sender: UITapGestureRecognizer){
        serviceCallToGetSelectCityOptions()
    }
    
    @objc func selectCabFare(_ sender: UITapGestureRecognizer) {
        let cabBo = self.arrRentalBookingCabTypes[(sender.view?.tag)!]
        cabBo.isShowing = !cabBo.isShowing
        
        self.rentalsTableView.reloadData()
        
    }
    
    //MARK: Segue Method
    func showSelection(arrCustomData: [CustomObject], type: String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MakeYourSelectionViewController") as! MakeYourSelectionViewController
        vc.arrSelection = arrCustomData
        vc.type = type
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Service Call
    func serviceCallToGetSelectCityOptions() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                      "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("url===\(AppConstant.cityListUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.cityListUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Rate Card : \(response)")
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if status == 1 {//Success
                                    self.arrCityList.removeAll()
                                    if let dictData = dict["data"] as? [[String: Any]] {
                                        for dict in dictData{
                                            let cityListBo = CustomObject()
                                            if let city_Name = dict["city_name"] as? String {
                                                cityListBo.name = city_Name
                                            }
                                            if let city_Id = dict["city_id"] as? String {
                                                cityListBo.id = city_Id
                                            }
                                            self.arrCityList.append(cityListBo)
                                        }
                                        for city in self.arrCityList{
                                            if AppConstant.cityName == city.name{
                                                self.selectedCityBo.name = city.name
                                                self.selectedCityBo.id = city.id
                                            }
                                        }
                                        if self.selectedCityBo.name == ""{
                                            self.selectedCityBo = self.arrCityList[0]
                                        }
                                    }
                                    self.showSelection(arrCustomData: self.arrCityList, type: "City")
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetSelectTripOptions(isShowPupup: Bool) {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "city_name" : AppConstant.cityName,
                "ride_type" : "1"
            ]
            
            print("url===\(AppConstant.getRateCardUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getRateCardUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Rate Card : \(response)")
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//Success
                                    self.arrCityList.removeAll()
                                    if let dictRateCard = dict["rateCard"] as? [String: Any] {
                                        if let arrCategory = dictRateCard["category"] as? [[String: Any]] {
                                            for dict in arrCategory{
                                                let customBo = CustomObject()
                                                if let rental_id = dict["rental_id"] as? String {
                                                    customBo.id = rental_id
                                                }
                                                if let rental_type = dict["rental_type"] as? String {
                                                    customBo.name = rental_type
                                                }
                                                self.arrCityList.append(customBo)
                                            }
                                        }
                                        
                                        if let dictFare = dictRateCard["fare"] as? [String: Any]{
                                            if let title1 = dictFare["title1"] as? String{
                                                self.vehicleInfo.title1 = title1
                                            }
                                            if let info1 = dictFare["info1"] as? String{
                                                self.vehicleInfo.info1 = info1
                                            }
                                            if let title2 = dictFare["title2"] as? String{
                                                self.vehicleInfo.title2 = title2
                                            }
                                            if let info2 = dictFare["info2"] as? String{
                                                self.vehicleInfo.info2 = info2
                                            }
                                            if let title3 = dictFare["title3"] as? String{
                                                self.vehicleInfo.title3 = title3
                                            }
                                            if let info3 = dictFare["info3"] as? String{
                                                self.vehicleInfo.info3 = info3
                                            }
                                        }
                                    }
                                    if isShowPupup{
                                        self.showSelection(arrCustomData: self.arrCityList, type: "Category")
                                    }else{
                                        if self.arrCityList.count > 0 {
                                            self.selectedTripBo = self.arrCityList[0]
                                            self.serviceCallToGetRentalTarif(rentalId: self.selectedTripBo.id)
                                        }
                                    }
                                    self.rentalsTableView.reloadData()
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetRentalTarif(rentalId : String) {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "city_name" : AppConstant.cityName,
                "ride_type" : 1,
                "rental_id" : rentalId
            ]
            
            print("url===\(AppConstant.getRentalTarifUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getRentalTarifUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Tarif : \(response)")
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//Success
                                    self.arrRentalBookingCabTypes.removeAll()
                                    if let dictRateCard = dict["rateCard"] as? [String: Any] {
                                        if let arrCategory = dictRateCard["category"] as? [[String: Any]] {
                                            for dict in arrCategory{
                                                let cabBo = RentalBookingCabTypes()
                                                if let cat_id = dict["cat_id"] as? String {
                                                    cabBo.cabId = cat_id
                                                }
                                                if let category_name = dict["category_name"] as? String {
                                                    cabBo.cabName = category_name
                                                }
                                                if let cabBlue = dict["image_blue"] as? String {
                                                    cabBo.cabBlue = cabBlue
                                                }
                                                if let cabWhite = dict["image_white"] as? String {
                                                    cabBo.cabWhite = cabWhite
                                                }
                                                if let add_km_fare = dict["add_km_fare"] as? String {
                                                    cabBo.addKmFare = add_km_fare
                                                }
                                                if let add_time_fare = dict["add_time_fare"] as? String {
                                                    cabBo.addTimeFare = add_time_fare
                                                }
                                                if let base_fare = dict["base_fare"] as? String {
                                                    cabBo.baseFare = base_fare
                                                }
                                                if let min_fare = dict["min_fare"] as? Int {
                                                    cabBo.minFare = String(min_fare)
                                                }
                                                
                                                self.arrRentalBookingCabTypes.append(cabBo)
                                            }
                                        }
                                    }
                                    
                                    if self.arrRentalBookingCabTypes.count > 0{
                                        let cabBo = self.arrRentalBookingCabTypes[0]
                                        cabBo.isShowing = true
                                    }
                                    
                                    self.rentalsTableView.reloadData()
                                    
                                }
                                else if (status == 3){//Logout from the app
                                    AppConstant.logout()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    
}
