//
//  RateCardViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 3/15/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class RateCardViewController: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesigns()
    }
    
    func initDesigns() {
        
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
        }
        
        // change selected bar color for XLPagerTabStrip
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = AppConstant.colorThemeYellow
        settings.style.buttonBarItemFont = UIFont.boldSystemFont(ofSize: 16.0)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarItemLeftRightMargin = 5
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.gray
            newCell?.label.textColor = UIColor.black
        }
        
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "RateCardPage", screenClass: "RateCardViewController")
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OneWayRateCardViewController")
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RentalsRateCardViewController")
        let child_3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OutstationRateCardViewController")
        return [child_1, child_2, child_3]
    }
    
    //MARK: Button Action
    @IBAction func menuBtnAction (_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    

}
