//
//  OutstationRateCardViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 23/09/22.
//  Copyright © 2022 ODITEK. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import GooglePlacesSearchController
import GooglePlaces

class OutstationRateCardViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate , UITableViewDataSource, selectionDelegate, GMSAutocompleteViewControllerDelegate, ChooseDateTimeDelegate {
    
    @IBOutlet weak var tblViewOutstation: UITableView!
    
    var itemInfo: IndicatorInfo = "OUTSTATION"
    var arrCityList = [CustomObject]()
    var selectedCityBo = CustomObject()
    var selectedCabBo = CustomObject()
    var arrCabTypes = [CustomObject]()
    var arrFareDetails = [FareDetails]()
    
    var dropCityFullName: String = ""
    var dropCityName: String = ""
    var dropCoordinate: CLLocationCoordinate2D? = nil
    
    var estimatedKm: String = ""
    var estimatedKmVal: Int = 0
    var tripType: Int = 0
    
    var strStartDateTime: String = ""
    var strEndDateTime: String = ""
    var startDateTime = Date()
    var endDateTime = Date()
    
    var isShowBtnContinue = true
    var isSelectedDateTime = false
    var isOnewaySelected = Bool()
    var isStartDateSelected = false
    var isCabSelected = false
    var isShowFareDetails = false
    
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        tblViewOutstation.tableFooterView = UIView()
        
        selectedCityBo.name = AppConstant.cityName
        
        serviceCallToGetSelectCityOptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppConstant.setTrackScreenName(screenName: "OutstationRateCardPage", screenClass: "OutstationRateCardViewController")
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    //MARK: Tableview Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if (isSelectedDateTime == false && isCabSelected == false && isShowFareDetails == false) && isShowBtnContinue == true{
            return 3
        }else if self.isOnewaySelected == false && self.strEndDateTime == ""{
            return 5
        }else{
            return 6
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 1
        }else if section == 2 {
            return 1
        }else if section == 3 {
            return 1
        }else if section == 4 {
            return 1
        }else{
            return arrFareDetails.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutstationRateCard", for: indexPath as IndexPath) as! OutstationRateCardTableViewCell
            cell.selectionStyle = .none
            
            cell.lblCityName.text = selectedCityBo.name
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectCityAction(_:)))
            cell.viewCitySelection.isUserInteractionEnabled = true
            cell.viewCitySelection.addGestureRecognizer(tap)
            
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectDropCity", for: indexPath as IndexPath) as! SelectDropCityTableViewCell
            cell.selectionStyle = .none
            
            if self.dropCityFullName == ""{
                cell.lblDropLocName.textColor = UIColor.darkGray
                cell.lblDropLocName.text = "Select City"
            }else{
                cell.lblDropLocName.textColor = UIColor.black
                cell.lblDropLocName.text = self.dropCityFullName
            }
            
            var steEstKm = ""
            if self.tripType != 1{//Round Trip
                steEstKm = self.estimatedKm.replacingOccurrences(of: "kms", with: "")
                steEstKm = String(Int(steEstKm.trim() == "" ? "0": steEstKm.trim())! * 2) + " kms"
            }else{
                steEstKm = self.estimatedKm
            }
            
            let attributedText = NSMutableAttributedString(string: "Estimated Kilometer:   ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .semibold)])
            let attributedTxt = NSMutableAttributedString(string: steEstKm, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)])
            attributedText.append(attributedTxt)
            
            cell.lblEstimatedKm.attributedText = attributedText
            
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnContinue", for: indexPath as IndexPath) as! ContinueBtnTableViewCell
            cell.selectionStyle = .none
            
            cell.btnContinue.tag = indexPath.row
            cell.btnContinue.addTarget(self, action: #selector(btnContinueAction(_:)), for: .touchUpInside)
            
            if isShowBtnContinue == false{
                cell.btnContinue.isHidden = true
                cell.btnContinueHeightConstraint.constant = 0
            }else{
                cell.btnContinue.isHidden = false
                cell.btnContinueHeightConstraint.constant = 40
            }
            
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCab", for: indexPath as IndexPath) as! SelectCabTableViewCell
            cell.selectionStyle = .none
            
            cell.lblVehiclaName.text = selectedCabBo.name
            cell.imgViewVehicle.sd_setImage(with: URL(string: selectedCabBo.vehImg_White), placeholderImage: UIImage(named: ""))
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectCabAction(_:)))
            cell.viewCabSelection.isUserInteractionEnabled = true
            cell.viewCabSelection.addGestureRecognizer(tap)
            
            return cell
        }else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectDateTime", for: indexPath as IndexPath) as! SelectDateTimeTableViewCell
            cell.selectionStyle = .none
            
            cell.btnOneway.tag = indexPath.row
            cell.btnOneway.addTarget(self, action: #selector(radioBtnOnewayAction(_:)), for: .touchUpInside)
            cell.btnRoundTrip.tag = indexPath.row
            cell.btnRoundTrip.addTarget(self, action: #selector(radioBtnRoundTripAction(_:)), for: .touchUpInside)
            
            cell.btnOneway.isEnabled = self.tripType == 0 ? false : true
            cell.lblOneway.textColor = self.tripType == 0 ? UIColor.gray : UIColor.black
            cell.lblOnewayDisableMsg.isHidden = self.tripType == 0 ? false : true
            cell.lblDisableMsgHeightConstraint.constant = self.tripType == 0 ? 30 : 0
            
            if isOnewaySelected == true{
                cell.radioBtnOneway.isSelected = true
                cell.radioBtnRoundTrip.isSelected = false
                cell.viewEndDateTime.isHidden = true
                cell.viewEndDateTimeHeightConstraint.constant = 0
                cell.viewContentHeightConstraint.constant = self.tripType == 0 ? 140 : 110
            }else{
                cell.radioBtnOneway.isSelected = false
                cell.radioBtnRoundTrip.isSelected = true
                cell.viewEndDateTime.isHidden = false
                cell.viewEndDateTimeHeightConstraint.constant = 35
                cell.viewContentHeightConstraint.constant = self.tripType == 0 ? 180 : 145
            }
            
            cell.btnSelectStartDateTime.tag = 0
            cell.btnSelectStartDateTime.addTarget(self, action: #selector(btnSelectDateTimeAction(_:)), for: .touchUpInside)
            cell.btnSelectEndDateTime.tag = 1
            cell.btnSelectEndDateTime.addTarget(self, action: #selector(btnSelectDateTimeAction(_:)), for: .touchUpInside)
            
            cell.lblStartDateTime.text = self.strStartDateTime
            if self.strEndDateTime != ""{
                isShowFareDetails = true
                cell.lblEndDateTime.text = self.strEndDateTime
                cell.btnSelectEndDateTime.setTitle("", for: .normal)
            }else{
                isShowFareDetails = false
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FareDetails", for: indexPath as IndexPath) as! OutstationFareDetailsTableViewCell
            cell.selectionStyle = .none
            
            let fareDetailsBo = arrFareDetails[indexPath.row]
            cell.lblFareTitle?.text = fareDetailsBo.title
            cell.lblFare?.text = fareDetailsBo.type == 0 ? "\(fareDetailsBo.price)" : "₹ \(fareDetailsBo.price)"
            
            return cell
        }
    }
    
    //MARK: Button Actions
    @objc func selectCityAction(_ sender: UITapGestureRecognizer){
        self.showSelection(arrCustomData: self.arrCityList, type: "City")
    }
    
    @objc func selectCabAction(_ sender: UITapGestureRecognizer) {
        //Show DropDown
        self.showSelection(arrCustomData: self.arrCabTypes, type: "Category")
    }
    
    @IBAction func btnSelectDropCityAction(_ sender: Any) {
        isShowBtnContinue = true
        isSelectedDateTime = false
        isCabSelected = false
        isShowFareDetails = false
        self.dropCityFullName = ""
        self.estimatedKm = "0 kms"
        tblViewOutstation.reloadData()
        
        if isShowBtnContinue == true{
            presentGSMAutocompleteViewController()
        }
    }
    
    @objc func btnContinueAction(_ sender: UIButton) {
        if self.dropCityFullName == ""{
            self.showAlert(alertTitle: "Select drop city to continue", alertMessage: "")
        }else{
            self.startDateTime = Date().add(hours: 1)!
            print("StartDate ==== \(self.startDateTime)")
            self.strStartDateTime = AppConstant.formattedDate(date: self.startDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter7)!
            
            //service call to get Vehicle Category
            serviceCallToGetVehicleCategory()
            
            isShowBtnContinue = false
            isSelectedDateTime = true
            isCabSelected = true
            isShowFareDetails = true
            tblViewOutstation.reloadData()
        }
        
    }
    
    @objc func radioBtnOnewayAction(_ sender: UIButton){
        isOnewaySelected = true
        if self.tripType != 0{
            self.tripType = 1
        }
        tblViewOutstation.reloadData()
        serviceCallToGetFareDetails()
    }
    
    @objc func radioBtnRoundTripAction(_ sender: UIButton){
        isOnewaySelected = false
        if self.tripType != 0{
            self.tripType = 2
        }
        tblViewOutstation.reloadData()
        if isOnewaySelected == false && self.strEndDateTime != ""{
            serviceCallToGetFareDetails()
        }else{
            isShowFareDetails = false
            self.showAlert(alertTitle: "Please select end date & time", alertMessage: "")
        }
        
    }
    
    @objc func btnSelectDateTimeAction(_ sender: UIButton){
        if sender.tag == 0{
            self.isStartDateSelected = true
        }else{
            self.isStartDateSelected = false
        }
        
        self.performSegue(withIdentifier: "datePicker", sender: self)
    }
    
    
    //MARK: Choose DateTime Protocol Delegates
    func selectedDateTime(totalTripTime: Date) {
        if isStartDateSelected == true{
            self.startDateTime = totalTripTime
            self.strStartDateTime = AppConstant.formattedDate(date: totalTripTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter7)!
        }else{
            self.endDateTime = totalTripTime
            self.strEndDateTime = AppConstant.formattedDate(date: totalTripTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter7)!
        }
        serviceCallToGetFareDetails()
        tblViewOutstation.reloadData()
    }
    
    //MARK: Selection Delegate
    func selectedOption(customBo: CustomObject, type: String){
        if type == "City"{
            selectedCityBo.name = customBo.name
            selectedCityBo.id = customBo.id
        }else if type == "Category"{
            selectedCabBo = CustomObject()
            selectedCabBo.name = customBo.name
            selectedCabBo.id = customBo.id
            selectedCabBo.vehImg_White = customBo.vehImg_White
        }
        self.tblViewOutstation.reloadData()
    }
    
    func showSelection(arrCustomData: [CustomObject], type: String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MakeYourSelectionViewController") as! MakeYourSelectionViewController
        vc.arrSelection = arrCustomData
        vc.type = type
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Service Call Method
    func serviceCallToGetSelectCityOptions() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                      "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("url===\(AppConstant.cityListUrl)")
            print("params===\(params!)")
            
            AF.request(AppConstant.cityListUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Rate Card : \(response)")
                    switch(response.result) {
                    case let .success(value):
                        
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if status == 1 {//Success
                                    self.arrCityList.removeAll()
                                    if let dictData = dict["data"] as? [[String: Any]] {
                                        for dict in dictData{
                                            let cityListBo = CustomObject()
                                            if let city_Name = dict["city_name"] as? String {
                                                cityListBo.name = city_Name
                                            }
                                            if let city_Id = dict["city_id"] as? String {
                                                cityListBo.id = city_Id
                                            }
                                            if let city_lat = dict["city_lat"] as? String {
                                                cityListBo.lat = city_lat
                                            }
                                            if let city_lng = dict["city_lng"] as? String {
                                                cityListBo.lng = city_lng
                                            }
                                            self.arrCityList.append(cityListBo)
                                        }
                                        for city in self.arrCityList{
                                            if AppConstant.cityName == city.name{
                                                self.selectedCityBo.name = city.name
                                                self.selectedCityBo.id = city.id
                                                self.pickUpCoordinate = CLLocationCoordinate2D(latitude: Double.init(city.lat) ?? 0.0, longitude: Double.init(city.lng) ?? 0.0)
                                            }
                                        }
                                        if self.selectedCityBo.name == ""{
                                            self.selectedCityBo = self.arrCityList[0]
                                        }
                                    }
                                    
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    func serviceCallToGetVehicleCategory() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            let params: Parameters = [
                "user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "latitude" : NSNumber(value: (pickUpCoordinate?.latitude)!),
                "longitude" : NSNumber(value: (pickUpCoordinate?.longitude)!),
                "ride_id": "2"
            ]
            
            print("url===\(AppConstant.getOutstationVehicleCategoryUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.getOutstationVehicleCategoryUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON{ response in
                    AppConstant.hideHUD()
                    //print("url===\(AppConstant.getOutstationVehicleCategoryUrl)")
                    debugPrint(response)
                    
                    switch(response.result) {
                        case let .success(value):
                            if let dict = value as? [String: Any]{
                                if let status = dict["status"] as? Int {
                                    if status == 1{//Success
                                        self.arrCabTypes.removeAll()
                                        if let arrVehicleCategoryInfo = dict["data"] as? [[String: Any]] {
                                            if (arrVehicleCategoryInfo.count > 0) {
                                                for dictInfo in arrVehicleCategoryInfo {
                                                    let customBo = CustomObject()
                                                    
                                                    if let cabId = dictInfo["cat_id"] as? String {
                                                        customBo.id = cabId
                                                    }
                                                    if let cabName = dictInfo["cat_name"] as? String {
                                                        customBo.name = cabName
                                                    }
                                                    if let cabImg_white = dictInfo["image_white"] as? String {
                                                        customBo.vehImg_White = cabImg_white
                                                    }
                                                    self.arrCabTypes.append(customBo)
                                                }
                                                
                                                if self.arrCabTypes.count > 0 {
                                                    self.selectedCabBo = self.arrCabTypes[0]
                                                }
                                                
                                                if self.tripType == 1{
                                                    if self.arrCabTypes.count > 0 && self.strStartDateTime != "" {
                                                        self.serviceCallToGetFareDetails()
                                                    }
                                                }else{
                                                    if self.arrCabTypes.count > 0 && (self.strStartDateTime != "" && self.strEndDateTime != ""){
                                                        self.serviceCallToGetFareDetails()
                                                    }else if self.isOnewaySelected == false && self.strEndDateTime == ""{
                                                        self.isShowFareDetails = false
                                                        self.showAlert(alertTitle: "Please select end date & time", alertMessage: "")
                                                    }
                                                }
                                                self.tblViewOutstation.reloadData()
                                            }
                                        }
                                        AppConstant.hideHUD()
                                    }else if (status == 3){
                                        AppConstant.hideHUD()
                                    }else{//Logout from the app
                                        AppConstant.hideHUD()
                                    }
                                }
                            }
                            break
                        case let .failure(error):
                            self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                            break
                    }
            }
            
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    
    func serviceCallToGetEstimatedCityDistance() {
        if AppConstant.hasConnectivity() {
           // AppConstant.showHUD()

            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "src_city" : selectedCityBo.name,
                "dst_city": self.dropCityName
            ]

            print("url===\(AppConstant.getCityDistanceUrl)")
            print("params===\(params!)")

            AF.request( AppConstant.getCityDistanceUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if status == "0"{
                                    if let errMsg = dict["error"] as? String{
                                        self.showAlert(alertTitle: errMsg, alertMessage: "")
                                    }
                                }else  if status == "1"{
                                    if let km = dict["km"] as? String {
                                        self.estimatedKm = km
                                    }
                                    if let kmVal = dict["km_val"] as? Int {
                                        self.estimatedKmVal = kmVal
                                    }
                                    if let oneWay = dict["one_way"] as? Int {
                                        self.tripType = oneWay
                                    }
                                    self.isOnewaySelected = self.tripType == 1 ? true : false
                                }
                            }
                            self.tblViewOutstation.reloadData()
                        }

                        break

                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break

                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }

    }
    
    func serviceCallToGetFareDetails() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            let pickLat : NSNumber = NSNumber(value: (pickUpCoordinate?.latitude)!)
            let pickLng : NSNumber = NSNumber(value: (pickUpCoordinate?.longitude)!)
            let dropLat : NSNumber = NSNumber(value: (dropCoordinate?.latitude)!)
            let dropLng : NSNumber = NSNumber(value: (dropCoordinate?.longitude)!)
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "ride_id" : "2",
                "trip_type": tripType == 1 ? 1 : 2,
                "start_date": AppConstant.formattedDate(date: self.startDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter2)!,
                "start_time": AppConstant.formattedDate(date: self.startDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter6)!,
                "end_date": tripType == 1 ? "" : AppConstant.formattedDate(date: self.endDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter2)!,
                "end_time": tripType == 1 ? "" : AppConstant.formattedDate(date: self.endDateTime, withFormat: StringConstant.dateFormatter1, ToFormat: StringConstant.dateFormatter6)!,
                "dest_city": self.dropCityName,
                "cat_id" : selectedCabBo.id,
                "city" : AppConstant.cityName,
                "src_lat" : String(describing: pickLat),
                "src_lon" : String(describing: pickLng),
                "des_lat" : String(describing: dropLat),
                "des_lon" : String(describing: dropLng),
            ]
            
            
            print("url===\(AppConstant.getEstimatedCostUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getEstimatedCostUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            debugPrint(dict)
                            self.arrFareDetails.removeAll()
                            if let status = dict["status"] as? String {
                                if(status == "0"){
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                }else  if(status == "1"){//Success
                                    if let fareInfoArray = dict["info_arry"] as? [[String: Any]]{
                                        debugPrint(fareInfoArray)
                                        for item in fareInfoArray {
                                            let fareDetailsBo = FareDetails()
                                            
                                            if let titleKey = item["key"] as? String {
                                                fareDetailsBo.title = titleKey.replacingOccurrences(of: "&#x20b9;", with: "₹ ")
                                            }
                                            if let value = item["value"] as? Int {
                                                fareDetailsBo.price = String(value)
                                            }
                                            if let value = item["value"] as? String {
                                                fareDetailsBo.price = value
                                            }
                                            if let type = item["type"] as? Int{
                                                fareDetailsBo.type = type
                                            }
                                            
                                            self.arrFareDetails.append(fareDetailsBo)
                                        }
                                    }
                                    self.isShowFareDetails = true
                                    self.tblViewOutstation.reloadData()
                                }else  if(status == "2"){
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.goToLandingScreen()
                                }
                            }
                            
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    // MARK: AutocompleteViewController Delegate
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.dropCityFullName = String(format: "%@", place.formattedAddress!)
        self.dropCityName = String(format: "%@", place.name!)
        self.dropCoordinate = place.coordinate
        
        dismiss(animated: true, completion: nil)
        
        //Service call to get CityDistance
        DispatchQueue.main.async {
            self.serviceCallToGetEstimatedCityDistance()
        }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {// TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Present GSMAutocompleteViewController Method
    func presentGSMAutocompleteViewController() {
        if AppConstant.hasConnectivity(){
            if pickUpCoordinate?.latitude == nil{
                return
            }
            
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = GMSPlacesAutocompleteTypeFilter.city
            acController.autocompleteFilter = filter
            
            let bounds: GMSCoordinateBounds = getCoordinateBounds(latitude: (pickUpCoordinate?.latitude)!, longitude: (pickUpCoordinate?.longitude)!)
//            acController.autocompleteBounds = GMSCoordinateBounds(coordinate: bounds.northEast, coordinate: bounds.southWest)
            present(acController, animated: true, completion: nil)
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    /* Returns Bounds */
    func getCoordinateBounds(latitude:CLLocationDegrees ,
                             longitude:CLLocationDegrees,
                             distance:Double = 0.01)->GMSCoordinateBounds{
        let center = CLLocationCoordinate2D(latitude: latitude,
                                            longitude: longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + distance, longitude: center.longitude + distance)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - distance, longitude: center.longitude - distance)
        
        return GMSCoordinateBounds(coordinate: northEast,
                                   coordinate: southWest)
        
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.view.endEditing(true)
        if segue.identifier == "datePicker"{
            let vc = segue.destination as! DatePickerViewController
            vc.delegate = self
            if self.isStartDateSelected == true {
                vc.selectedDate = self.startDateTime
            }else{
                vc.selectedDate = self.endDateTime
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
