//
//  OneWayRateCardViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 3/15/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import iOSDropDown
import Alamofire

class OneWayRateCardViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate , UITableViewDataSource, selectionDelegate {
    
    @IBOutlet weak var oneWayTableView: UITableView!
    
    var isFareDetailsShowing = true
    
    var itemInfo: IndicatorInfo = "ONE WAY"
    var vehicleInfo = VehicleTypeBO()
    var arrCityList = [CustomObject]()
    var selectedCityBo = CustomObject()
    var selectedCabBo = CustomObject()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        serviceCallToGetSelectTripOptions(isShowPupup: false)
        selectedCityBo.name = AppConstant.cityName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "OneWayRateCardPage", screenClass: "OneWayRateCardViewController")
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    //MARK: Tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OneWayRateCardTableViewCell", for: indexPath as IndexPath) as! OneWayRateCardTableViewCell
        
        cell.selectionStyle = .none
        
        let attributedString1 = NSMutableAttributedString(string: "\(vehicleInfo.title1) \n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .medium), NSAttributedString.Key.foregroundColor : UIColor.black])
        let attributedText1 = NSMutableAttributedString(string: "\(vehicleInfo.info1) \n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.darkGray])
        attributedString1.append(attributedText1)
        
        let attributedString2 = NSMutableAttributedString(string: "\(vehicleInfo.title2) \n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .medium), NSAttributedString.Key.foregroundColor : UIColor.black])
        let attributedText2 = NSMutableAttributedString(string: "\(vehicleInfo.info2) \n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.darkGray])
        attributedString2.append(attributedText2)
        
        let attributedString3 = NSMutableAttributedString(string: "\(vehicleInfo.title3) \n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .medium), NSAttributedString.Key.foregroundColor : UIColor.black])
        let attributedText3 = NSMutableAttributedString(string: "\(vehicleInfo.info3) \n\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedString.Key.foregroundColor : UIColor.darkGray])
        attributedString3.append(attributedText3)
        
        attributedString2.append(attributedString3)
        attributedString1.append(attributedString2)
        
        if isFareDetailsShowing == true{
            cell.lblfareDesc.isHidden = false
            cell.lblfareDesc.attributedText = attributedString1
        }else{
            cell.lblfareDesc.isHidden = true
            cell.lblfareDesc.text = ""
        }
        
        cell.lblVehiclaName.text = selectedCabBo.name
        cell.lblSelectedVehiclaName.text = selectedCabBo.name
        cell.imgViewVehicle.sd_setImage(with: URL(string: selectedCabBo.vehImg_White), placeholderImage: UIImage(named: ""))
        
        cell.lblCityName.text = selectedCityBo.name
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.viewTotalTapAction(_:)))
        cell.viewTotalFare.isUserInteractionEnabled = true
        cell.viewTotalFare.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.selectCabAction(_:)))
        cell.viewCabSelection.isUserInteractionEnabled = true
        cell.viewCabSelection.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.selectCityAction(_:)))
        cell.viewCitySelection.isUserInteractionEnabled = true
        cell.viewCitySelection.addGestureRecognizer(tap3)
        
        return cell
    }
    
    //MARK: Button Action
    @objc func viewTotalTapAction(_ sender: UITapGestureRecognizer) {
        isFareDetailsShowing = !isFareDetailsShowing
        oneWayTableView.reloadData()
    }
    
    @objc func selectCabAction(_ sender: UITapGestureRecognizer) {
        self.serviceCallToGetSelectTripOptions(isShowPupup: true)
    }
    
    @objc func selectCityAction(_ sender: UITapGestureRecognizer){
        serviceCallToGetSelectCityOptions()
    }
    
    //MARK: Selection Delegate
    func selectedOption(customBo: CustomObject, type: String){
        if type == "City"{
            selectedCityBo.name = customBo.name
            selectedCityBo.id = customBo.id
        }else if type == "Category"{
            selectedCabBo.name = customBo.name
            selectedCabBo.id = customBo.id
            selectedCabBo.vehImg_White = customBo.vehImg_White
        }
        
        self.oneWayTableView.reloadData()
    }
    
    func showSelection(arrCustomData: [CustomObject], type: String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MakeYourSelectionViewController") as! MakeYourSelectionViewController
        vc.arrSelection = arrCustomData
        vc.type = type
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Service Call
    func serviceCallToGetSelectCityOptions() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),"access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("url===\(AppConstant.cityListUrl)")
            print("params===\(params!)")
            
            AF.request(AppConstant.cityListUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Rate Card : \(response)")
                    switch(response.result) {
                    case let .success(value):
                        
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if status == 1 {//Success
                                    self.arrCityList.removeAll()
                                    if let dictData = dict["data"] as? [[String: Any]] {
                                        for dict in dictData{
                                            let cityListBo = CustomObject()
                                            if let city_Name = dict["city_name"] as? String {
                                                cityListBo.name = city_Name
                                            }
                                            if let city_Id = dict["city_id"] as? String {
                                                cityListBo.id = city_Id
                                            }
                                            self.arrCityList.append(cityListBo)
                                        }
                                        for city in self.arrCityList{
                                            if AppConstant.cityName == city.name{
                                                self.selectedCityBo.name = city.name
                                                self.selectedCityBo.id = city.id
                                            }
                                        }
                                        if self.selectedCityBo.name == ""{
                                            self.selectedCityBo = self.arrCityList[0]
                                        }
                                    }
                                    self.showSelection(arrCustomData: self.arrCityList, type: "City")
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    func serviceCallToGetSelectTripOptions(isShowPupup: Bool) {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "city_name" : AppConstant.cityName,
                "ride_type" : "0"
            ]
            
            print("url===\(AppConstant.getRateCardUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.getRateCardUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int{
                                if status == 1{//Success
                                    self.arrCityList.removeAll()
                                    if let dictRateCard = dict["rateCard"] as? [String: Any]{
                                        if let arrCategory = dictRateCard["category"] as? [[String: Any]]{
                                            for dict in arrCategory{
                                                let customBo = CustomObject()
                                                if let cat_id = dict["cat_id"] as? String {
                                                    customBo.id = cat_id
                                                }
                                                if let category_name = dict["category_name"] as? String {
                                                    customBo.name = category_name
                                                }
                                                if let image_white = dict["image_white"] as? String {
                                                    customBo.vehImg_White = image_white
                                                }
                                                self.arrCityList.append(customBo)
                                            }
                                        }
                                        if let dictFare = dictRateCard["fare"] as? [String: Any]{
                                            if let title1 = dictFare["title1"] as? String{
                                                self.vehicleInfo.title1 = title1
                                            }
                                            if let info1 = dictFare["info1"] as? String{
                                                self.vehicleInfo.info1 = info1
                                            }
                                            if let title2 = dictFare["title2"] as? String{
                                                self.vehicleInfo.title2 = title2
                                            }
                                            if let info2 = dictFare["info2"] as? String{
                                                self.vehicleInfo.info2 = info2
                                            }
                                            if let title3 = dictFare["title3"] as? String{
                                                self.vehicleInfo.title3 = title3
                                            }
                                            if let info3 = dictFare["info3"] as? String{
                                                self.vehicleInfo.info3 = info3
                                            }
                                        }
                                    }
                                    if isShowPupup{
                                        self.showSelection(arrCustomData: self.arrCityList, type: "Category")
                                    }else{
                                        if self.arrCityList.count > 0 {
                                            self.selectedCabBo = self.arrCityList[0]
                                        }
                                    }
                                    
                                    self.oneWayTableView.reloadData()
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    
}
