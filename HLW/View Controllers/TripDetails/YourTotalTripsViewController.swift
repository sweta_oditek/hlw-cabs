//
//  YourTotalTripsViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 1/15/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class YourTotalTripsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tblViewTrip: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    var arrTrips = [BookingStatusBO]()
    var bookingStatusBo = BookingStatusBO()
    var selectedBookId: String = ""
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    var dropCoordinate : CLLocationCoordinate2D? = nil

    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        lblNoDataFound.isHidden = true
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
        }
        
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControl.Event.valueChanged)
        tblViewTrip.addSubview(refreshControl)
        
        self.serviceCallToGetTripList()
        
        //Enable left swipe to back
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "TotalTripPage", screenClass: "YourTotalTripsViewController")
    }
    
    //MARK: Button Action
    @objc func pullToRefresh(sender:AnyObject) {
        // Code to refresh table view
        self.serviceCallToGetTripList()
        refreshControl.endRefreshing()
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @objc func btnCancelScheduleBookingAction(_ sender: UIButton){
        self.bookingStatusBo = arrTrips[sender.tag]
        if bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.scheduleBooking {
            self.showAlertForCancelBooking(strTitle: "Cancel Booking", strDescription: "Are you sure to cancel booking", delegate: self)
        }
        
    }
    
    //MARK: Tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTrips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YourTotalTripsTableViewCell", for: indexPath as IndexPath) as! YourTotalTripsTableViewCell
        cell.selectionStyle = .none
        
        let tripBo = self.arrTrips[indexPath.row]
        
        cell.lblCRN.text = tripBo.tripListInfoBo.bookId
        
//        tripBo.tripListInfoBo.bookStatus = StringConstant.bookingConfirm
//        tripBo.tripListInfoBo.bookStatus = StringConstant.startRide
        
        if tripBo.tripListInfoBo.bookStatus == StringConstant.cancelByCustomer||tripBo.tripListInfoBo.bookStatus == StringConstant.cancelByDriver{//Booking Canceled
            cell.tripCancelImage.isHidden = false
            cell.lblPrice.text = ""
            cell.viewScheduleDate.isHidden = true
            cell.viewScheduleHeightConstraint.constant = 0
        }else if tripBo.tripListInfoBo.bookStatus == StringConstant.bookingConfirm||tripBo.tripListInfoBo.bookStatus == StringConstant.startRide {
            cell.tripCancelImage.isHidden = true
            cell.lblPrice.text = "ONGOING"
            cell.lblPrice.textColor = UIColor.init(hexString: "18C302")
            cell.viewScheduleDate.isHidden = true
            cell.viewScheduleHeightConstraint.constant = 0
            //cell.lblPrice.setTitleColor(AppConstant.colorThemeBlue, for: .normal)
        }else if tripBo.tripListInfoBo.bookStatus == StringConstant.scheduleBooking {//Schedule Booking
            cell.viewScheduleDate.isHidden = false
            cell.viewScheduleHeightConstraint.constant = 40
            cell.tripCancelImage.isHidden = true
            cell.lblPrice.text = ""
        }else if tripBo.tripListInfoBo.bookStatus == StringConstant.rideCompleted {//Completed
            cell.viewScheduleDate.isHidden = true
            cell.viewScheduleHeightConstraint.constant = 0
            cell.tripCancelImage.isHidden = true
            cell.lblPrice.text = "₹\(tripBo.tripListInfoBo.totalfare)"
        }else{
            cell.viewScheduleDate.isHidden = true
            cell.viewScheduleHeightConstraint.constant = 0
            cell.tripCancelImage.isHidden = true
            cell.lblPrice.text = ""
        }
        cell.lblTripStatus.text
            = tripBo.tripListInfoBo.bookStatusStr
        cell.lblRideType.text = tripBo.tripListInfoBo.rideName + " - "
        cell.lblVehicleType.text = tripBo.tripListInfoBo.catName
        cell.lblRideDate.text = tripBo.tripListInfoBo.bookDate + " " + tripBo.tripListInfoBo.bookTime
        cell.imgViewCab.sd_setImage(with: URL(string: tripBo.tripListInfoBo.imgPath), placeholderImage: UIImage(named: ""))
        cell.lblScheduleDate.text = "Schedule Date: \(tripBo.tripListInfoBo.scheduleDate) \(tripBo.tripListInfoBo.scheduleTime)"
        
        cell.btnCancelScheduleBooking.tag = indexPath.row
        cell.btnCancelScheduleBooking.addTarget(self, action: #selector(btnCancelScheduleBookingAction(_:)), for: .touchUpInside)
        
        if tripBo.tripListInfoBo.rideId == "0"{//One Way
            cell.lblPickUpAddress.text = tripBo.tripListInfoBo.source
            cell.lblDropAddress.text = tripBo.tripListInfoBo.destination
        }else{//Rental
            if tripBo.tripListInfoBo.bookStartDateTime == "" || tripBo.tripListInfoBo.bookEndDateTime == ""{
                cell.lblPickUpAddress.text = tripBo.tripListInfoBo.source == "" ? "NA" : tripBo.tripListInfoBo.source
                cell.lblDropAddress.text = tripBo.tripListInfoBo.destination == "" ? "NA" : tripBo.tripListInfoBo.destination
            }else{
                cell.lblPickUpAddress.text = tripBo.tripListInfoBo.bookStartDateTime
                cell.lblDropAddress.text = tripBo.tripListInfoBo.bookEndDateTime
            }
            
//            cell.lblPickUpAddress.text = tripBo.tripListInfoBo.bookStartDateTime == "" ? "NA" : tripBo.tripListInfoBo.bookStartDateTime
//            cell.lblDropAddress.text = tripBo.tripListInfoBo.bookEndDateTime == "" ? "NA" : tripBo.tripListInfoBo.bookEndDateTime
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        bookingStatusBo = self.arrTrips[indexPath.row]
        selectedBookId = bookingStatusBo.tripListInfoBo.bookId
        if bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.bookingConfirm||bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.driverArrived||bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.startRide||bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.stopRide {
            self.bookingStatusBo.bookingInfoBo.book_status = self.bookingStatusBo.tripListInfoBo.bookStatus
            self.serviceCallToGetBookingStatusDetails(bookStatus: bookingStatusBo.tripListInfoBo.bookStatus)
            return
        }else if bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.newCustomerBooking||bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.scheduleBooking {
            self.showAlert(alertTitle: "", alertMessage: "No Data To Show")
        }else{
            self.performSegue(withIdentifier: "trip_details", sender: self)
        }
        
    }
    
    //MARK: Service Call Method
    func serviceCallToGetBookingStatusDetails(bookStatus: String) {
        if AppConstant.hasConnectivity() {
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "book_id" : selectedBookId,
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)
            ]
            
            print("params===\(params!)")
            print("Api===\(AppConstant.getBookingStatusDetailsUrl)")
            
            AF.request( AppConstant.getBookingStatusDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    debugPrint(response)
                    
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if(status == "1"){//Success
                                    
                                    if let bookId = dict["book_id"] as? String{
                                        self.bookingStatusBo.bookingInfoBo.bookId = bookId
                                    }
                                    if let dictBookInfo = dict["book_info"] as? [String: Any]{
                                        if let bookOtp = dictBookInfo["book_otp"] as? String{
                                            self.bookingStatusBo.driverBo.otp = bookOtp
                                        }
                                        if let pickPnt = dictBookInfo["pick_pnt"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.pick_pnt = pickPnt
                                        }
                                        if let dropPnt = dictBookInfo["drop_pnt"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.drop_pnt = dropPnt
                                        }
                                        if let srcLat = dictBookInfo["src_lat"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.src_lat = srcLat
                                        }
                                        if let srcLon = dictBookInfo["src_lon"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.src_lon = srcLon
                                        }
                                        if let desLat = dictBookInfo["des_lat"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.des_lat = desLat
                                        }
                                        if let desLon = dictBookInfo["des_lon"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.des_lon = desLon
                                        }
                                        if let rideId = dictBookInfo["ride_id"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.ride_id = rideId
                                        }
                                        if let rideName = dictBookInfo["ride_name"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.ride_name = rideName
                                        }
                                        if let catName = dictBookInfo["cat_name"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.cat_name = catName
                                        }
                                        if let waitTime = dictBookInfo["wait_time"] as? Int{
                                            self.bookingStatusBo.bookingInfoBo.wait_time = waitTime
                                        }
                                        if let rideCompleted = dictBookInfo["ride_completed"] as? String{
                                            if rideCompleted == ""{
                                                self.bookingStatusBo.bookingInfoBo.ride_completed = 0
                                            }else{
                                                self.bookingStatusBo.bookingInfoBo.ride_completed = Int(rideCompleted)!
                                            }
                                        }
                                        
                                    }
                                    if let dictRideFareInfo = dict["ride_fare"] as? [String: Any]{
                                        if let total_Cost = dictRideFareInfo["total_cost"] as? String{
                                            self.bookingStatusBo.rideFareBo.totalCost = total_Cost
                                        }
                                        if let tax = dictRideFareInfo["tax"] as? String{
                                            self.bookingStatusBo.rideFareBo.tax = tax
                                        }
                                        if let pay_Mode = dictRideFareInfo["pay_mode"] as? String{
                                            self.bookingStatusBo.rideFareBo.payMode = pay_Mode
                                        }
                                        if let fareDetails = dictRideFareInfo["fare_detail"] as? [[String: Any]]{
                                            for dict in fareDetails{
                                                let fareBO = FareDetailsBO()
                                                if let key = dict["key"] as? String{
                                                    fareBO.title = key
                                                }
                                                if let value = dict["value"] as? Double{
                                                    fareBO.value = String(value)
                                                }
                                                if let value = dict["value"] as? String{//Need to Change
                                                    fareBO.value = value
                                                }
                                                if let type = dict["type"] as? Int{
                                                    fareBO.type = type
                                                }
                                                self.bookingStatusBo.rideFareBo.arrFareDetails.append(fareBO)
                                            }
                                        }
                                    }
                                    if let dictDriverInfo = dict["driver_info"] as? [String: Any]{
                                        if let driverId = dictDriverInfo["driver_id"] as? String {
                                            self.bookingStatusBo.driverBo.driver_id = driverId
                                        }
                                        if let driverName = dictDriverInfo["driver_name"] as? String {
                                            self.bookingStatusBo.driverBo.driver_name = driverName
                                        }
                                        if let driverMobile = dictDriverInfo["driver_mobile"] as? String {
                                            self.bookingStatusBo.driverBo.driver_mobile = driverMobile
                                        }
                                        if let driverRating = dictDriverInfo["driver_rating"] as? String {
                                            self.bookingStatusBo.driverBo.driver_rating = driverRating
                                        }
                                        if let driverImage = dictDriverInfo["driver_image"] as? String {
                                            self.bookingStatusBo.driverBo.driver_image = driverImage
                                        }
                                        if let vehcile_reg_No = dictDriverInfo["vehcile_reg_no"] as? String {
                                            self.bookingStatusBo.driverBo.vehcile_registration_number = vehcile_reg_No
                                        }
                                        if let vehcile_brand_Name = dictDriverInfo["vehcile_brand_name"] as? String {
                                            self.bookingStatusBo.driverBo.vehcile_brand_name = vehcile_brand_Name
                                        }
                                        if let vehcile_model_Name = dictDriverInfo["vehcile_model_name"] as? String {
                                            self.bookingStatusBo.driverBo.vehcile_model_name = vehcile_model_Name
                                        }
                                        if let vehicleImage = dictDriverInfo["vehicle_image"] as? String {
                                            self.bookingStatusBo.driverBo.vehicle_image = vehicleImage
                                        }
                                    }
                                    if bookStatus == StringConstant.bookingConfirm||bookStatus == StringConstant.driverArrived||bookStatus == StringConstant.startRide {
                                        self.performSegue(withIdentifier: "RideDetails", sender: self)
                                    }else{
                                        self.performSegue(withIdentifier: "ShowBillDetails", sender: self)
                                    }
                                    
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetTripList() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
            ]
            
            print("url===\(AppConstant.tripHistoryUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.tripHistoryUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint("Ride History : \(response)")
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            self.arrTrips.removeAll()
                            if let status = dict["status"] as? String {
                                if(status == "1"){
                                    if let arrData = dict["book_data"] as? [[String: Any]]{
                                        for dictData in arrData{
                                            let tripBo = BookingStatusBO()
                                            if let book_id = dictData["book_id"] as? String{
                                                tripBo.tripListInfoBo.bookId = book_id
                                            }
                                            if let book_status = dictData["book_status"] as? String{
                                                tripBo.tripListInfoBo.bookStatus = book_status
                                            }
                                            if let book_status_str = dictData["book_status_str"] as? String{
                                                tripBo.tripListInfoBo.bookStatusStr = book_status_str
                                            }
                                            if let start_date_time = dictData["start_date_time"] as? String{
                                                tripBo.tripListInfoBo.bookStartDateTime = start_date_time
                                            }
                                            if let end_date_time = dictData["end_date_time"] as? String{
                                                tripBo.tripListInfoBo.bookEndDateTime = end_date_time
                                            }
                                            if let ride_id = dictData["ride_id"] as? String{
                                                tripBo.tripListInfoBo.rideId = ride_id
                                            }
                                            if let ride_name = dictData["ride_name"] as? String{
                                                tripBo.tripListInfoBo.rideName = ride_name
                                            }
                                            if let rental_name = dictData["rental_name"] as? String{
                                                tripBo.tripListInfoBo.rentalName = rental_name
                                            }
                                            if let cat_id = dictData["cat_id"] as? String{
                                                tripBo.tripListInfoBo.catId = cat_id
                                            }
                                            if let cat_name = dictData["cat_name"] as? String{
                                                tripBo.tripListInfoBo.catName = cat_name
                                            }
                                            if let cat_image = dictData["cat_image"] as? String{
                                                tripBo.tripListInfoBo.imgPath = cat_image
                                            }
                                            if let book_date = dictData["book_date"] as? String{
                                                tripBo.tripListInfoBo.bookDate = book_date
                                            }
                                            if let book_time = dictData["book_time"] as? String{
                                                tripBo.tripListInfoBo.bookTime = book_time
                                            }
                                            if let src_loc = dictData["src_loc"] as? String{
                                                tripBo.tripListInfoBo.source = src_loc
                                            }
                                            if let des_loc = dictData["des_loc"] as? String{
                                                tripBo.tripListInfoBo.destination = des_loc
                                            }
                                            if let total_fare = dictData["total_fare"] as? String{
                                                tripBo.tripListInfoBo.totalfare = total_fare
                                            }
                                            if let schedule_Date = dictData["schedule_date"] as? String{
                                                tripBo.tripListInfoBo.scheduleDate = schedule_Date
                                            }
                                            if let schedule_Time = dictData["schedule_time"] as? String{
                                                tripBo.tripListInfoBo.scheduleTime = schedule_Time
                                            }
                                            
                                            self.arrTrips.append(tripBo)
                                        }
                                    }
                                    self.lblNoDataFound.isHidden = self.arrTrips.count > 0 ? true : false
                                    
                                    self.tblViewTrip.reloadData()
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.lblNoDataFound.isHidden = false
                                    }
                                }
                                
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetCancelBooking() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                "action" : "cancelbook",
                "policy_id" : "0",
                "book_id" : bookingStatusBo.tripListInfoBo.bookId
            ]
            
            print("url===\(AppConstant.cancelBookingUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.cancelBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{
                                    self.showAlert(alertTitle: "Success", alertMessage: "Booking has been cancelled successfully")
                                    self.serviceCallToGetTripList()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    //MARK: Alert Method
    func showAlertForCancelBooking(strTitle: String,strDescription: String,delegate: AnyObject?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: strTitle, message: strDescription, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
            
            let yesAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.serviceCallToGetCancelBooking()
            })
            
            alert.addAction(yesAction)
            alert.preferredAction = yesAction
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Segue Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "trip_details"{
            let vc  =  segue.destination as! TripDetailsViewController
            vc.bookId = selectedBookId
        }
        if segue.identifier == "RideDetails"{
            let vc = segue.destination as! DriverToReachPickUpPointViewController
            
            if self.bookingStatusBo.bookingInfoBo.ride_id == "0"{
                pickUpCoordinate = CLLocationCoordinate2D(latitude:Double(self.bookingStatusBo.bookingInfoBo.src_lat)!
                    , longitude:Double(self.bookingStatusBo.bookingInfoBo.src_lon)!)
                dropCoordinate = CLLocationCoordinate2D(latitude:Double(self.bookingStatusBo.bookingInfoBo.des_lat)!
                    , longitude:Double(self.bookingStatusBo.bookingInfoBo.des_lon)!)
                
                vc.pickUpCoordinate = self.pickUpCoordinate
                vc.dropCoordinate = self.dropCoordinate
                vc.pickUpAddress = self.bookingStatusBo.bookingInfoBo.pick_pnt
                vc.dropAddress = self.bookingStatusBo.bookingInfoBo.drop_pnt
                vc.bookingStatusBo = self.bookingStatusBo
            }else{
//                pickUpCoordinate = CLLocationCoordinate2D(latitude:Double(self.bookingStatusBo.bookingInfoBo.src_lat)!
//                    , longitude:Double(self.bookingStatusBo.bookingInfoBo.src_lon)!)
                pickUpCoordinate = CLLocationCoordinate2D(latitude: 20.296222, longitude: 85.842078)
                vc.pickUpCoordinate = self.pickUpCoordinate
                vc.pickUpAddress = self.bookingStatusBo.bookingInfoBo.pick_pnt
                vc.bookingStatusBo = self.bookingStatusBo
            }
            vc.isFromTotalTrip = true
        }
        if segue.identifier == "ShowBillDetails"{
            let vc = segue.destination as! BillViewController
            vc.bookingStatusBo = self.bookingStatusBo
        }
    }

}
