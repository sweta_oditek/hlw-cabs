//
//  FeedbackViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 2/11/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class FeedbackViewController: UIViewController {
    
    @IBOutlet var imgViewDriver: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    
    @IBOutlet var imgViewRating: UIImageView!
    @IBOutlet weak var lblRatingTitle: UILabel!
    @IBOutlet weak var cosmosViewRatingStar: CosmosView!
    
    @IBOutlet var imgViewVehicle: UIImageView!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleRegNo: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    var bookingStatusBo = BookingStatusBO()
    var ratingStar: Int = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
            btnHeightConstraint.constant = 60
        }
        imgViewDriver.layer.cornerRadius = imgViewDriver.frame.size.width / 2
        imgViewDriver.clipsToBounds = true
        
        imgViewRating.layer.cornerRadius = imgViewRating.frame.size.width / 2
        imgViewRating.clipsToBounds = true
        
        view1.layer.cornerRadius = 5
        view1.clipsToBounds = true
        view1.dropShadow()
        
        view2.layer.cornerRadius = 5
        view2.clipsToBounds = true
        view2.dropShadow()
        
        let imgDriver: String = bookingStatusBo.driverBo.driver_image
        imgViewDriver.sd_setImage(with: URL(string: imgDriver), placeholderImage: UIImage(named: "driver"))
        lblDriverName.text = self.bookingStatusBo.driverBo.driver_name
        
        var imgVehicle: String = bookingStatusBo.driverBo.vehicle_image
        imgVehicle = imgVehicle.replacingOccurrences(of: " ", with: "%20", options: .caseInsensitive, range: nil)
        imgViewVehicle.sd_setImage(with: URL(string: imgVehicle), placeholderImage: UIImage(named: "car_micro_blue"))
        lblVehicleName.text = "\(bookingStatusBo.driverBo.vehcile_brand_name) \(bookingStatusBo.driverBo.vehcile_model_name)"
        lblVehicleRegNo.text = bookingStatusBo.driverBo.vehcile_registration_number
        
        imgViewRating.image = UIImage.init(named: "ride_great")
        lblRatingTitle.text = "A great ride!"
        cosmosViewRatingStar.rating = 5
        
        cosmosViewRatingStar.settings.fillMode = .full
        cosmosViewRatingStar.didFinishTouchingCosmos = { rating in
            self.ratingStar = Int(rating)
            print(rating)
            if self.ratingStar == 1{
                self.imgViewRating.image = UIImage.init(named: "ride_very_bad")
                self.lblRatingTitle.text = "Very Bad!"
            }
            else if self.ratingStar == 2{
                self.imgViewRating.image = UIImage.init(named: "ride_bad")
                self.lblRatingTitle.text = "A bad one!"
            }
            else if self.ratingStar == 3{
                self.imgViewRating.image = UIImage.init(named: "ride_ok")
                self.lblRatingTitle.text = "An ok ok ride!"
            }
            else if self.ratingStar == 4{
                self.imgViewRating.image = UIImage.init(named: "ride_good")
                self.lblRatingTitle.text = "A good ride!"
            }
            else if self.ratingStar == 5{
                self.imgViewRating.image = UIImage.init(named: "ride_great")
                self.lblRatingTitle.text = "A great ride!"
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "FeedbackPage", screenClass: "FeedbackViewController")
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.goToMainScreen()
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        self.serviceCallToUpdateRating()
    }
    
    //MARK: - Service Call Method
    func serviceCallToUpdateRating() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id), "book_id": AppConstant.retrievFromDefaults(key: StringConstant.book_id),"access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),"rating": self.ratingStar]
            
            print("Url===\(AppConstant.updateRatingUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.updateRatingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{
                                    self.showAlert(title: "Success", desc: "Thank you for your feedback")
                                }else{
                                    self.showAlert(title: "Error", desc: "Something went wrong, Please try after some time.")
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    //MARK: - Alert Method
    func showAlert(title: String, desc: String){
        
        let alert = UIAlertController(title: title, message: desc, preferredStyle: .alert)
        
        let okBtn = UIAlertAction(title: "OK", style: .default){ (action)in
            UIView.animate(withDuration: 0.25, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                self.view.alpha = 0.0
            }, completion: {(finished : Bool) in
                if(finished)
                {
                    self.willMove(toParent: nil)
                    self.removeFromParent()
                    self.view.removeFromSuperview()
                    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.goToMainScreen()
                }
            })
        }
        alert.addAction(okBtn)
        self.present(alert, animated: true, completion: nil)
    }

    
}
extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 1
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
