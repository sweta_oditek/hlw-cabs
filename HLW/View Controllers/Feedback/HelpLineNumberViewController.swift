//
//  HelpLineNumberViewController.swift
//  HLW
//
//  Created by ODITEK on 31/08/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class HelpLineNumberViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblViewUserSosList: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    
    var arrUserSosList = [UserSOSDataBO]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
        
        tblViewUserSosList.delegate = self
        tblViewUserSosList.dataSource = self
        tblViewUserSosList.tableFooterView = UIView()
        
        self.serviceCallToUserSosHelpNumbers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "HelpLineNumberScreen", screenClass: "HelpLineNumberViewController")
    }
    
    //MARK: Tableview Delgate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUserSosList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userSosNumberList", for: indexPath as IndexPath)as! SOSCallTableViewCell
        cell.selectionStyle = .none
        
        let userSosBo = arrUserSosList[indexPath.row]
        cell.lblContactName.text = userSosBo.sosNumber
        
        cell.btnCall.tag = indexPath.row
        cell.btnCall.addTarget(self, action: #selector(btnCallAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: Button Actions
    @objc func btnCallAction(_ sender: UIButton){
        let phoneNumber: String = arrUserSosList[sender.tag].sosNumber.replacingOccurrences(of: " ", with: "")
        if let phoneCallURL:URL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(phoneCallURL as URL)
                }
                
            }
        }
    }
    
//    @IBAction func btnCall1Action(_ sender: Any) {
//        let phoneNumber:String = "1800121333300"
//        if let phoneCallURL:URL = URL(string: "tel://\(phoneNumber)") {
//            let application:UIApplication = UIApplication.shared
//            if (application.canOpenURL(phoneCallURL)) {
//                if #available(iOS 10.0, *) {
//                    application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
//                } else {
//                    UIApplication.shared.openURL(phoneCallURL as URL)
//                }
//
//            }
//        }
//    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.removeFromParent()
                self.view.removeFromSuperview()
            }
        })
    }
    
    //MARK: Service Call Method
    func serviceCallToUserSosHelpNumbers(){
        if AppConstant.hasConnectivity(){
            AppConstant.showHUD()
            
            let params: Parameters = ["user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                                      "access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken),
                                      "action": "sos"]
            
            print("Url===\(AppConstant.userSOSUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.userSOSUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    
                    switch(response.result) {
                        case let .success(value):
                            if let dict = value as? [String: Any]{
                                if let status = dict["status"] as? String{
                                    if status == "1"{//Success
                                        self.arrUserSosList.removeAll()
                                        if let arrUserSosInfo = dict["data"] as? [[String: Any]]{
                                            for dictData in arrUserSosInfo{
                                                let sosBo = UserSOSDataBO()
                                                if let sos = dictData["sos"] as? String{
                                                    sosBo.sosNumber = sos
                                                }
                                                
                                                self.arrUserSosList.append(sosBo)
                                            }
                                        }
                                        self.tblViewUserSosList.reloadData()
                                    }else{
                                        if let errorMsg = dict["msg"] as? String{
                                            self.showAlert(alertTitle: "", alertMessage: errorMsg)
                                        }
                                    }
                                }
                            }
                            
                            break
                        case let .failure(error):
                            self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                            break
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
