//
//  AboutUsViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 22/01/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import StoreKit

class AboutUsViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet var imgViewAppIcon: UIImageView!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnRateUs: UIButton!
    
    var header_Title: String = ""
    var show_Url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        if AppConstant.screenSize.height >= 812 {
            navBarHeightConstraint.constant = 92
        }
        
        imgViewAppIcon.layer.cornerRadius = imgViewAppIcon.frame.size.height/2
        imgViewAppIcon.clipsToBounds = true
        
        btnRateUs.layer.cornerRadius = 5
        btnRateUs.clipsToBounds = true
        
        lblVersion.text = "V \(AppConstant.getAppVersion())"
        
        //Enable left swipe to back
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "AboutusPage", screenClass: "AboutUsViewController")
    }
    
    //MARK: Button Action
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnTCAction(_ sender: Any) {
        header_Title = "Terms & Conditions"
        show_Url = "http://hlwcab.com/mobile/terms_and_conditions_mobile.html"
        self.performSegue(withIdentifier: "Terms&Conditions", sender: self)
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
        header_Title = "Privacy Policy"
        show_Url = "http://hlwcab.com/mobile/privacy_policy_mobile.html"
        self.performSegue(withIdentifier: "Terms&Conditions", sender: self)
    }
    
    @IBAction func btnRateUsAction(_ sender: Any) {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "Terms&Conditions"{
            let vc = segue.destination as! Term_ConditionViewController
            vc.headerTitle = self.header_Title
            vc.showUrl = self.show_Url
        }
    }
    
    //MARK: Rate the App on AppStore
    func rateApp() {
        if let url = URL(string: "https://itunes.apple.com/app/id1484939425" ) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
