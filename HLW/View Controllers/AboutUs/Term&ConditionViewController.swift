//
//  Term&ConditionViewController.swift
//  HLW
//
//  Created by ODITEK on 19/09/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import WebKit

class Term_ConditionViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var headerTitle: String = ""
    var showUrl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        if AppConstant.screenSize.height >= 812 {
            navBarHeightConstraint.constant = 92
        }
        
        AppConstant.showHUD()
        webView.navigationDelegate = self
        
        lblHeaderTitle.text = headerTitle
        webView.isHidden = true
        webView.load(NSURLRequest(url: NSURL (string: showUrl)! as URL) as URLRequest)
//        webView.allowsBackForwardNavigationGestures = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "Term_ConditionPage", screenClass: "Term_ConditionViewController")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppConstant.hideHUD()
        webView.isHidden = false
    }
    
    // MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
