//
//  EmergencyContactsViewController.swift
//  HLW
//
//  Created by OdiTek Solutions on 15/01/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire
import ContactsUI

class EmergencyContactsViewController: UIViewController, SlideMenuControllerDelegate, CNContactPickerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblViewContacts: UITableView!
    @IBOutlet var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblEmergencyDesc: UILabel!
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint!
    
    var arrContacts = [EmergencyContactBO]()
    var contactListBo = EmergencyContactBO()
    var isFromContactPicker = false

    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            navBarHeightConstraint.constant = 92
            btnHeightConstraint.constant = 60
        }
        
        if arrContacts.count < 1{
            lblEmergencyDesc.isHidden = false
            tblViewContacts.isHidden = true
        }else{
            lblEmergencyDesc.isHidden = true
            tblViewContacts.isHidden = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isFromContactPicker{
            serviceCallToGetEmergencyContact()
        }
        isFromContactPicker = false
        
        AppConstant.setTrackScreenName(screenName: "EmergencyContactsPage", screenClass: "EmergencyContactsViewController")
    }

    // MARK: Button Action
    @IBAction func btnSlideMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    @IBAction func btnAddContactAction(_ sender: Any) {
        if arrContacts.count < 5{
            isFromContactPicker = true
            let contacVC = CNContactPickerViewController()
            contacVC.delegate = self
            self.present(contacVC, animated: true, completion: nil)
        }else{
            self.showAlert(alertTitle: StringConstant.addContactErrorMsg, alertMessage: "")
        }
    }
    
    @objc func btnRemoveContactAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Remove", message: StringConstant.remove_contact_from_emergency_msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Remove", style: .destructive) { action in
            self.arrContacts.remove(at: sender.tag)
//            self.tblViewContacts.reloadData()
            
            if self.arrContacts.count < 1{
                self.lblEmergencyDesc.isHidden = false
                self.tblViewContacts.isHidden = true
            }else{
                self.lblEmergencyDesc.isHidden = true
                self.tblViewContacts.isHidden = false
            }
            let contactsBo = EmergencyContactBO()
            self.serviceCallToUpdateEmergencyContact(contact: contactsBo, isForAddNew: false)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            
        })
        self.present(alert, animated: true)
    }
    
    // MARK: Delegate method CNContectPickerDelegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let contactsBo = EmergencyContactBO()
        contactsBo.name = "\(contact.givenName) \(contact.familyName)"
        let numbers = contact.phoneNumbers.first
        contactsBo.number = (numbers?.value)?.stringValue
        contactsBo.share_status = "0"
        if self.checkIfContactNumberAlreadyExist(num: contactsBo.number!.replacingOccurrences(of: "-", with: "")) == false{
            if arrContacts.count < 1{
                lblEmergencyDesc.isHidden = false
                tblViewContacts.isHidden = true
            }else{
                lblEmergencyDesc.isHidden = true
                tblViewContacts.isHidden = false
            }
            self.serviceCallToUpdateEmergencyContact(contact: contactsBo, isForAddNew: true)
        }else{
            self.showAlert(alertTitle: StringConstant.contact_already_exist_emergency_list_msg, alertMessage: "")
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Tableview Delegates & Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrContacts.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:EmergencyContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EmergencyContactTableViewCell") as! EmergencyContactTableViewCell
        cell.selectionStyle = .none
        
        let contactBo = self.arrContacts[indexPath.section]
        cell.lblContactName.text = contactBo.name
        cell.lblContactNumber.text = contactBo.number
        cell.btnRemove.tag = indexPath.section
        cell.shareRideSwitch.tag = indexPath.section
        cell.btnRemove.addTarget(self, action: #selector(btnRemoveContactAction(_:)), for: .touchUpInside)
        cell.shareRideSwitch.isOn = contactBo.share_status == "1" ? true : false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return CGFloat.leastNonzeroMagnitude
        }
        else {
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func checkIfContactNumberAlreadyExist(num: String)-> Bool{
        for contact in self.arrContacts {
            if (num == contact.number){
                return true
            }
        }
        return false
    }
    
    //MARK: Switch Trigger Method
    
    @IBAction func switchDidStatusChange(_ sender: UISwitch) {
        let contactBo = self.arrContacts[sender.tag]
        contactBo.share_status = sender.isOn == true ? "1" : "0"
        serviceCallToUpdateEmergencyContact(contact: contactBo, isForAddNew: false)
    }
    
    //MARK: Service Call
    func serviceCallToGetEmergencyContact() {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),"access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("url===\(AppConstant.getEmergencyUrl)")
            print("params===\(params!)")
            
            AF.request(AppConstant.getEmergencyUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            self.arrContacts.removeAll()
                            if let status = dict["status"] as? Int{
                                if status == 1{
                                    if let arrContactInfo = dict["data"] as? [[String: Any]]{
                                        for dictData in arrContactInfo{
                                            let contactBo = EmergencyContactBO()
                                            if let name = dictData["name"] as? String{
                                                contactBo.name = name
                                            }
                                            if let mob = dictData["mobile"] as? String{
                                                contactBo.number = mob
                                            }
                                            if let share = dictData["share"] as? String{
                                                contactBo.share_status = share
                                            }
                                            self.arrContacts.append(contactBo)
                                        }
                                     }
                                    if self.arrContacts.count < 1{
                                        self.lblEmergencyDesc.isHidden = false
                                        self.tblViewContacts.isHidden = true
                                    }else{
                                        self.lblEmergencyDesc.isHidden = true
                                        self.tblViewContacts.isHidden = false
                                    }
                                    self.tblViewContacts.reloadData()
                                }else{
                                    self.lblEmergencyDesc.text = StringConstant.empty_emergency_contact_msg
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    func serviceCallToUpdateEmergencyContact(contact: EmergencyContactBO, isForAddNew: Bool) {
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            if isForAddNew{
                arrContacts.append(contact)
            }
            var arrEmergency = [[String: Any]]()
            for contact in arrContacts{
                let dict = ["name": contact.name!,"mobile": contact.number!.replacingOccurrences(of: "-", with: ""),"share": contact.share_status!]
                arrEmergency.append(dict)
            }
            
            var params: Parameters!
            params = ["user_id": AppConstant.retrievFromDefaults(key: StringConstant.user_id),"access_token": AppConstant.retrievFromDefaults(key: StringConstant.accessToken), "emergency": self.json(from: arrEmergency)!]
            
            print("url===\(AppConstant.updateEmergencyUrl)")
            print("params===\(params!)")
            
            AF.request( AppConstant.updateEmergencyUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int{
                                if status == 1{
                                    self.serviceCallToGetEmergencyContact()
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: "Error", alertMessage: msg)
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.ascii)
    }

}
