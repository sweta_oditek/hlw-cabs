//
//  SignInWithPaaswordViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 1/24/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class SignInWithPaaswordViewController: UIViewController {
    
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var passwordBtn: UIButton!
    @IBOutlet weak var btnLoginWithOtp: UIButton!
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    var mobileNumber: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        initDesigns()
    }
    
    func initDesigns() {
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizer.Direction.up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        
        let attrStrPassword1 : NSMutableAttributedString = NSMutableAttributedString(string: "Enter your password for ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Medium", size: 16.0)!])
        attrStrPassword1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 13/255.0, green: 13/255.0, blue: 13/255.0, alpha: 1.0), range: NSRange(location: 0,length: attrStrPassword1.length))
        let attrStrPassword2 : NSMutableAttributedString = NSMutableAttributedString(string: mobileNumber, attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 17.0)!])
        attrStrPassword1.append(attrStrPassword2)
        // set label Attribute
        lblInfo.attributedText = attrStrPassword1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "SignInWithPaaswordPage", screenClass: "SignInWithPaaswordViewController")
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func togglePasswordBtnAction(_ sender: Any) {
        if (self.passwordTf.isSecureTextEntry == true) {
            self.passwordBtn.setImage( UIImage.init(named: "showpass"), for: .normal)
            self.passwordTf.isSecureTextEntry =  false
        }else{
            self.passwordBtn.setImage( UIImage.init(named: "hidepass"), for: .normal)
            self.passwordTf.isSecureTextEntry =  true
        }
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        self.dismissKeyboard()
    }
    
    @IBAction func forgotPasswordBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "forgot_password", sender: self)
    }
    
    @IBAction func LoginWithOtpBtnAction(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logInBtnAction(_ sender: Any) {
        if (passwordTf.text!.trim() == "") {
            self.showAlert(alertTitle: StringConstant.password_blank_validation_TitleMsg, alertMessage: StringConstant.password_blank_validation_DescMsg)
        }else{
            serviceCallToLogin()
        }
    }
    
    //MARK: Api Service call Method
    func serviceCallToLogin(){
        if AppConstant.hasConnectivity(){
            AppConstant.showHUD()
            var params: Parameters!
            params = ["mobile": mobileNumber,
                      "password": passwordTf.text!.trim()]
            print("params ===\(params!)")
            
            AF.request(AppConstant.loginWithPassworFdUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
                AppConstant.hideHUD()
                debugPrint(response)
                switch(response.result) {
                case let .success(value):
                
                    if let dict = value as? [String: Any]{
                        debugPrint(dict)
                        
                        if let status = dict["status"] as? Int{
                            if(status == 0){
                                if let msg = dict["msg"] as? String{
                                    self.showAlert(alertTitle: msg, alertMessage: "")
                                }
                            }else  if(status == 1){ //Success
                                if let name = dict["name"] as? String{
                                    AppConstant.saveInDefaults(key: StringConstant.name, value: name)
                                }
                                if let uId = dict["user_id"] as? String{
                                    AppConstant.saveInDefaults(key: StringConstant.user_id, value: uId)
                                }
                                if let mobile = dict["mobile"] as? String{
                                    AppConstant.saveInDefaults(key: StringConstant.mobile, value: mobile)
                                }
                                if let email = dict["email"] as? String{
                                    AppConstant.saveInDefaults(key: StringConstant.email, value: email)
                                }
                                if let accessToken = dict["access_token"] as? String{
                                    AppConstant.saveInDefaults(key: StringConstant.accessToken, value: accessToken)
                                }
                                
                                AppConstant.saveInDefaults(key: StringConstant.isLoggedIn, value: "1")
                                
                                self.performSegue(withIdentifier: "home_screen", sender: self)
                            }
                        }
                    }
                    
                    break
                    
                case let .failure(error):
                    self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                    break
                    
                }
                
            }
        }else{
                AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
            }
    }
    
/*    func serviceCallToLogin()*/
}

