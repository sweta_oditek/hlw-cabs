//
//  LandingScreenViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 1/25/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import EAIntroView

class LandingScreenViewController: UIViewController, EAIntroDelegate {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var viewYourMobileNumber: UIView!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblText: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesigns()
    }
    
    func initDesigns() {
        if (AppConstant.retrieveBoolFromDefaults(key: StringConstant.isIntroduceScreenShown) == false) {
            //Show Indroduction
            self.showIntro()
        }
        
        if AppConstant.screenSize.width > 320{//Screen above 5s
            lblMobile.font = UIFont.init(name: StringConstant.appFontPoppinsMedium, size: 18.0)
        }
        
        logoImage.layer.cornerRadius = logoImage.frame.width / 2
        logoImage.clipsToBounds = true
        
        viewYourMobileNumber.layer.borderWidth = 2
        viewYourMobileNumber.layer.borderColor = AppConstant.colorThemeBlue.cgColor
        viewYourMobileNumber.clipsToBounds = true
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.viewYourMobileNumberAction(_:)))
        viewYourMobileNumber.addGestureRecognizer(tap1)
        viewYourMobileNumber.isUserInteractionEnabled = true
        
        let attributedText = NSMutableAttributedString(string: "Please enter your mobile number to get start your", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .regular)])
        let attributedTxt = NSMutableAttributedString(string: "\n new journey", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold)])
        attributedText.append(attributedTxt)
        lblText.attributedText = attributedText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "LandingScreenPage", screenClass: "LandingScreenViewController")
    }
    
    //MARK: - Button Action
    @objc func viewYourMobileNumberAction(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "login", sender: self)
    }
    
    //MARK: Introdution View
    func showIntro() {
        let page1 = EAIntroPage.init(customViewFromNibNamed: "Introduction1")
        let page2 = EAIntroPage.init(customViewFromNibNamed: "Introduction2")
        let page3 = EAIntroPage.init(customViewFromNibNamed: "Introduction3")
        let page4 = EAIntroPage.init(customViewFromNibNamed: "Introduction4")
        let page5 = EAIntroPage.init(customViewFromNibNamed: "Introduction5")
        
        let introView = EAIntroView.init(frame: self.view.bounds, andPages: [page1!,page2!,page3!,page4!,page5!])
        introView?.delegate = self
        introView?.skipButton.alpha = 1.0
        introView?.skipButton.setTitle("Skip", for: .normal)
        introView?.skipButton.isEnabled = true
        
        introView?.show(in: self.view)
        
    }
    
    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
        AppConstant.saveBoolInDefaults(key: StringConstant.isIntroduceScreenShown, value: true)
        if(wasSkipped) {
            
            print("Intro skipped")
            
        } else {
            
            print("Intro not skipped")
        }
    }

}
