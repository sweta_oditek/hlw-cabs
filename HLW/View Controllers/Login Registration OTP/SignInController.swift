//
//  SignInController.swift
//  Taxi Booking
//
//  Created by Chinmaya Sahu on 02/02/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire
import EAIntroView
import NVActivityIndicatorView
import ADCountryPicker

class SignInController: UIViewController, UITextFieldDelegate, EAIntroDelegate, ADCountryPickerDelegate {
    
    @IBOutlet weak var mobileNoTf: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var mobileNoView: UIView!
    @IBOutlet weak var imgViewCountryFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var lblCountryCallingCode: UILabel!
    @IBOutlet weak var imgMenuCountryList: UIImageView!
    
    @IBOutlet var loginBtnBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint!
    
    var userid : String!
    var mobile : String!
    var token : String!
    let limitLength = 10
    
    let countryCodePicker = ADCountryPicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDesign()
    }
    
    func initDesign(){
        
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
            btnHeightConstraint.constant = 60
        }
        
        //Manage for iPhone 5 and Below
        if AppConstant.screenSize.height <= 568{
            mobileNoTf.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        }
        
        mobileNoTf.delegate = self
        self.mobileNoTf.becomeFirstResponder()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizer.Direction.up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        
        mobileNoView.layer.cornerRadius = 5
        mobileNoView.clipsToBounds = true
        
        //Get Country code from locale
        let bundle = "country_assets.bundle/"
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            if let image = UIImage(named: bundle + countryCode.uppercased(), in: Bundle(for:SignInController.self), compatibleWith: nil){
                imgViewCountryFlag.image = image
            }
            self.lblCountryCode.text = countryCode
            self.lblCountryCallingCode.text = getCountryCallingCode(countryRegionCode: countryCode)
        }
        
    }
    
    func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = "+" + prefixCodes[countryRegionCode]!
        return countryDialingCode
        
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        self.dismissKeyboard()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNoTf){
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength
        }
        else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        AppConstant.setTrackScreenName(screenName: "SignInPage", screenClass: "SignInController")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Button Action
    @IBAction func loginBtnAction(_ sender: Any) {
        var errTitleMessage : String?
        var errDescMessage : String?
        let mobile = self.mobileNoTf?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let countryCode = self.lblCountryCallingCode?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(mobile == ""){
            errTitleMessage = StringConstant.mobile_blank_validation_TitleMsg
            errDescMessage = StringConstant.mobile_blank_validation_DescMsg
        }else if((mobile?.count)! < 10){
            errTitleMessage = StringConstant.alert_TitleMsg
            errDescMessage = StringConstant.validate_mobile_msg
        }
        
        if(errTitleMessage != nil || errDescMessage != nil){
            self.showAlert(alertTitle: errTitleMessage!, alertMessage: errDescMessage!)
        }else {
            serviceCallToLogin(mobile: mobile!, countryCode: countryCode!)
        }
    }
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountryCodeAction(_ sender: Any) {
        countryCodePicker.delegate = self
        countryCodePicker.showCallingCodes = true
        let pickerNavigationController = UINavigationController(rootViewController: countryCodePicker)
        pickerNavigationController.navigationBar.barTintColor = AppConstant.colorThemeBlue
        pickerNavigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        pickerNavigationController.navigationBar.isTranslucent = false
        countryCodePicker.closeButtonTintColor = UIColor.white
        countryCodePicker.font = UIFont(name: "Poppins-Regular", size: 15)
        self.present(pickerNavigationController, animated: true, completion: nil)
        
        
    }
    
    // MARK: - Api Service Call Method
    func serviceCallToLogin(mobile: String, countryCode: String){
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            let params: Parameters = [
                "mobile": mobile,
                "con_code": countryCode
            ]
            print("params===\(params)")
            
            AF.request( AppConstant.verifyMobileUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 0){
                                    
                                    let alert = UIAlertController(title: StringConstant.register_now_msg, message: "", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Yes", style: .cancel) { action in
                                        self.performSegue(withIdentifier: "sign_up", sender: self)
                                    })
                                    alert.addAction(UIAlertAction(title: "No", style: .default) { action in
                                        
                                    })
                                    self.present(alert, animated: true)
                                    
                                }else  if(status == 1){//success
                                    
                                    self.performSegue(withIdentifier: "login_with_otp", sender: self)
                                    
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    
    //MARK: Keyboard Delegate
    @objc func keyboardWillAppear(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        if let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            loginBtnBottomConstraint.constant = keyboardHeight
        }
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification) {
        loginBtnBottomConstraint.constant = 0
    }
    
    //MARK:Country Picker Delegate
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.lblCountryCode.text = code
        self.lblCountryCallingCode.text = dialCode
        let bundle = "country_assets.bundle/"
        if let image = UIImage(named: bundle + code.uppercased(), in: Bundle(for:SignInController.self), compatibleWith: nil){
            self.imgViewCountryFlag.image = image
        }
        countryCodePicker.dismiss(animated: true) {
            
        }
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "mainController"){
            let vc = segue.destination as! HomeScreenController
            vc.userid = self.userid
            vc.token = self.token
        }
        else if (segue.identifier == "sign_up"){
            let vc = segue.destination as! SignUpController
            vc.mobile = mobileNoTf?.text
            vc.countryCode = lblCountryCode.text
        }else if (segue.identifier == "login_with_otp"){
            let vc = segue.destination as! LoginWithOTPViewController
            vc.mobile = mobileNoTf.text?.trim()
            vc.countryCode = lblCountryCode.text!
        }
    }
    
    
    
    
}
extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
