//
//  SetNewPasswordViewController.swift
//  HLW
//
//  Created by Chinmaya Sahu on 2/20/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class SetNewPasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordTf: UITextField!
    @IBOutlet weak var confirmPasswordTf: UITextField!
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    var mobile : String?
    var countryCode : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.bounds.size.height >= 812{
            navBarHeightConstraint.constant = 92
            btnHeightConstraint.constant = 60
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "SetNewPasswordPage", screenClass: "SetNewPasswordViewController")
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdatePasswordAction(_ sender: Any) {
        var errMessage : String?
        let newPassword = self.newPasswordTf?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let confirmPassword = self.confirmPasswordTf?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(newPassword == ""){
            errMessage = StringConstant.password_blank_validation_msg
        }else if((newPassword?.count)! < 8){
            errMessage = StringConstant.password_validation_msg
        }else if(!AppConstant.validatePassword(phrase: newPassword!)){
            errMessage = StringConstant.password_validation_msg
        }else if(confirmPassword == ""){
            errMessage = StringConstant.cnf_password_blank_validation_msg
        }else if(newPassword != confirmPassword){
            errMessage = StringConstant.password_mismatch_validation_msg
        }
        
        if(errMessage != nil){
            self.showAlert(alertTitle: errMessage!, alertMessage: "")
        }else {
            serviceCallToUpdateSetNewPassword(newPassword: newPassword!)
        }
    }
    
    // MARK: - Api Service Call Method
    func serviceCallToUpdateSetNewPassword(newPassword: String){
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            let params: Parameters = [
                "mobile": mobile!,
                "password": newPassword
            ]
            
            print("url===\(AppConstant.updatePasswordUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.updatePasswordUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int {
                                if(status == 1){//success
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                    //Pop to Login with Password Page
                                    for controller in self.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: SignInWithPaaswordViewController.self) {
                                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                                            break
                                        }
                                    }
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: StringConstant.noInternetConnectionMsg)
        }
    }
    

}
