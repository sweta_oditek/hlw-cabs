//
//  DriverToReachPickUpPointViewController.swift
//  Taxi Booking
//
//  Created by OdiTek Solutions on 19/02/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import SDWebImage
import Alamofire

class DriverToReachPickUpPointViewController: UIViewController , GMSMapViewDelegate , CLLocationManagerDelegate {
    
    @IBOutlet weak var driverProfilePic: UIImageView!
    @IBOutlet var googleMapView: GMSMapView!
    @IBOutlet var pickUpLocationTF: UITextField!
    @IBOutlet var dropLocationTF: UITextField!
    @IBOutlet var viewPickUpLocation: UIView!
    @IBOutlet var viewDropLocation: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var viewDriverInfo: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var bottomViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var navBarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropLocationTFHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var driverInfoImg: UIImageView!
    @IBOutlet weak var lblDriverInfo: UILabel!
    
    @IBOutlet weak var btnSos: UIButton!
    
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblVehicleNumber: UILabel!
    @IBOutlet var lblOTP: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    
    var locationManager = CLLocationManager()
    var pickUpCoordinate : CLLocationCoordinate2D? = nil
    var dropCoordinate : CLLocationCoordinate2D? = nil
    var pickUpAddress : String!
    var dropAddress : String!
    var bookingStatusBo = BookingStatusBO()
    var arrLocations = [CLLocationCoordinate2D]()
    var driverCoordinate : CLLocationCoordinate2D? = nil
    var driver_lat_old : String!
    var driver_lon_old : String!
    var driver_lat_new : String!
    var driver_lon_new : String!
    var count : Int! = 1
    var timer = Timer()
    var driverStartLocMarker = GMSMarker()
    var currentCity : String!
    var driverMarker = GMSMarker()
    var driverLocInfo = DriverLocationInfoBo()
    
    //Draw animated path
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    
    var index = 0
    var driverPins = [GMSMarker]()
    let driverPin = GMSMarker()
    var oldCabCoordinate : CLLocationCoordinate2D? = nil
    
    var isFromTotalTrip: Bool = false
    
    var movingcar: UIImage = UIImage.init(named: StringConstant.car_yellow_map_pin)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.serviceCallToDriverLocation()
        
        designAfterStoryBoard()
        //setValuesToView()manas
        
        callInEvery5Sec()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.disableDragFromNotification(notification:)), name: Notification.Name("DisableMapFromDragNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookStatusNotification(notification:)), name: Notification.Name("BookingStatusNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookStatusStopNotification(notification:)), name: Notification.Name("BookingStatusNotification"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleMapView.clear()
        
        if self.bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.startRide{// StartRide or OnRide
            updateBookStatus()
        }else{
        updateLocation()
        }
        
        AppConstant.setTrackScreenName(screenName: "DriverToReachPickUpPointPage", screenClass: "DriverToReachPickUpPointViewController")
    }
    
    // MARK: - Design Methods
    func designAfterStoryBoard() {
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            navBarHeightConstraint.constant = 92
            bottomViewBottomConstraint.constant = 34
        }
        driverProfilePic.layer.cornerRadius = driverProfilePic.frame.size.width / 2
        driverProfilePic.clipsToBounds = true
        
        btnSos.layer.cornerRadius = btnSos.frame.size.width / 2
        btnSos.clipsToBounds = true
        
        viewPickUpLocation.layer.cornerRadius = 3
        viewPickUpLocation.clipsToBounds = true
        viewDropLocation.layer.cornerRadius = 3
        viewDropLocation.clipsToBounds = true
        
        googleMapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewDriverInfoTap(_sender:)))
        viewDriverInfo.addGestureRecognizer(tap)
        viewDriverInfo.isUserInteractionEnabled = true
        
    }
    
    func updateDriverLocPin(){
        googleMapView.clear()
        
        guard let lat = Double(driverLocInfo.driverLat),
              let lon = Double(driverLocInfo.driverLon) else {
            return
        }
        
        if bookingStatusBo.bookingInfoBo.book_status == StringConstant.startRide || bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.startRide {
            self.updateBookStatus()
        }
        
        driverCoordinate = CLLocationCoordinate2D(latitude:lat, longitude:lon)
        
        if oldCabCoordinate == nil{
            oldCabCoordinate = driverCoordinate
        }
        
        movingcar = self.image(with: getCabImageFromName(cabName: bookingStatusBo.bookingInfoBo.cat_name), scaledTo: CGSize(width: 40.0, height: 40.0))
        animateVehiceLocation(oldCoodinate: oldCabCoordinate!, newCoodinate: driverCoordinate!, image: getCabImageFromName(cabName: bookingStatusBo.bookingInfoBo.cat_name), pin: driverLocInfo.vehiclepin, bearing: Int(driverLocInfo.driverBearing)!)
        
        oldCabCoordinate = driverCoordinate
        
        //Add Locations in arrLocations
        arrLocations.append(driverCoordinate!)
        arrLocations.append(pickUpCoordinate!)
//        self.setBoundsForMap()
    }
    
    func getCabImageFromName(cabName: String) -> UIImage{
        var image: UIImage = UIImage.init(named: StringConstant.micro_car_map_pin)!
        
        if cabName == "Auto"{
            image = UIImage.init(named: StringConstant.auto_yellow_map_pin)!
        }else{
            image = UIImage.init(named: StringConstant.car_yellow_map_pin)!
        }
        return image
    }
    
    func updateLocation(){
        if bookingStatusBo.bookingInfoBo.ride_id == "0"{
            topViewHeightConstraint.constant = 101
            dropLocationTF.isHidden = false
            dropLocationTFHeightConstraint.constant = 50
            
            pickUpLocationTF.text = self.pickUpAddress
            dropLocationTF.text = self.dropAddress
        }else{
            topViewHeightConstraint.constant = 51
            dropLocationTF.isHidden = true
            dropLocationTFHeightConstraint.constant = 0
            
            pickUpLocationTF.text = self.pickUpAddress
        }
        
        // Mark: Retrieve&Show the Data
        let image: String = bookingStatusBo.driverBo.driver_image
        driverProfilePic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: ""))
        
        var img: String = bookingStatusBo.driverBo.vehicle_image
        img = img.replacingOccurrences(of: " ", with: "%20", options: .caseInsensitive, range: nil)
        vehicleImage.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: ""))
        
        lblDriverName.text = bookingStatusBo.driverBo.driver_name
        
        let vehcileBrandName: String = bookingStatusBo.driverBo.vehcile_brand_name
        let vehcileModelName: String = bookingStatusBo.driverBo.vehcile_model_name
        lblVehicleName.text = "\(vehcileBrandName) \(vehcileModelName)"
        
        let bookOtp: String = bookingStatusBo.driverBo.otp
        lblOTP.text = "START RIDE PIN: \(bookOtp)"
        
        lblVehicleNumber.text = bookingStatusBo.driverBo.vehcile_registration_number
        lblRating.text = bookingStatusBo.driverBo.driver_rating
        
        //Hide My location button
        self.googleMapView.settings.myLocationButton = false
        
        if(AppConstant.count == 0){
            AppConstant.count = 600
        }
        
        let pickuplat = String(describing: self.pickUpCoordinate!.latitude)
        let pickUplon = String(describing: self.pickUpCoordinate!.longitude)
        
        createMarker(titleMarker: "", iconMarker: UIImage.init(named: StringConstant.pickUpPin)!, latitude: pickuplat, longitude: pickUplon)
    }
    
    override func viewDidLayoutSubviews() {
        
        topView.setShadowWithCornerRadius(corners: 20.0)
    }
    
    func setValuesToView() {
        
        lblRating.text = "4.0"
        lblOTP.text = String(format : "OTP: %@", bookingStatusBo.driverBo.otp)
        lblDriverName.text = bookingStatusBo.driverBo.driver_name
        lblVehicleName.text = "\(bookingStatusBo.driverBo.vehcile_brand_name) \(bookingStatusBo.driverBo.vehcile_model_name)"

        driverProfilePic.sd_setImage(with: URL(string: bookingStatusBo.driverBo.driver_image), placeholderImage: UIImage(named: "driverProfile"))
        driver_lat_old = bookingStatusBo.driverBo.latitude
        driver_lon_old = bookingStatusBo.driverBo.longitude
        driver_lat_new = bookingStatusBo.driverBo.latitude
        driver_lon_new = bookingStatusBo.driverBo.longitude
        
    }
    func callInEvery5Sec(){
        googleMapView.clear()
        // Scheduling timer to Call the function "updateCounting" with the interval of 5 seconds
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.serviceCallToDriverLocation), userInfo: nil, repeats: true)
        
    }
    
    //Mark: - This Function Should Call In after Start the Ride
    @objc func bookStatusNotification(notification: Notification){
        
            //Call api to confirm your booking
            self.serviceCallToGetBookingStatusDetails()
    }
    //Mark: - This Function Should Call In after Stop the Ride
    @objc func bookStatusStopNotification(notification: Notification){
        stopTimer()
    }
    
    func updateBookStatus(){
        DispatchQueue.main.async {
            self.lblTitle.text = "Enjoy your ride"
            self.driverInfoImg.image = UIImage.init(named: "driver_profile")
            self.lblDriverInfo.text = "Driver Info"
//            if self.bookingStatusBo.bookingInfoBo.ride_id == "1"{
//                self.lblOTP.text = "STOP RIDE PIN: \(self.bookingStatusBo.bookingInfoBo.stopOTP)"
//            }
            self.updateLocation()
        }
        
        if self.bookingStatusBo.bookingInfoBo.ride_id == "0"{
            googleMapView.clear()
            let droplat = String(describing: self.dropCoordinate!.latitude)
            let droplon = String(describing: self.dropCoordinate!.longitude)
            
            createMarker(titleMarker: "", iconMarker: UIImage.init(named: StringConstant.dropPin)!, latitude: droplat, longitude: droplon)
            
            arrLocations.removeAll()
            arrLocations.append(pickUpCoordinate!)
            arrLocations.append(dropCoordinate!)
        }
        
//        setBoundsForMap()
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    // MARK: - Google Map Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.myLocationButton = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        googleMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        locationManager.stopUpdatingLocation()
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func handleUserCurrentLocationTap(_ sender: UITapGestureRecognizer? = nil) {
        
        guard let lat = self.googleMapView.myLocation?.coordinate.latitude,
            let lng = self.googleMapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15)
        self.googleMapView.animate(to: camera)
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
    }
    
    // MARK: Set bounds for Map
    
    func setBoundsForMap() {
        var bounds = GMSCoordinateBounds()
        for location in arrLocations
        {
            let latitude = location.latitude
            let longitude = location.longitude
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude:latitude, longitude:longitude)
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        self.googleMapView.animate(with: update)
    }
    
    //MARK: - Draw direction path, from start location to desination location
    func drawPolinine(){
        //Draw route between two locations
        if self.dropCoordinate != nil{//Manas
            self.drawPath(startLocation: self.driverCoordinate!, endLocation: self.pickUpCoordinate!)
        }
    }
    
    //MARK: - this is function for create direction path, from start location to desination location
    func drawPath(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D)
    {
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(AppConstant.GoogleMapApiKey)"
        print("Drawpath url ->",String(describing: url))
        
        AF.request(url).responseJSON { response in
            
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result as Any)   // result of response serialization
            
            if let json = try? JSON(data: response.data!) {
                //print(json)
                let routes = json["routes"].arrayValue
                
                // print route using Polyline
                for route in routes
                {
                    let arrlegs = route["legs"].arrayValue
                    let dict = arrlegs[0]
                    let distanceDict = dict["distance"].dictionary
                    let distance = distanceDict!["text"]?.string
                    print(distance as Any)
                    let durationDict = dict["duration"].dictionary
                    let duration = durationDict!["text"]?.string
                    print(duration as Any)
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 5
                    polyline.strokeColor = UIColor.init(red: 105.0/255.0, green: 161.0/255.0, blue: 240.0/255.0, alpha: 1)
                    polyline.map = self.googleMapView
                }
            }
            
        }
    }
    
    // MARK: function for create a marker pin on map
    
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: String, longitude: String) {
        let lat = Double(latitude)
        let lon = Double(longitude)
        
        let coordinates = CLLocationCoordinate2D(latitude:lat!
            , longitude:lon!)
        
        if(titleMarker == "start"){
            self.driverStartLocMarker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
            self.driverStartLocMarker.title = titleMarker
            self.driverStartLocMarker.icon = iconMarker
            self.driverStartLocMarker.map = googleMapView
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) { // change 10 to desired number of seconds
                // Your code with delay
                self.driverStartLocMarker.map = nil
            }
        }else{
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
            marker.title = titleMarker
            marker.icon = iconMarker
            marker.map = googleMapView
        }
        
    }
    
    func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    @objc func disableDragFromNotification(notification: Notification){
        //Take Action on Notification
        if (AppConstant.isSlideMenu) {
            googleMapView.settings.scrollGestures = false
        }else{
            googleMapView.settings.scrollGestures = true
        }
        
    }
    
    //MARK: - Button Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        if isFromTotalTrip == true{
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            for controller in self.navigationController!.viewControllers as Array{
                if controller.isKind(of: HomeScreenController.self){
                    NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: StringConstant.notification_bookStatus)))
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
        }
    }
    
    @IBAction func btnSosAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SOSCallViewController") as! SOSCallViewController
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        vc.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        vc.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            vc.view.alpha = 1.0
            vc.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        self.addChild(vc)
        self.view.addSubview(vc.view)
    }
    
    @IBAction func btnCallDriverAction(_ sender: Any) {
        let alert = UIAlertController(title: "Call Driver", message: StringConstant.callDriverMsg, preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            let phoneNumber:String = self.bookingStatusBo.driverBo.driver_mobile
            
            if let phoneCallURL:URL = URL(string: "tel://\(phoneNumber)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL as URL)
                    }
                    
                }
            }
        })
        alert.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in
            
            
        })
        alert.addAction(cancelAction)
        
        alert.preferredAction = confirmAction
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnShareDetailsAction(_ sender: Any) {
        var vehName = "\(bookingStatusBo.driverBo.vehcile_brand_name) \(bookingStatusBo.driverBo.vehcile_model_name)"
        var driverLocation = "\(driverLocInfo.driverLat), \(driverLocInfo.driverLon)"
        
        let message = "Driver Name: \(bookingStatusBo.driverBo.driver_name) \nDriver Number: \(bookingStatusBo.driverBo.driver_mobile) \nDriver Vehicle Name: \(vehName) \nVehicle Regd Number: \(bookingStatusBo.driverBo.vehcile_registration_number) \nDriver Location : \(driverLocation)"
        
        //Set the link to share.
        if let link = NSURL(string: "http://yoururl.com")
        {
            let objectsToShare = [message] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnHelpDeskAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpLineNumberViewController") as! HelpLineNumberViewController
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        vc.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        vc.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            vc.view.alpha = 1.0
            vc.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        self.addChild(vc)
        self.view.addSubview(vc.view)
    }
    
    @IBAction func btnCancelRideAction(_ sender: Any) {
        if bookingStatusBo.bookingInfoBo.book_status == StringConstant.startRide || bookingStatusBo.tripListInfoBo.bookStatus == StringConstant.startRide {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverInformationViewController") as! DriverInformationViewController
            self.present(vc, animated: true, completion: nil)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CancelRidePopupViewController") as! CancelRidePopupViewController
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            
            vc.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            vc.view.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                vc.view.alpha = 1.0
                vc.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
            self.addChild(vc)
            self.view.addSubview(vc.view)
        }
        
    }
    
    @objc func viewDriverInfoTap(_sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "driverInfo", sender: self)
    }
    
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "driverInfo"{
            let vc = segue.destination as! DriverInfoScreenController
            vc.bookingStatusBo = self.bookingStatusBo //manas
        }
        if segue.identifier == "BillingDetails"{
            let vc = segue.destination as! BillViewController
            vc.bookingStatusBo = self.bookingStatusBo
        }
    }
    
    //MARK: - Service Call Method
    func serviceCallToCancelBooking() {
        AppConstant.isBookingConfirm = false
        if AppConstant.hasConnectivity() {
            AppConstant.showHUD()
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: "user_id"),
                "driver_id" : bookingStatusBo.driverBo.driver_id,
                "booking_id" : AppConstant.retrievFromDefaults(key: "booking_id")]
            
            print("Url===\(AppConstant.cancelBookingUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.cancelBookingUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                        if let arrData = value as? [[String: Any]]{
                            let dict = arrData[0]
                            if let status = dict["status"] as? Int {
                                if(status == 0){
                                    let msg = dict["msg"] as? String
                                    self.showAlert(alertTitle: msg!, alertMessage: "")
                                }else  if(status == 1){
                                    //Show alert for booking cancelled
                                    AppConstant.showAlertForCancelBooking()
                                    
                                }else  if(status == 2){
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.goToLandingScreen()
                                }
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    @objc func serviceCallToDriverLocation(){
        if AppConstant.hasConnectivity() {
            //AppConstant.showHUD()
            
            var params: Parameters!
            params = ["user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                      "driver_id" : bookingStatusBo.driverBo.driver_id,
                      "book_id" : AppConstant.retrievFromDefaults(key: StringConstant.book_id),
                      "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("Url===\(AppConstant.driverLocationUrl)")
            print("params===\(params)")
            
            AF.request( AppConstant.driverLocationUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    switch(response.result) {
                    case let .success(value):
                    
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String{
                                if status == "1"{//Success
                                    if let book_Id = dict["book_id"] as? String{
                                        self.driverLocInfo.bookId = book_Id
                                    }
                                    if let cust_Id = dict["cust_id"] as? String{
                                        self.driverLocInfo.cusId = cust_Id
                                    }
                                    if let driver_Id = dict["driver_id"] as? String{
                                        self.driverLocInfo.driverId = driver_Id
                                    }
                                    if let driver_Lat = dict["driver_lat"] as? String{
                                        self.driverLocInfo.driverLat = driver_Lat
                                    }
                                    if let driver_Lon = dict["driver_lon"] as? String{
                                        self.driverLocInfo.driverLon = driver_Lon
                                    }
                                    if let driver_Geoid = dict["driver_geoid"] as? String{
                                        self.driverLocInfo.driverGeoId = driver_Geoid
                                    }
                                    if let driver_Bearing = dict["driver_bearing"] as? String{
                                        self.driverLocInfo.driverBearing = driver_Bearing
                                    }
                                    if let ride_Completed = dict["ride_completed"] as? String{
                                        if ride_Completed == ""{
                                            self.bookingStatusBo.bookingInfoBo.ride_completed = 0
                                        }else{
                                            self.bookingStatusBo.bookingInfoBo.ride_completed = Int(ride_Completed)!
                                        }
                                    }
                                    self.updateDriverLocPin()
                                    
                                    if self.bookingStatusBo.bookingInfoBo.book_status != StringConstant.startRide {
                                        //Draw route between two locations
                                          self.drawPolinine()
                                    }
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "Something went wrong! please try after some time")
                                    }
                                }
                                
                            }
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    func serviceCallToGetBookingStatusDetails() {
        if AppConstant.hasConnectivity() {
            
            var params: Parameters!
            params = [
                "user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id),
                "book_id" : AppConstant.retrievFromDefaults(key: StringConstant.book_id),
                "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("params===\(params!)")
            print("Api===\(AppConstant.getBookingStatusDetailsUrl)")
            
            AF.request( AppConstant.getBookingStatusDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    debugPrint(response)
                    
                    switch(response.result) {
                    case let .success(value):
                    
                    if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? String {
                                if(status == "1"){//Success
                                    if let book_Status = dict["book_status"] as? String{
                                        self.bookingStatusBo.bookingInfoBo.book_status = book_Status
                                     }
                                    if let book_id = dict["book_id"] as? String{
                                        self.bookingStatusBo.bookingInfoBo.bookId = book_id
                                    }
                                    if let dictBookInfo = dict["book_info"] as? [String: Any]{
                                        if let bookOtp = dictBookInfo["book_otp"] as? String{
                                            self.bookingStatusBo.driverBo.otp = bookOtp
                                        }
                                        if let pickPnt = dictBookInfo["pick_pnt"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.pick_pnt = pickPnt
                                        }
                                        if let dropPnt = dictBookInfo["drop_pnt"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.drop_pnt = dropPnt
                                        }
                                        if let srcLat = dictBookInfo["src_lat"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.src_lat = srcLat
                                        }
                                        if let srcLon = dictBookInfo["src_lon"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.src_lon = srcLon
                                        }
                                        if let desLat = dictBookInfo["des_lat"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.des_lat = desLat
                                        }
                                        if let desLon = dictBookInfo["des_lon"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.des_lon = desLon
                                        }
                                        if let rideId = dictBookInfo["ride_id"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.ride_id = rideId
                                        }
                                        if let rideName = dictBookInfo["ride_name"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.ride_name = rideName
                                        }
                                        if let catName = dictBookInfo["cat_name"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.cat_name = catName
                                        }
                                        if let waitTime = dictBookInfo["wait_time"] as? Int{
                                            self.bookingStatusBo.bookingInfoBo.wait_time = waitTime
                                        }
                                        if let stop_Otp = dictBookInfo["stop_otp"] as? String{
                                            self.bookingStatusBo.bookingInfoBo.stopOTP = stop_Otp
                                        }
                                        if let ride_Time = dictBookInfo["ride_time"] as? Int{
                                            self.bookingStatusBo.bookingInfoBo.ride_time = ride_Time
                                        }
                                        if let rideCompleted = dictBookInfo["ride_completed"] as? String{
                                            if rideCompleted == ""{
                                                 self.bookingStatusBo.bookingInfoBo.ride_completed = 0
                                            }else{
                                                 self.bookingStatusBo.bookingInfoBo.ride_completed = Int(rideCompleted)!
                                            }
                                        }
                                        
                                    }
                                    if let dictRideFareInfo = dict["ride_fare"] as? [String: Any]{
                                        if let total_Cost = dictRideFareInfo["total_cost"] as? String{
                                            self.bookingStatusBo.rideFareBo.totalCost = total_Cost
                                        }
                                        if let tax = dictRideFareInfo["tax"] as? String{
                                            self.bookingStatusBo.rideFareBo.tax = tax
                                        }
                                        if let pay_Mode = dictRideFareInfo["pay_mode"] as? String{
                                            self.bookingStatusBo.rideFareBo.payMode = pay_Mode
                                        }
                                        if let fareDetails = dictRideFareInfo["fare_detail"] as? [[String: Any]]{
                                            for dict in fareDetails{
                                                let fareBO = FareDetailsBO()
                                                if let key = dict["key"] as? String{
                                                    fareBO.title = key
                                                }
                                                if let value = dict["value"] as? Double{
                                                    fareBO.value = String(value)
                                                }
                                                if let value = dict["value"] as? String{//Need to Change
                                                    fareBO.value = value
                                                }
                                                if let type = dict["type"] as? Int{
                                                    fareBO.type = type
                                                }
                                                self.bookingStatusBo.rideFareBo.arrFareDetails.append(fareBO)
                                            }
                                        }
                                    }
                                    if let dictDriverInfo = dict["driver_info"] as? [String: Any]{
                                        if let driverId = dictDriverInfo["driver_id"] as? String {
                                            self.bookingStatusBo.driverBo.driver_id = driverId
                                        }
                                        if let driverName = dictDriverInfo["driver_name"] as? String {
                                            self.bookingStatusBo.driverBo.driver_name = driverName
                                        }
                                        if let driverMobile = dictDriverInfo["driver_mobile"] as? String {
                                            self.bookingStatusBo.driverBo.driver_mobile = driverMobile
                                        }
                                        if let driverRating = dictDriverInfo["driver_rating"] as? String {
                                            self.bookingStatusBo.driverBo.driver_rating = driverRating
                                        }
                                        if let driverImage = dictDriverInfo["driver_image"] as? String {
                                            self.bookingStatusBo.driverBo.driver_image = driverImage
                                        }
                                        if let vehcile_reg_No = dictDriverInfo["vehcile_reg_no"] as? String {
                                            self.bookingStatusBo.driverBo.vehcile_registration_number = vehcile_reg_No
                                        }
                                        if let vehcile_brand_Name = dictDriverInfo["vehcile_brand_name"] as? String {
                                            self.bookingStatusBo.driverBo.vehcile_brand_name = vehcile_brand_Name
                                        }
                                        if let vehcile_model_Name = dictDriverInfo["vehcile_model_name"] as? String {
                                            self.bookingStatusBo.driverBo.vehcile_model_name = vehcile_model_Name
                                        }
                                        if let vehicleImage = dictDriverInfo["vehicle_image"] as? String {
                                            self.bookingStatusBo.driverBo.vehicle_image = vehicleImage
                                        }
                                    }
                                    
                                    if self.bookingStatusBo.bookingInfoBo.book_status == StringConstant.startRide {//StartRide
                                        self.updateBookStatus()
                                    }else if self.bookingStatusBo.bookingInfoBo.book_status == StringConstant.stopRide {//StopRide
                                        self.performSegue(withIdentifier: "BillingDetails", sender: self)
                                    }else if self.bookingStatusBo.bookingInfoBo.book_status == StringConstant.driverArrived{// Driver Arrived
                                        self.lblTitle.text = "Driver arrived at your location"
                                    }
                                   
                                }else{
                                    if let errorMsg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: errorMsg, alertMessage: "")
                                    }
                                }
                            }
                        }
                        
                       // self.setValuesToView()
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }
    
    // MARK: - Animation Method
    
    func animateVehiceLocation(oldCoodinate: CLLocationCoordinate2D , newCoodinate: CLLocationCoordinate2D, image: UIImage, pin : GMSMarker, bearing: Int) {
        index = index + 1
        print("Index ===\(index)")
        print("Driver location New ===\(newCoodinate)")
        print("Driver location Old ===\(oldCoodinate)")
        
        pin.icon = image
        pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        
        //found bearing value by calculation when marker add
        pin.position = oldCoodinate
        //this can be old position to make car movement to new position
        pin.map = self.googleMapView
        //marker movement animation
        CATransaction.begin()
        CATransaction.setValue(Int(7.0), forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
//            pin.rotation = CDouble(bearing)
            
        })
        pin.position = newCoodinate
        //this can be new position after car moved from old position to new position with animation
        pin.map = self.googleMapView
        pin.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        pin.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
        //found bearing value by calculation
        CATransaction.commit()
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    
    //MARK: - Memory Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

