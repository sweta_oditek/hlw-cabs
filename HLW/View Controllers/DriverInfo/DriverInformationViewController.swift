//
//  DriverInformationViewController.swift
//  HLW
//
//  Created by ODITEK on 04/09/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos

class DriverInformationViewController: UIViewController {

    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgDriverProfilePic: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverRating: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblAlternateMobileNo: UILabel!
    @IBOutlet weak var cosmosViewRatingStar: CosmosView!
    
    @IBOutlet weak var imgVehicle: UIImageView!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleRegNo: UILabel!
    
    @IBOutlet weak var lblCommunicationAddress: UILabel!
    @IBOutlet weak var lblCommCityName: UILabel!
    @IBOutlet weak var lblCommPincode: UILabel!
    
    @IBOutlet weak var lblPermanentAddress: UILabel!
    @IBOutlet weak var lblPermanentCityName: UILabel!
    @IBOutlet weak var lblPermanentPincode: UILabel!
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    
    var driverInfo = DriverInfoBo()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initDesign()
    }
    
    func initDesign(){
        //Manage for iPhone X
        if AppConstant.screenSize.height >= 812{
            navBarHeightConstraint.constant = 92
        }
        
        self.mainContentView.isHidden = true
        
        imgDriverProfilePic.layer.cornerRadius = imgDriverProfilePic.frame.size.width / 2
        imgDriverProfilePic.clipsToBounds = true
        
        self.doneBtn.layer.shadowOpacity = 0.25
        self.doneBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.doneBtn.layer.shadowRadius = 2
        self.doneBtn.layer.shadowColor = UIColor.darkGray.cgColor
        self.doneBtn.layer.masksToBounds = false
        
        self.serviceCallToDriverDetails()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "DriverInformationPage", screenClass: "DriverInformationViewController")
    }
    
    func setValuesToView(){
        let image: String = driverInfo.driverImage
        imgDriverProfilePic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "driver"))
        lblDriverName.text = driverInfo.driverName
        lblDriverRating.text = driverInfo.driverRating
        cosmosViewRatingStar.rating = driverInfo.driverRating == "" ? 0.0 : Double(driverInfo.driverRating)!
        lblMobileNo.text = "Mobile No.: \(driverInfo.driverMobile)"
        lblAlternateMobileNo.text = "Alternative Mobile No.: \(driverInfo.alternateNo)"
        
        var img: String = driverInfo.vehImage
        img = img.replacingOccurrences(of: " ", with: "%20", options: .caseInsensitive, range: nil)
        imgVehicle.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "car_micro_blue"))
        lblVehicleName.text = "\(driverInfo.vehBrandName) \(driverInfo.vehModelName)"
        lblVehicleRegNo.text = driverInfo.vehRegistrationNo
        
        lblCommunicationAddress.text = driverInfo.comAddress
        lblCommCityName.text = "City: \(driverInfo.comCity)"
        lblCommPincode.text = "Pincode: \(driverInfo.comPincode)"
        lblPermanentAddress.text = driverInfo.perAddress
        lblPermanentCityName.text = "City: \(driverInfo.perCity)"
        lblPermanentPincode.text = "Pincode: \(driverInfo.perPincode)"
    }
    
    // MARK: - Button Action
    @IBAction func btnDoneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Service Call Method
    func serviceCallToDriverDetails() {
        if AppConstant.hasConnectivity() {
             AppConstant.showHUD()
            
            let params: Parameters = ["user_id" : AppConstant.retrievFromDefaults(key: StringConstant.user_id), "book_id" : AppConstant.retrievFromDefaults(key: StringConstant.book_id), "access_token" : AppConstant.retrievFromDefaults(key: StringConstant.accessToken)]
            
            print("Url===\(AppConstant.driverDetailsUrl)")
            print("params===\(params)")
            
            AF.request(AppConstant.driverDetailsUrl, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { response in
                    AppConstant.hideHUD()
                    debugPrint(response)
                    
                    self.mainContentView.isHidden = false
                    switch(response.result) {
                    case let .success(value):
                        if let dict = value as? [String: Any]{
                            if let status = dict["status"] as? Int{
                                if status == 1{
                                    if let dictData = dict["data"] as? [String: Any]{
                                        if let driver_Id = dictData["driver_id"] as? String{
                                            self.driverInfo.driverId = driver_Id
                                        }
                                        if let driver_Name = dictData["driver_name"] as? String{
                                            self.driverInfo.driverName = driver_Name
                                        }
                                        if let driver_Mobile = dictData["driver_mobile"] as? String{
                                            self.driverInfo.driverMobile = driver_Mobile
                                        }
                                        if let driver_Rating = dictData["driver_rating"] as? String{
                                            self.driverInfo.driverRating = driver_Rating
                                        }
                                        if let driver_Image = dictData["driver_image"] as? String{
                                            self.driverInfo.driverImage = driver_Image
                                        }
                                        if let vehcile_Reg_no = dictData["vehcile_reg_no"] as? String{
                                            self.driverInfo.vehRegistrationNo = vehcile_Reg_no
                                        }
                                        if let vehcile_Brand_name = dictData["vehcile_brand_name"] as? String{
                                            self.driverInfo.vehBrandName = vehcile_Brand_name
                                        }
                                        if let vehcile_Model_name = dictData["vehcile_model_name"] as? String{
                                            self.driverInfo.vehModelName = vehcile_Model_name
                                        }
                                        if let vehicle_Image = dictData["vehicle_image"] as? String{
                                            self.driverInfo.vehImage = vehicle_Image
                                        }
                                        if let alternative_No = dictData["alternative_no"] as? String{
                                            self.driverInfo.alternateNo = alternative_No
                                        }
                                        if let com_Address = dictData["com_address"] as? String{
                                            self.driverInfo.comAddress = com_Address
                                        }
                                        if let com_City = dictData["com_city"] as? String{
                                            self.driverInfo.comCity = com_City
                                        }
                                        if let com_Pincode = dictData["com_pincode"] as? String{
                                            self.driverInfo.comPincode = com_Pincode
                                        }
                                        if let per_Address = dictData["per_address"] as? String{
                                            self.driverInfo.perAddress = per_Address
                                        }
                                        if let per_City = dictData["per_city"] as? String{
                                            self.driverInfo.perCity = per_City
                                        }
                                        if let per_Pincode = dictData["per_pincode"] as? String{
                                            self.driverInfo.perPincode = per_Pincode
                                        }
                                    }
                                }else{
                                    if let msg = dict["msg"] as? String{
                                        self.showAlert(alertTitle: msg, alertMessage: "")
                                    }
                                }
                            }
                            self.setValuesToView()
                        }
                        
                        break
                        
                    case let .failure(error):
                        self.showAlert(alertTitle: error.localizedDescription, alertMessage: "")
                        break
                        
                    }
            }
        }else{
            AppConstant.showSnackbarMessage(msg: "Please check your internet connection.")
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
