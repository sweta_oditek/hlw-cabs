//
//  DriverInfoScreenController.swift
//  Taxi Booking
//
//  Created by Chinmaya Sahu on 29/01/22.
//  Copyright © 2022 OdiTek Solutions. All rights reserved.
//

import UIKit
import Alamofire

class DriverInfoScreenController: UIViewController {

    @IBOutlet var imgViewDriver: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblVehicleNumber: UILabel!
    @IBOutlet var vehicleLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPathLine: UILabel!
    
    var bookingStatusBo = BookingStatusBO()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initDesign()
    }
    
    func initDesign(){
        self.imgViewDriver.layer.cornerRadius = self.imgViewDriver.frame.size.width / 2
        self.imgViewDriver.clipsToBounds = true
        
        imgViewDriver.sd_setImage(with: URL(string: bookingStatusBo.driverBo.driver_image), placeholderImage: UIImage(named: "driver_man"))
        lblDriverName.text = bookingStatusBo.driverBo.driver_name
        lblVehicleName.text = "\(bookingStatusBo.driverBo.vehcile_brand_name) \(bookingStatusBo.driverBo.vehcile_model_name)"
        lblVehicleNumber.text = bookingStatusBo.driverBo.vehcile_registration_number
        
    }
    
    override func viewDidLayoutSubviews() {
        //move car
        vehicleLeadingConstraint.constant =  ((self.lblPathLine.frame.size.width * CGFloat(bookingStatusBo.bookingInfoBo.ride_completed)) / 100) - 15
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppConstant.setTrackScreenName(screenName: "DriverInfoPage", screenClass: "DriverInfoScreenController")
    }
    
    // MARK: - Button Action
    @IBAction func btnDoneAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
