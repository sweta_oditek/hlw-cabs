//
//  StringConstant.swift
//  HLW
//
//  Created by OdiTek Solutions on 22/12/18.
//  Copyright © 2018 OdiTek Solutions. All rights reserved.
//

import Foundation

class StringConstant: NSObject {
    
    //User Default
    static var isLoggedIn: String = "isLoggedIn"
    static var isIntroduceScreenShown: String = "isIntroduceScreenShown"
    static var name: String = "name"
    static var user_id: String = "user_id"
    static var accessToken: String = "access_token"
    static var current_address: String = "current_address"
    static var deviceToken: String = "APNID"
    static var mobile: String = "mobile"
    static var email: String = "email"
    static var profile_image: String = "profile_image"
    static var book_id: String = "book_id"
    
    //Cab Names
    static var car_micro: String = "Micro"
    static var car_mini: String = "Mini"
    static var car_sedan: String = "Sedan"
    static var car_suv: String = "SUV"
    static var auto: String = "Auto"
    
    static var car_micro_blue: String = "car_micro_blue"
    static var car_micro_white: String = "car_micro_white"
    static var car_micro_gray: String = "car_micro_grey"
    
    static var car_mini_blue: String = "car_mini_blue"
    static var car_mini_white: String = "car_mini_white"
    static var car_mini_gray: String = "car_mini_grey"
    
    static var car_sedan_blue: String = "car_sedan_blue"
    static var car_sedan_white: String = "car_sedan_white"
    static var car_sedan_gray: String = "car_sedan_grey"
    
    static var car_suv_blue: String = "car_share_blue"
    static var car_suv_white: String = "car_share_white"
    static var car_suv_gray: String = "car_suv_grey"
    
    static var auto_blue: String = "auto_blue_fill"
    static var auto_white: String = "auto_blue"
    static var auto_gray: String = "auto_fill_grey"
    
    static var micro_car_map_pin: String = "map_pin_micro"
    static var mini_car_map_pin: String = "map_pin_mini"
    static var sedan_car_map_pin: String = "map_pin_sedan"
    static var suv_car_map_pin: String = "map_pin_suv"
    static var auto_gray_map_pin: String = "map_pin_Auto_gray"
    static var auto_yellow_map_pin: String = "map_pin_auto_yellow"
    static var car_yellow_map_pin: String = "car_new"
    
    //Location Pin
    static let dropPin: String = "map_pin"
    static let pickUpPin: String = "map_pin_green"
    
    //Notification Types
    static let errorMsg: String = "2"
    static let saveNotification: String = "5"
    
    //BookingStatus Types
    static let noService: String = "0"
    static let newCustomerBooking: String = "1"
    static let bookingConfirm: String = "2"
    static let driverArrived: String = "3"
    static let startRide: String = "4"
    static let rideCompleted: String = "5"
    static let cancelByCustomer: String = "6"
    static let cancelByAdmin: String = "7"
    static let cancelByDriver: String = "8"
    static let onlinePaymentFailure: String = "9"
    static let waitForBankTransfer: String = "10"
    static let scheduleBooking: String = "11"
    static let stopRide: String = "12"
    
    //Font
    static let appFontPoppinsMedium : String = "Poppins-Medium"
    static let appFontPoppinsRegular : String = "Poppins-Regular"
    static let appFontPoppinsBold : String = "Poppins-Bold"
    
    //Messages
    static var logout_msg: String = "Are you sure you want to logout?"
    static var remove_contact_from_emergency_msg: String = "Are you sure you want to remove this from your emergency contact list?"
    static var contact_already_exist_emergency_list_msg: String = "Contact already exist in your Emergency contact list"
    static var empty_emergency_contact_msg: String = "Alert your family and friends \n in  case of an emergency. \nAdd them to your emergency contacts."
    static var internetToAddPhotoMsg : String = "Internet connectivity is required to add photo"
    static var noInternetConnectionMsg : String = "Please check your internet connection."
    static var changeMobileTitleMsg : String = "Do you want to change your mobile number?"
    static var changeMobileDescMsg : String = "OTP will be sent to Registered Mobile Number"
    static var callDriverMsg: String = "Are you sure you want to call driver?"
    static var addContactErrorMsg: String = "You can add up to 5 contacts"
    
    //DateFormatter
    static var dateFormatter1 : String = "yyyy-MM-dd HH:mm:ss ZZZ"
    static var dateFormatter2 : String = "dd-MM-yyyy"
    static var dateFormatter3 : String = "dd MMM yyyy, h:mm a"
    static var dateFormatter4 : String = "dd-MM-yyyy h:mm a"
    static var dateFormatter5 : String = "dd MMM yyyy HH:mm"
    static var dateFormatter6 : String = "h:mm a"
    static var dateFormatter7 : String = "dd MMM,h:mm a"
    
    //Social Login Type
    static var facebook : String = "facebook"
    static var google : String = "google"
    
    //Validation Msg
    static var mobile_blank_validation_msg : String = "Please enter your mobile number"
    static var register_now_msg : String = "Mobile number doesn't exist in our record. Do you want to Register Now?"
    static var validate_mobile_msg : String = "The mobile number is not correct."
    static var otp_blank_validation_msg : String = "Please enter OTP sent to your mobile number."
    static var password_blank_validation_msg : String = "Please enter your password"
    static var old_password_blank_validation_msg : String = "Old password is required!"
    static var old_password_mismatch_validation_msg : String = "Your old password is incorrect."
    static var name_blank_validation_msg : String = "Name is required!"
    static var email_blank_validation_msg : String = "Email is required!"
    static var email_validation_msg : String = "Invalid email format!"
    static var password_validation_msg : String = "Password should contain at least one alphabet,one number and minimum 8 characters!"
    static var cnf_password_blank_validation_msg : String = "Please enter your confirm password."
    static var password_mismatch_validation_msg : String = "Confirm password must be matching!"
    static var coupon_validation_msg : String = "Please enter Coupon code"
    
    static var alert_TitleMsg : String = "Alert!"
    static var mobile_blank_validation_TitleMsg : String = "Invalid Mobile Number"
    static var mobile_blank_validation_DescMsg : String = "Please enter a valid mobile number"
    static var password_blank_validation_TitleMsg : String = "Password is required!"
    static var password_blank_validation_DescMsg : String = "Field should not remain blank"
    static var cnf_password_blank_validation_Descmsg : String = "Confirm password is required!"
    
    //Others
    static var iOS : String = "iOS"
    static var poppinsRegular : String = "Poppins-Regular"
    static var poppinsMedium : String = "Poppins-Medium"
    static var poppinsSemibold : String = "Poppins-Semibold"
    static var poppinsBold : String = "Poppins-Bold"
    static var fareDesc : String = "When booking this ride in the city, you ‘ll see a total fare before confirming you booking. This is what you ‘ll repaying at the end of your trip. Just enter your drop location to know the total fare.\n\nTotal fare includes applicable taxes, Tool/Parkinh/Hub fee and any fare increases due to demand wherever applicable"
    
    //Notifications
    static var notification_bookStatus: String = "BookingStatus"
    
    //CCAvenue
    static var merchantId: String = "220822"//220822 -2
    static var sandboxCCAvenueAccessCode: String = "AVJG02GF75BD91GJDB"//AVEI85GE70CN08IENC -4YRUXLSRO20O8NIH /////AVJG02GF75BD91GJDB
    static var currency: String = "INR"
    static var sandboxRedirectUrl: String = "http://172.104.60.124/CCavenue/ccavResponseHandler.php"
    static var sandboxCancelUrl: String = "http://172.104.60.124/CCavenue/ccavResponseHandler.php"
    static var sandboxrsaKeyUrl: String = "http://172.104.60.124/CCavenue/GetRSA.php"
    
    static var liveCCAvenueAccessCode: String = "AVRJ86GG60BL75JRLB"
    static var liveRedirectUrl: String = "https://mobile.hlwcab.in/ccavenue/ccavResponseHandler.php"
    static var liveCancelUrl: String = "https://mobile.hlwcab.in/ccavenue/ccavResponseHandler.php"
    static var liversaKeyUrl: String = "https://mobile.hlwcab.in/ccavenue/GetRSA.php"
    
    //Payment Status Result
    static var payuTransStatusSuccess: String = "Transaction Success!"
    static var payuTransStatusFailure: String = "Transaction Failure!"
    static var payuTransStatusCancel: String = "Transaction Cancelled!"
    
    static var payuResultStatusSuccess: String = "Success!"
    static var payuResultStatusFailure: String = "Failure!"
    
    //Working key
    //D5523A0254E3421E4EBE0E48C0F266CF
}

