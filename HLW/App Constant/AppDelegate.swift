//
//  AppDelegate.swift
//  HLW
//
//  Created by OdiTek Solutions on 22/12/18.
//  Copyright © 2018 OdiTek Solutions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase
import FBSDKLoginKit
import GoogleSignIn
import NVActivityIndicatorView
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navController: UINavigationController!
    
    
    // AIzaSyDIqKyViDTwa9GISngfxvyZsUU-rZfQqqo
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        self.registerAPN(application)
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        GMSServices.provideAPIKey(AppConstant.GoogleMapApiKey)
        GMSPlacesClient.provideAPIKey(AppConstant.GoogleMapApiKey)
        
        GIDSignIn.sharedInstance().clientID = AppConstant.GIDSignInClientKey
        
        let isLoggedIn = AppConstant.retrievFromDefaults(key: StringConstant.isLoggedIn)
        if(isLoggedIn == "1"){
            self.goToMainScreen()
        }else{
            self.goToLandingScreen()
        }
        
        if let dictNotif = launchOptions?[.remoteNotification] as?  [AnyHashable : Any]{
            print("App opened by tapping from push notification")
        }
        
        FirebaseApp.configure()
        
        //Check for update app
        checkIfAnyUpdatedVersionInAppStore()
        
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        checkIfAnyUpdatedVersionInAppStore()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0;
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        PersistantStorage.saveContext()
    }
    
    func goToMainScreen(){
        //        navController = UINavigationController()
        //        navController.setNavigationBarHidden(true, animated: true)
        //        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let initialViewControlleripad : HomeScreenController = mainStoryboardIpad.instantiateViewController(withIdentifier: "HomeScreenController") as! HomeScreenController
        //        navController.viewControllers = [initialViewControlleripad]
        //        self.window = UIWindow(frame: UIScreen.main.bounds)
        //        self.window?.rootViewController = navController
        //        self.window?.makeKeyAndVisible()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeScreenController") as! HomeScreenController
        self.navController = UINavigationController(rootViewController: mainViewController)
        self.navController?.isNavigationBarHidden = true
        let leftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuController") as! MenuController
        leftViewController.homeViewController = self.navController
        let slideMenuController = ExSlideMenuController(mainViewController:self.navController!, leftMenuViewController: leftViewController)
        slideMenuController.addLeftGestures()
        slideMenuController.delegate = mainViewController
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }
    
    func goToLandingScreen(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "LandingScreenViewController") as! LandingScreenViewController
        self.navController = UINavigationController(rootViewController: mainViewController)
        self.navController?.isNavigationBarHidden = true
        let leftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuController") as! MenuController
        leftViewController.homeViewController = self.navController
        let slideMenuController = ExSlideMenuController(mainViewController:self.navController!, leftMenuViewController: leftViewController)
        slideMenuController.addLeftGestures()
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }
    
    func logoutAction(){
        AppConstant.saveInDefaults(key: "isLoggedIn", value: "0")
        self.goToLandingScreen();
    }
    
    func registerAPN(_ application: UIApplication){
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
    }
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        let str = deviceToken.map { String(format: "%02hhx", $0) }.joined()
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        //print("str : \(str)")
        AppConstant.saveInDefaults(key: StringConstant.deviceToken, value: str)
        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
        AppConstant.isBookingConfirm = true
        
        guard
            let aps = data[AnyHashable("aps")] as? NSDictionary
            else {
                // handle any error here
                return
        }
        
        let isLoggedIn = AppConstant.retrievFromDefaults(key: "isLoggedIn")
        if(isLoggedIn == "1"){
            
            if(application.applicationState == .active) {
                AppConstant.isBookingConfirm = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotification"), object: nil)
                
                //app is currently active, can update badges count here
                
            } else if(application.applicationState == .background){
                
                //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
                
            } else if(application.applicationState == .inactive){
                
                //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
                
            }
            
        }
        
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        
        if ApplicationDelegate.shared.application(app, open: url, sourceApplication: sourceApplication, annotation: nil){
            return true
        }else if GIDSignIn.sharedInstance().handle(url){
            return true
        }
        return false
    }
    
    //MARK:- App Update
    func checkIfAnyUpdatedVersionInAppStore(){
        checkForUpdate { (isUpdate) in
            print("Update needed:\(isUpdate)")
            if isUpdate{
                DispatchQueue.main.async {
                    self.showPopupForAppUpdate()
                    print("new update Available")
                }
            }
        }
    }
    
    func checkForUpdate(completion:@escaping(Bool)->()){
        guard let bundleInfo = Bundle.main.infoDictionary,
            let currentVersion = bundleInfo["CFBundleShortVersionString"] as? String,
            let identifier = bundleInfo["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)")
            //print("app url = \(url)")
            else{
                print("something went wrong")
                completion(false)
                return
        }
        
        let task = URLSession.shared.dataTask(with: url) {
            (data, resopnse, error) in
            if error != nil{
                completion(false)
                print("something went wrong")
            }else{
                do{
                    guard let reponseJson = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any],
                        let result = (reponseJson["results"] as? [Any])?.first as? [String: Any],
                        let appStoreVersion = result["version"] as? String
                        else{
                            completion(false)
                            return
                    }
                    print("Current Ver:\(currentVersion)")
                    print("Appstore version:\(appStoreVersion)")
                    let floatCurr = Float(currentVersion)!
                    let floatAppstore = Float(appStoreVersion)!
                    if floatCurr < floatAppstore{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
                catch{
                    completion(false)
                    print("Something went wrong")
                }
            }
        }
        task.resume()
    }
    
    func showPopupForAppUpdate(){
        //Set alert popupview backgroundColor:
        let overlayView = UIView(frame: CGRect(x: 0, y: 0, width: AppConstant.screenSize.width, height: AppConstant.screenSize.height))
        overlayView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        self.window?.rootViewController?.view.addSubview(overlayView)
        
        let alertController = UIAlertController(
            title: "Update available",
            message: "We have now just got smarter!.\nUpdating to the latest version of the app to get latest information.",
            preferredStyle: UIAlertController.Style.alert
        )
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: UIAlertAction.Style.default) { (action) in
                overlayView.removeFromSuperview()
        }
        let confirmAction = UIAlertAction(
        title: "Update", style: UIAlertAction.Style.default) { (action) in
            let url = "https://itunes.apple.com/app/id1484939425"
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL(string: url)!)
            }
            overlayView.removeFromSuperview()
        }
        
        alertController.view.tintColor = AppConstant.colorThemeBlue
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        alertController.preferredAction = confirmAction
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Notification will Present method called")
        AppConstant.isBookingConfirm = true
        
        var message = ""
        var bookSts = ""
        
        if let aps = notification.request.content.userInfo[AnyHashable("aps")] as? NSDictionary{
            let notiFyBO = Notifications(context: PersistantStorage.context)
            if let alert = aps["alert"] as? NSDictionary{
                let body = alert["body"] as? String
                if let title = alert["title"] as? String{
                    notiFyBO.title = title
                }
            }
            if let cus_Id = aps["cus_id"] as? String{
                notiFyBO.cus_id = cus_Id
            }
            if let notify_Id = aps["notify_id"] as? String{
                notiFyBO.notification_id = notify_Id
            }
            
            if let msg = aps["message"] as? String{
                message = msg
            }else{
                message = ""
            }
            
            var bookId = ""
            if let book_Id = aps["book_id"] as? String{
                bookId = book_Id
            }else{
                bookId = ""
            }
            
            if let book_Status = aps["book_status"] as? String{
                bookSts = book_Status
            }else{
                bookSts = ""
            }
            
            var bookStatus_Str = ""
            if let book_Status_str = aps["book_status_str"] as? String{
                bookStatus_Str = book_Status_str
            }else{
                bookStatus_Str = ""
            }
            
            var catName = ""
            if let cat_Name = aps["cat_name"] as? String{
                catName = cat_Name
            }else{
                catName = ""
            }
            
            var cusName = ""
            if let cus_Name = aps["cus_name"] as? String{
                cusName = cus_Name
            }else{
                cusName = ""
            }
            
            var driverName = ""
            if let driver_Name = aps["driver_name"] as? String{
                driverName = driver_Name
            }else{
                driverName = ""
            }
            
            var rideName = ""
            if let ride_Name = aps["ride_name"] as? String{
                rideName = ride_Name
            }else{
                rideName = ""
            }
            
            if let subject = aps["subject"] as? String{
                notiFyBO.subject = subject
            }
            if let type = aps["type"] as? String{
                notiFyBO.notification_type = type
            }
            
            var vehBrandName = ""
            if let vehcile_Brand_Name = aps["vehcile_brand_name"] as? String{
                vehBrandName = vehcile_Brand_Name
            }else{
                vehBrandName = ""
            }
            
            var vehModelImg = ""
            if let vehcileModel_Image = aps["vehcile_model_image"] as? String{
                vehModelImg = vehcileModel_Image
            }else{
                vehModelImg = ""
            }
            
            var vehModelName = ""
            if let vehcile_Model_Name = aps["vehcile_model_name"] as? String{
                vehModelName = vehcile_Model_Name
            }else{
                vehModelName = ""
            }
            
            var vehRegNo = ""
            if let vehcile_reg_No = aps["vehcile_reg_no"] as? String{
                vehRegNo = vehcile_reg_No
            }else{
                vehRegNo = ""
            }
            
            if let image_Url = aps["image_url"] as? String{
                notiFyBO.image_url = image_Url
            }else{
                notiFyBO.image_url = ""
            }
            notiFyBO.date = NSDate()
            
            if notiFyBO.notification_type == StringConstant.saveNotification {
                PersistantStorage.saveContext()
            }else if notiFyBO.notification_type == StringConstant.errorMsg {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookingErrorNotification"), object: nil)
                let window :UIWindow = UIApplication.shared.keyWindow!
                window.rootViewController?.showAlert(alertTitle: message, alertMessage: notiFyBO.subject!)
            }else{
                AppConstant.saveInDefaults(key: StringConstant.book_id, value: bookId)
                print("Booking Id === \(bookId)")
                if bookSts == StringConstant.startRide || bookSts == StringConstant.stopRide {//Start Ride & Stop Ride
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookingStatusNotification"), object: nil)
                }else if bookSts == StringConstant.cancelByDriver {//Booking Cancelled By Driver
                    self.goToMainScreen()
                }
            }
            
        }
        
//        AppConstant.showAlert(strTitle: alert!, strDescription: "", delegate: self)
        
        print(notification.description)
        print(notification.request.content.userInfo)
        if bookSts == StringConstant.bookingConfirm{//Confirm Booking
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotification"), object: nil)
        }
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert, .badge, .sound])
    }
    
    // For handling tap and user actions
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Action First Tapped")
        switch response.actionIdentifier {
        case "action1":
            print("Action First Tapped")
        case "action2":
            print("Action Second Tapped")
        default:
            break
        }
        completionHandler()
    }
    
}



