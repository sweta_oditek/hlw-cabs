//
//  RentalCabFareDetailsTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 21/05/19.
//  Copyright © 2019 OdiTek Solutions. All rights reserved.
//

import UIKit

class RentalCabFareDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblBaseFareTitle: UILabel!
    @IBOutlet weak var lblAddKmFareTitle: UILabel!
    @IBOutlet weak var lblAddTimeFareTitle: UILabel!
    @IBOutlet weak var lblMinFareTitle: UILabel!
    
    @IBOutlet weak var lblBaseFare: UILabel!
    @IBOutlet weak var lblAddKmFare: UILabel!
    @IBOutlet weak var lblAddTimeFare: UILabel!
    @IBOutlet weak var lblMinFare: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewCab: UIView!
    @IBOutlet weak var lblCabName: UILabel!
    @IBOutlet var viewContainerHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewCab.layer.cornerRadius = 3
        viewCab.clipsToBounds = true
        viewCab.layer.shadowOpacity = 0.25
        viewCab.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewCab.layer.shadowRadius = 2
        viewCab.layer.shadowColor = UIColor.black.cgColor
        viewCab.layer.masksToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
