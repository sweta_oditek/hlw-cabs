//
//  RentalDetailsRateCardTableViewCell.swift
//  HLW
//
//  Created by Chinmaya Sahu on 3/15/19.
//  Copyright © 2019 OdiTek Solutions. All rights reserved.
//

import UIKit

class RentalDetailsRateCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rentalCarView: UIView!
    @IBOutlet weak var viewTripSelection: UIView!
    @IBOutlet weak var lblSelection: UILabel!

    @IBOutlet weak var viewRentals: UIView!
    
    @IBOutlet weak var viewCitySelection: UIView!
    @IBOutlet weak var lblCityName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rentalCarView.layer.cornerRadius = rentalCarView.frame.height / 2
        rentalCarView.clipsToBounds = true
        
        viewRentals.layer.cornerRadius = 3
        viewRentals.clipsToBounds = true
        viewRentals.layer.shadowOpacity = 0.25
        viewRentals.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewRentals.layer.shadowRadius = 2
        viewRentals.layer.shadowColor = UIColor.black.cgColor
        viewRentals.layer.masksToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
