//
//  OutstationFareDetailsTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 24/09/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class OutstationFareDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblFareTitle: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //Manage for iPhone 5 and Below
//        if AppConstant.screenSize.height <= 568{
//            lblFareTitle.font = UIFont.init(name: "Poppins-Regular", size: 13.0)
//            lblFare.font = UIFont.init(name: "Poppins-Regular", size: 13.0)
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
