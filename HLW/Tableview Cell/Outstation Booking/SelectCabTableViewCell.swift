//
//  SelectCabTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 24/09/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class SelectCabTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCabSelection: UIView!
    @IBOutlet weak var lblVehiclaName: UILabel!
    @IBOutlet weak var cabImgView: UIView!
    @IBOutlet weak var imgViewVehicle: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cabImgView.layer.cornerRadius = cabImgView.frame.height / 2
        cabImgView.clipsToBounds = true
        
        viewCabSelection.layer.cornerRadius = 4
        viewCabSelection.clipsToBounds = true
        viewCabSelection.layer.shadowOpacity = 0.25
        viewCabSelection.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewCabSelection.layer.shadowRadius = 2
        viewCabSelection.layer.shadowColor = UIColor.black.cgColor
        viewCabSelection.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
