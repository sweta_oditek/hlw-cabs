//
//  OutstationRateCardTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 24/09/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class OutstationRateCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCitySelection: UIView!
    @IBOutlet weak var lblCityName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
