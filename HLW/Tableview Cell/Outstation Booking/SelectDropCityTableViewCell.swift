//
//  SelectDropCityTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 23/04/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class SelectDropCityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDropLocName: UILabel!
    @IBOutlet weak var lblEstimatedKm: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
