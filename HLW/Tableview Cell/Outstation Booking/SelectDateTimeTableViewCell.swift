//
//  SelectDateTimeTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 23/04/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class SelectDateTimeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnOneway: UIButton!
    @IBOutlet weak var lblOneway: UILabel!
    @IBOutlet weak var radioBtnOneway: UIButton!
    @IBOutlet weak var btnRoundTrip: UIButton!
    @IBOutlet weak var radioBtnRoundTrip: UIButton!
    @IBOutlet weak var lblStartDateTime: UILabel!
    @IBOutlet weak var btnSelectStartDateTime: UIButton!
    @IBOutlet weak var lblEndDateTime: UILabel!
    @IBOutlet weak var btnSelectEndDateTime: UIButton!
    @IBOutlet weak var viewEndDateTime: UIView!
    @IBOutlet weak var viewEndDateTimeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewContentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblOnewayDisableMsg: UILabel!
    @IBOutlet weak var lblDisableMsgHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
