//
//  OutstationPickupLocationTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 22/04/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class OutstationPickupLocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPickupLocation: UILabel!
    @IBOutlet weak var lblCurrentCity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
