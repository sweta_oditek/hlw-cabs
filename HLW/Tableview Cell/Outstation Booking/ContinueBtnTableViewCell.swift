//
//  ContinueBtnTableViewCell.swift
//  HLW
//
//  Created by OdiTek Solutions on 23/04/20.
//  Copyright © 2020 ODITEK. All rights reserved.
//

import UIKit

class ContinueBtnTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnContinueHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
