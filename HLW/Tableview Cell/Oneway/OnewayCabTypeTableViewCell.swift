//
//  OnewayCabTypeTableViewCell.swift
//  HLW
//
//  Created by ODITEK on 08/11/19.
//  Copyright © 2019 OdiTek Solutions. All rights reserved.
//

import UIKit

class OnewayCabTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCabContents: UIView!
    @IBOutlet weak var imgViewCab: UIImageView!
    @IBOutlet weak var lblCabName: UILabel!
    @IBOutlet weak var lblNoOfCabs: UILabel!
    @IBOutlet weak var lblCabTiming: UILabel!
    
    @IBOutlet weak var btnTotalFare: UIButton!
    @IBOutlet weak var lblTotalTax: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
