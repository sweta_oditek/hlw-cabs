//
//  BillingDetailsTableViewCell.swift
//  HLW
//
//  Created by ODITEK on 03/08/19.
//  Copyright © 2019 OdiTek Solutions. All rights reserved.
//

import UIKit

class BillingDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRideFare: UILabel!
    @IBOutlet weak var lblBillRideFare: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
